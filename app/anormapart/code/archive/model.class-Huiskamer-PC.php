<?php
    /********************** Php Model Catalog class ************************/
    /**
     * Model class
     * Generates Model for PHP
     *
     * You can use phpDocumentor to generate documentation
     *
     * 
     * @lastmodified 19/01/2015
     * @since 01/06/2012           
     * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M        
     * @version 0.2
    */
    namespace AnOrmApart\Php;
    class Model extends \AnOrmApart\Catalog
    {
        /** ------------------ zipAddScriptAppModelClass  --------------------------
        *
        * Create App Model
        * @lastmodified 21/01/2015
        * @since 01/06/2012           
        * @author Jef Inghelbrecht - An Orm Apart       
        * @version 0.2
        * @return string  
        */
        public function zipAddScriptAppModelClass($zip)
        {
            $script = '';
            if ($this->CatalogExists())
            {
                // make model for application
                $fileName = "{$this->GetNamespaceToLower()}/{$this->GetDatabaseNameToLower()}/src/Model.php";
                $zip->addFromString($fileName, $this->makeScriptAppModelClass());    
                return TRUE;
            }
            return false;
        }

        /** ------------------ zipAddScriptModelClasses  --------------------------
        *
        * Create Model class for one table
        * @lastmodified 21/01/2015
        * @since 01/06/2012           
        * @author Jef Inghelbrecht - An Orm Apart       
        * @version 0.2
        * @return string  
        */
        public function zipAddScriptModelClasses($zip)
        {
            $script = '';
            if ($this->CatalogExists())
            {
                // Make model class
                foreach ($this->catalog as $table)
                {
                    // onthoud met welke tabel we bezig zijn
                    $this->dTable = $table;
                    $fileName = "{$this->GetNamespaceToLower()}/{$this->GetDatabaseNameToLower()}/src/Model/{$this->dTable->GetNameUCFirst()}.php";
                    $zip->addFromString($fileName, $this->makeScriptModelClass($fileName));    
                }
                return TRUE;
            }
            return false;
        }

        /** ------------------ makeScripModelClasses  --------------------------
        *
        * Create Model class for one table
        * @lastmodified 21/01/2015
        * @since 01/06/2012           
        * @author Jef Inghelbrecht - An Orm Apart       
        * @version 0.2
        * @return string  
        */
        public function makeScriptModelClasses()
        {
           $script = '';
            if ($this->CatalogExists())
            {
                // Make model class
                foreach ($this->catalog as $table)
                {
                    // onthoud met welke tabel we bezig zijn
                    $this->dTable = $table;
                    $fileName = "{$this->GetNamespaceToLower()}/{$this->GetDatabaseNameToLower()}/src/Model/{$this->dTable->GetNameUCFirst()}.php";
                    $script .= $this->makeScriptModelClass($fileName);
                    $script .= "\n";    
                }
            }
            return $script;
        }

        /** ------------------ makeScriptAppModelClass  --------------------------
        *
        * Create Model class that holds all individual models for tables
        * @lastmodified 21/01/2015
        * @since 01/06/2012           
        * @author Jef Inghelbrecht - An Orm Apart       
        * @version 0.2
        * @return string  
        */
        public function makeScriptAppModelClass()
        {
            $filename = "{$this->GetNamespaceToLower()}/{$this->GetDatabaseNameToLower()}/src/Model.php";
            $script = "<?php\n";
            $script .= "/* modernways.be\n";
            $script .= "* created by an orm apart\n";
            $script .= "* Entreprise de modes et de manières modernes\n";
            $script .= "* Application Model class for {$this->GetDatabaseName()} app\n";
            $script .= "* Filename: $filename\n";
            $script .= "* Created on " . date('l jS \of F Y h:i:s A') . "\n";
            $script .= "*/ \n";
            // first make the inherited classes\n";
            $script .= "// Write here your own code to change the\n";
            $script .= "// standard behaviour of the code generated\n";
            // php directive and namespace
            $script .= "namespace {$this->GetNamespace()}\\{$this->GetDatabaseName()};\n";
            // class Model
            $script .= "class Model";
            $script .= "\n";
            // begin class bll code block
            $script .= "\t{\n";
            foreach ($this->catalog as $table) {
                // onthoud met welke tabel we bezig zijn
                $this->dTable = $table;
                $fileName = "{$this->GetNamespaceToLower()}/{$this->GetDatabaseNameToLower()}/src/Model/CodeBehind/{$this->dTable->GetNameUCFirst()}.php";
                $script .= "\tprotected \${$this->dTable->GetNameLCFirst()};\n";
                $script .= "\tpublic function get{$this->dTable->GetNameUCFirst()}()\n";
                $script .= "\t{\n";
                $script .= "\t\treturn \$this->{$this->dTable->GetNameLCFirst()};\n";
                $script .= "\t}\n\n";
                $script .= "\tpublic function set{$this->dTable->GetNameUCFirst()}(\$value)\n";
                $script .= "\t{\n";
                $script .= "\t\t\$this->{$this->dTable->GetNameLCFirst()} = \$value;\n";
                $script .= "\t}\n\n";
            };
            $script .= "\tfunction __construct(\$log)\n";
            $script .= "\t{\n";
            $script .= "\t\t\$this->log = \$log;\n";
            $script .= "\t}\n\n";

            $script .= "\tfunction __destruct()\n";
            $script .= "\t{\n";
            $script .= "\t}\n\n";
            $script .= "}\n";
            $script .= "\n";    
            return $script;
        }

        /** ------------------ makeScriptModelClass  --------------------------
        *
        * Create Model class for one table
        * @lastmodified 21/01/2015
        * @since 01/06/2012           
        * @author Jef Inghelbrecht - An Orm Apart       
        * @version 0.2
        * @return string  
        */
        public function makeScriptModelClass($fileName)
        {
            $script = "<?php\n";
            $script .= "\t/* modernways.be\n\t * created by an orm apart\n";
            $script .= "\t * Entreprise de modes et de manières modernes\n";
            $script .= "\t * Model for {$this->GetDatabaseName()} app\n";
            $script .= "\t * Filename: $fileName\n";
            $script .= "\t * Created on " . date('l jS \of F Y h:i:s A') . "\n\t*/ \n";
            // first make the inherited classes\n";
            $script .= "\t// Write here your own code to change the\n";
            $script .= "\t// standard behaviour of the code generated\n";
            // php directive and namespace
            $script .= "\tnamespace {$this->GetNamespace()}\\{$this->GetDatabaseName()}\\Model;\n";
            // class Model
            $script .= "\tclass {$this->dTable->GetNameUCFirst()}";
            // use always base class
            $script .= " extends \\{$this->GetNamespace()}\\{$this->GetDatabaseName()}\\Model\\CodeBehind\\{$this->dTable->GetNameUCFirst()}";
            $script .= "\n";
            // begin class bll code block
            $script .= "\t{\n";
            $script .= "\t}\n";
            $script .= "\n";    
            return $script;
        }

        /** ------------------ zipAddScriptModelCodebehindClasses  --------------------------
        *
        * Make Model scripts for all tables in database for PHP
        * @lastmodified 20/01/2015
        * @since 01/06/2012           
        * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M        
        * @version 0.2
        * @return string  
        */        
        public function zipAddScriptModelCodebehindClasses($zip)
        {
              $script = '';
              if ($this->CatalogExists())
              {
                   foreach ($this->catalog as $table)
                   {
                        // onthoud met welke tabel we bezig zijn
                       $this->dTable = $table;
                       $fileName = "{$this->GetNamespaceToLower()}/{$this->GetDatabaseNameToLower()}/src/Model/CodeBehind/{$this->dTable->GetNameUCFirst()}.php";
                       $zip->addFromString($fileName, $this->makeScriptModelCodebehindClass($fileName));    
                   }
                   return true;
               }
               return false;
        }
    
        /** ------------------ makeScriptModelCodebehindClasses  --------------------------
        *
        * Make Model scripts for all tables in database for PHP
        * @lastmodified 20/01/2015
        * @since 01/06/2012           
        * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M        
        * @version 0.2
        * @return string  
        */        
        public function makeScriptModelCodebehindClasses()
        {
              $script = '';
              if ($this->CatalogExists())
              {
                   foreach ($this->catalog as $table)
                   {
                        // onthoud met welke tabel we bezig zijn
                       $this->dTable = $table;
                       $fileName = "{$this->GetNamespaceToLower()}/{$this->GetDatabaseNameToLower()}/src/Model/CodeBehind/{$this->dTable->GetNameUCFirst()}.php";
                       $script .= $this->makeScriptModelCodeBehindClass($fileName);
                   }
                   $script .= "\n";
               }
               return $script;
        }

        /** ------------------ makeScriptModelCodeBehindClass  --------------------------
        *
        * Create Code behing Model class for one table
        * @lastmodified 20/01/2015
        * @since 01/06/2012           
        * @author Jef Inghelbrecht - An Orm Apart       
        * @version 0.2
        * @return string  
        */
        public function makeScriptModelCodeBehindClass($fileName)
        {
            $script = "<?php\n";
            $script .= "\t/* modernways.be\n\t * created by an orm apart\n";
            $script .= "\t * Entreprise de modes et de manières modernes\n";
            $script .= "\t * Model for {$this->GetDatabaseName()} app\n";
            $script .= "\t * FileName: $fileName\n";
            $script .= "\t * Created on " . date('l jS \of F Y h:i:s A') . "\n\t*/ \n";
            // make the CodeBehind classes
            $script .= "\t// Code generated by An Orm Apart\n";
            $script .= "\t// do not modify the contents of this namespace\n";
            // php directive and namespace
            $script .= "\tnamespace {$this->GetNamespace()}\\{$this->GetDatabaseName()}\\Model\\CodeBehind;\n";
            // class Business logic layer
            $script .= "\tclass {$this->dTable->GetNameUCFirst()}";
            // use always base class
            $script .= " extends \\{$this->GetNamespace()}\Helpers\Model\Base";
            $script .= "\n";
            // begin class bll code block
            $script .= "\t{\n";
            $script .= $this->makeScriptCreateOne();
            $script .= $this->makeScriptDeleteOne();
            $script .= $this->makeScriptReadAll();
            $script .= $this->makeScriptReadOne();
            $script .= $this->makeScriptUpdateOne();
            // end class bll block
            $script .= "\t}\n";
            $script .= "\n";    
            return $script;
        }
    
        /** ------------------ makeScriptCreateOne  --------------------------
        *
        * Make script for Create One use case
        * @lastmodified 20/01/2015
        * @since 01/06/2012           
        * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M        
        * @version 0.2
        * @param string $method: if CreateOne no Id
        * @return string  
        */
        private function makeScriptCreateOne()
        {
            // CreateOne method
            $script = "\t\t// use case: {$this->dTable->GetNameToLower()}-create-one\n";
            $script .= "\t\tpublic function createOne(";
            // parameter list
            $parameterList = '';
            foreach ($this->dTable->GetRows() as $row)
            {
                if ($row->GetFieldName() != 'id')
                {
                    $parameterList .= "\${$row->GetFieldName()}, ";                
                }
            }
            $script .= rtrim($parameterList, ', ');
            $script .= ")\n";
            $script .= "\t\t{\n";
            foreach ($this->dTable->GetRows() as $row)
            {
                if ($row->GetFieldName() != 'id')
                {
                    $script .= "\t\t\t\$this->bdo->set{$row->GetColumnName()}(\${$row->GetFieldName()});\n";
                }
            }
            if ($this->dTable->IsExtend())
            {
                $script .= "\t\t\t\$this->bdo->setInsertedBy('JI');\n";
            }
            $script .= "\t\t\t\$this->dal->insert();\n";
            // end method block
            $script .= "\t\t}\n";
            return $script;
        }

        private function makeScriptDeleteOne()
        {
            // DeleteOne method
            $script = "\t\t// use case: {$this->dTable->GetNameToLower()}-delete-one\n";
            $script .= "\t\tpublic function deleteOne(\$id)\n";
            $script .= "\t\t{\n";
            $script .= "\t\t\t\$this->bdo->setId(\$id);\n";
            $script .= "\t\t\t\$this->dal->delete();\n";
            // end method block
            $script .= "\t\t}\n";
            return $script;
        }

        private function makeScriptReadAll()
        {
            // SelectAll method
            $script = "\t\t// use case: {$this->dTable->GetNameToLower()}-select-all\n";
            $script .= "\t\tpublic function readAll()\n";
            $script .= "\t\t{\n";
            $script .= "\t\t\t\$this->list = \$this->dal->selectAll();\n";
            // end method block
            $script .= "\t\t}\n";
            return $script;
        }
        private function makeScriptReadOne()
        {
            // DeleteOne method
            $script = "\t\t// use case: {$this->dTable->GetNameToLower()}-read-one\n";
            $script .= "\t\tpublic function readOne(\$id)\n";
            $script .= "\t\t{\n";
            $script .= "\t\t\t\$this->bdo->setId(\$id);\n";
            $script .= "\t\t\t\$this->dal->selectOne();\n";
            // end method block
            $script .= "\t\t}\n";
            return $script;
        }

        private function makeScriptUpdateOne()
        {
            // CreateOne method
            $script = "\t\t// use case: {$this->dTable->GetNameToLower()}-update-one\n";
            $script .= "\t\tpublic function updateOne(";
            // parameter list
            $parameterList = '';
            foreach ($this->dTable->GetRows() as $row)
            {
                $parameterList .= "\${$row->GetFieldName()}, ";                
            }
            $script .= rtrim($parameterList, ', ');
            $script .= ")\n";
            $script .= "\t\t{\n";
            foreach ($this->dTable->GetRows() as $row)
            {
                $script .= "\t\t\t\$this->bdo->set{$row->GetColumnName()}(\${$row->GetFieldName()});\n";
            }
            if ($this->dTable->IsExtend())
            {
                $script .= "\t\t\t\$this->bdo->setUpdatedBy('JI');\n";
            }
            $script .= "\t\t\t\$this->dal->update();\n";
            // end method block
            $script .= "\t\t}\n";
            return $script;
        }
    }
?>
