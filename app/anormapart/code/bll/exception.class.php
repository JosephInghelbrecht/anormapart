<?php

namespace AnOrmApart;
use exception; // buikd in class
// define MySQLException subclass
// The User defined Exception class is defined by extending the built-in Exception class. 
class AnOrmApartException extends \Exception
{
    private $exceptionType;
    private $error;

    function __construct($message,  $code, $fileName = 'log.dat')
    {
        // call parent of Exception class
        parent::__construct($message, $code);
        switch ($code) {
            case 0:
                $this->exceptionType='FileIO';
                break;
            case 1:
                $this->exceptionType='d2bHtmlSyntax';
                break;
            case 2:
                $this->exceptionType='ResultException';
                break;
            default:
                $this->exceptionType='Unknown Exception';
        }
        $date = getdate(date("U"));
        $fullDate = 

        $this->error = $date[weekday] . ', ' . $date[month] .
            $date[mday] .', ' . $date[year] . ' ----- Exception ' . $this->exceptionType . 
            ': ' . $this->getMessage() .
            ' --> in file: ' . $this->getFile() .
            ', line: ' . $this->getLine();
        // log error
        // call error_log():
        error_log($this->error . "\n", 3, $fileName);
    }

    public function ErrorMessage()
    {
        return $this->error;
    }
}


?>
