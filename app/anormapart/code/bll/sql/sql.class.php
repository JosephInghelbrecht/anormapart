<?php
/********************** MySql Catalog class ************************/
/**
 * Catalog clas
 * Generates SQL for MySQL
 *
 * You can use phpDocumentor to generate documentation
 *
 *
 * @lastmodified 3/3/2013
 * @since 01/06/2012
 * @author Jef Inghelbrecht - Entreprise de Modes et de Manières Modernes - e3M
 * @version 0.2
 */
namespace AnOrmApart\MySql;

class Catalog extends \AnOrmApart\Catalog
{

    private function scriptAnsiMode()
    {
        $script = '';
        if ($this->isMySql()) {
            $script .= "SET GLOBAL TRANSACTION ISOLATION LEVEL SERIALIZABLE;\n";
            $script .= "-- mode changes syntax and behavior to conform more closely to standard SQL.\n";
            $script .= "-- It is one of the special combination modes listed at the end of this section.\n";
            $script .= "SET GLOBAL sql_mode = 'ANSI';\n";
        }
        return $script;
    }

    private function scriptCreateDatabase()
    {
        if ($this->isMsSql()) {
            $script = "-- If database does not exist, create the database\n";
            $script .= "IF EXISTS (SELECT name FROM master.sys.databases WHERE name = N'{$this->GetDatabaseName()}')\n";
            $script .= "BEGIN\n";
            $script .= "\tDROP DATABASE {$this->GetDatabaseName()}\n";
            $script .= "END\n";
            $script .= "CREATE DATABASE {$this->GetDatabaseName()}\n";
            $script .= "GO\n";

        } elseif ($this->isMySql()) {
            $script = "-- If database does not exist, create the database\n";
            $script .= "CREATE DATABASE IF NOT EXISTS {$this->GetDatabaseName()};\n";
        }
        return $script;
    }

    private function scriptDisableConstraints()
    {
        if ($this->isMsSql()) {
            $script = "USE {$this->GetDatabaseName()}\n";
            $script .= "go\n";
            $script .= "EXEC sp_msforeachtable \"ALTER TABLE ? NOCHECK CONSTRAINT all\"\n\n";
            $script .= "GO\n";
        } elseif ($this->isMySql()) {
            $script = "-- With the MySQL FOREIGN_KEY_CHECKS variable,\n";
            $script .= "-- you don't have to worry about the order of your\n";
            $script .= "-- DROP and CREATE TABLE statements at all, and you can\n";
            $script .= "-- write them in any order you like, even the exact opposite.\n";
            $script .= "SET FOREIGN_KEY_CHECKS = 0;\n\n";
        }
        return $script;
    }

    private function scriptEnableConstraints()
    {
        if ($this->isMsSql()) {
            $script = "USE {$this->GetDatabaseName()}\n";
            $script .= "go\n";
            $script .= "EXEC sp_msforeachtable \"ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all\"\n\n";
            $script .= "GO\n";
        } elseif ($this->isMySql()) {
            $script = "-- With the MySQL FOREIGN_KEY_CHECKS variable,\n";
            $script .= "-- you don't have to worry about the order of your\n";
            $script .= "-- DROP and CREATE TABLE statements at all, and you can\n";
            $script .= "-- write them in any order you like, even the exact opposite.\n";
            $script .= "SET FOREIGN_KEY_CHECKS = 1;\n\n";
        }
        return $script;
    }

    private function scriptDropTable()
    {
        $script = '';
        if ($this->isMsSql()) {
            $script .= "-- Vooraleer de tabel {$this->dTable->getNameUCFirst()} te creëren, test als\n";
            $script .= "-- de tabel al bestaat. Als de tabel al bestaat\n";
            $script .= "-- moet je die eerst droppen\n";
            $script .= "IF EXISTS (SELECT * FROM sys.tables WHERE name='{$this->dTable->getNameUCFirst()}')\n";
            $script .= "BEGIN\n";
            $script .= "    DROP TABLE [{$this->dTable->getNameUCFirst()}]\n";
            $script .= "END\n";
            $script .= "GO\n";
        } elseif ($this->isMySql()) {
            $script .= "DROP TABLE IF EXISTS $this->escape{$this->dTable->getNameUCFirst()}$this->escape;\n";
        }
        return $script;
    }

    private function scriptDropStoredProcedure($name)
    {
        $script = '';
        if ($this->isMsSql()) {
            $script .= "-- Vooraleer de stored procedure te creëren, test als\n";
            $script .= "-- de die al bestaat. Als de SP al bestaat\n";
            $script .= "-- moet je die eerst droppen\n";
            $script .= "IF EXISTS (SELECT 1 FROM sys.procedures WHERE object_id = OBJECT_ID(N'{$name}'))\n";
            $script .= "BEGIN\n";
            $script .= "    DROP PROCEDURE {$name}\n";
            $script .= "END\n";
            $script .= "GO\n";
        } elseif ($this->isMySql()) {
            $script .= "DROP PROCEDURE IF EXISTS {$name};\n";
        }
        return $script;
    }

    private function scriptPrimaryKey($row)
    {
        $script = '';
        if ($this->isMsSql()) {
            $script .= " IDENTITY(1, 1),\n";
            $script .= "\tCONSTRAINT pk_{$this->dTable->getNameUCFirst()}_{$row->getColumnName()} PRIMARY KEY(" . $row->getColumnName() . ')';
        } elseif ($this->isMySql()) {
            $script .= " AUTO_INCREMENT,\n";
            $script .= "\tCONSTRAINT PRIMARY KEY(" . $this->escape . $row->getColumnName() . $this->escape . ')';
        }
        return $script;
    }

    private function scriptForeignKey($row)
    {
        $script = '';
        if ($this->isMsSql()) {
            // use alias to allow multiple instances same table
            $script .= "$this->escape{$row->getColumnName()}-{$row->getReferenceTable()}$this->escape.$this->escape{$row->GetReferenceDisplayColumn()}$this->escape as ";
            $script .= "{$row->getColumnName()}{$row->GetReferenceDisplayColumn()}, ";
        } elseif ($this->isMySql()) {
            $script .= " AUTO_INCREMENT,\n";
            $script .= "\tCONSTRAINT PRIMARY KEY(" . $row->getColumnName() . ')';
        }
        return $script;
    }

    /** ------------------ DDL MySQL --------------------------
     *
     * Make CREATE TABLE script
     * @return string
     */
    private function GetCreateTableSqlScript()
    {
        $script = '';
        $fk = '';
        $uc = ''; // unique constraint
        $script .= "-- modernways.be\n-- created by an orm apart\n";
        $script .= "-- Entreprise de modes et de manières modernes\n";
        $script .= "-- MySql: CREATE TABLE {$this->dTable->getNameUCFirst()}\n";
        $script .= "-- Created on " . date('l jS \of F Y h:i:s A') . "\n-- \n";
        // $script .= "USE $this->escape{$this->GetDatabaseName()}$this->escape;\n";
        $script .= $this->scriptDropTable();
        $script .= "CREATE TABLE $this->escape{$this->dTable->getNameUCFirst()}$this->escape (\n";
        // echo '<pre>';
        // var_dump($script);
        // echo '</pre>';
        // return;
        foreach ($this->dTable->getRows() as $row) {
            // prepare for unique constraints
            if ($row->isUnique() && !$row->isPrimaryKey()) {
                $uc .= "\tCONSTRAINT uc_{$this->dTable->getNameUCFirst()}_{$row->getColumnName()} UNIQUE ({$row->getColumnName()}),\n";
            }
            // prepare foreignkey constraints
            if ($row->isForeignKey()) {
                $fk .= "\tCONSTRAINT fk_{$this->dTable->getNameUCFirst()}{$row->getColumnName()} ";
                $fk .= "FOREIGN KEY ($this->escape{$row->getColumnName()}$this->escape) REFERENCES $this->escape";
                $fk .= "{$row->getReferenceTable()}$this->escape ($this->escape{$row->GetReferenceColumn()}$this->escape),\n";
            }
            // add row declaration
            $script .= "\t$this->escape{$row->getColumnName()}$this->escape {$row->getSqlDataType()}";
            $script .= ($row->isRequired() ? 'NOT NULL' : 'NULL');

            if ($row->isPrimaryKey()) {
                $script .= $this->scriptPrimaryKey($row);

            }
            $script .= ",\n";
        }
        $script .= $uc; // add named unique contraints
        $script .= $fk; // add named foreign key constraints
        $script = rtrim($script, ",\n");
        $script .= ");\n\n";
        return $script;
    }

    public function GetCreateTableSqlScriptAll()
    {
        $script = "-- An Orm Apart -- " . date('l jS \of F Y h:i:s A') . "\n-- \n";
        if ($this->CatalogExists()) {
            $script .= $this->scriptAnsiMode();
            $script .= $this->scriptCreateDatabase();
            // print_r($this->catalog);
            $script .= "USE $this->escape{$this->GetDatabaseName()}$this->escape;\n";
            $script .= $this->scriptDisableConstraints();
            foreach ($this->catalog as $table) {
                // onthoud met welke tabel we bezig zijn
                $this->dTable = $table;
                $script .= $this->GetCreateTableSqlScript();
             }
            $script .= $this->scriptEnableConstraints();
        }
        return $script;
    }

    /*******************  Data Manipulation language scripts **************************
     *******************           Helper methods            **************************
     *
     * /**
     * Create inner joins for foreign keys
     * @return string
     */
    private function getInnerJoin($isList = false)
    {
        $result = '';
        foreach ($this->dTable->getRows() as $row) {
            if (($isList ? $row->isList() : true) && $row->isForeignKey()) {
                // add alias to allow multiple instances of same table
                // for different foreign keys
                $result .= "\tINNER JOIN $this->escape{$row->getReferenceTable()}$this->escape ";
                $result .= "as {$this->escape}Join{$row->getReferenceTable()}On{$row->getColumnName()}$this->escape\n";
                $result .= "\t\tON $this->escape{$this->dTable->getNameUCFirst()}$this->escape.$this->escape{$row->getColumnName()}$this->escape = ";
                $result .= "{$this->escape}Join{$row->getReferenceTable()}On{$row->getColumnName()}$this->escape.$this->escape{$row->getReferenceColumn()}$this->escape\n";
            }
        }
        // return rtrim($result, "\n");
        return $result;
    }

    /**
     * Create select parameterlist, only lsit columns
     * or SelectBy and SelectAll procedures, not for SelectOne
     * @return select by parameters as string
     */
    private function getSelectParameterList($isList = true)
    {
        $result = '';
        foreach ($this->dTable->getRows() as $row) {
            if (($isList ? $row->isList() : true) || $row->isPrimaryKey()) {
                if ($row->isForeignKey()) {
                    // use alias to allow multiple instances same table
                    // the alias is composed of foreignkey, a dash, followed by the reference table
                    $result .= "{$this->escape}Join{$row->getReferenceTable()}On{$row->getColumnName()}$this->escape.$this->escape{$row->getReferenceDisplayColumn()}$this->escape as ";
                    $result .= "{$this->escape}{$row->getReferenceTable()}{$row->getReferenceDisplayColumn()}$this->escape,\n\t";
                }
                $result .= "$this->escape{$this->dTable->getNameUCFirst()}$this->escape.$this->escape{$row->getColumnName()}$this->escape,\n\t";
            }
        }
        // remove last ",\n"
        $result = rtrim($result, ",\n\t");
        $result .= "\n";
        return $result;
    }

    /**
     * Create insert parameterlist for one table as a string
     * @return insert parameters as string
     */
    private function getParameterList()
    {
        $result = '';
        foreach ($this->dTable->getRows() as $row) {
            if (!$row->isPrimaryKey()) {
                $result .= "\tIN p" . $row->getColumnName() . ' ' . $row->getSqlDataType() .
                    ",\n";
            }
        }
        // remove last ",\n"
        $result = rtrim($result, ",\n");
        $result .= "\n";
        return $result;
    }

    /**
     * Create insert/update tablecolumns for one table as a string
     * @return insert tablecolumns as string
     */
    private function getColumnNameList()
    {
        $result = '';
        foreach ($this->dTable->getRows() as $row) {
            if (!$row->isPrimaryKey()) {
                $result .= "\t\t$this->escape{$this->dTable->getNameUCFirst()}$this->escape.$this->escape{$row->getColumnName()}$this->escape,\n";
            }
        }
        // remove last ",]\n"
        $result = rtrim($result, ",\n");
        $result .= "\n";
        return $result;
    }

    /**
     * Create valuelist for one table as a string
     * @return insert values as string
     */
    private function getColumnValueList()
    {
        $result = '';
        foreach ($this->dTable->getRows() as $row) {
            if (!$row->isPrimaryKey()) {
                $result .= "\t\tp{$row->getColumnName()},\n";
            }
        }
        // remove last ",\n"
        $result = rtrim($result, ",\n");
        $result .= "\n";
        return $result;
    }

    /**
     * Create MySQL script for CRUD
     * @return  string
     */
    public function getDMLAll()
    {
        if ($this->CatalogExists()) {
            $code = "USE {$this->GetDatabaseName()};\n";
            // print_r($this->catalog);
             foreach ($this->catalog as $table) {
                // onthoud met welke tabel we bezig zijn
                $this->dTable = $table;
                $code .= $this->Insert();
                $code .= $this->Update();
                $code .= $this->Delete();
                // eigenlijk mag die weg, het volstaat om bij Id list op true te zetten
                // dat geldt ook voor stored procedure
                $code .= $this->SelectOne();
                $code .= $this->SelectAll();
                //$code .= $this->Count();
                //$code .= $this->CountBy();
                $code .= $this->SelectBy();
                $code .= $this->SelectLike();
                $code .= $this->SelectLikeX();
            }
        }
        return $code;
    }

    /**
     * Create stored procedure insert for one table as a string
     * @return stored procedure insert for one table as string
     */
    private function insert()
    {
        $script = "-- modernways.be\n-- created by an orm apart\n";
        $script .= "-- Entreprise de modes et de manières modernes\n";
        $script .= "-- MySql DML\n";
        $script .= '-- Created : ' . date('l jS \of F Y h:i:s A') . "\n";
        $script .= "-- DML Insert Stored Procedure for {$this->dTable->getNameUCFirst()} \n-- \n";
        $script .= $this->scriptDropStoredProcedure($this->dTable->getNameUCFirst() . 'Insert');
        $script .= ($this->isMySQL() ? "DELIMITER //\n" : '');
        $script .= "CREATE PROCEDURE $this->escape{$this->dTable->getNameUCFirst()}Insert$this->escape\n";
        $script .= "(\n";
        /*
       // parameterlist, no parameter needed for columns with SP- prefix in
       // Default Value column, because they are assigned in the
       // stored procedure itself
       // If function name is UPDATE only, no parameter
       */
        foreach ($this->dTable->getRows() as $row) {
            if ($row->isPrimaryKey()) {
                if ($this->isMsSql()) {
                    $script .= "\t@" . $row->getColumnName() . ' ' . $row->getSqlDataType() . " output,\n";
                } else {
                    $script .= "\tOUT p" . $row->getColumnName() . ' ' . $row->getSqlDataType() . ",\n";
                }
            } elseif (!($row->isSetByStoredProcedure()) and
                ($row->getDefaultValueFunctionName() != 'UPDATE')
            ) {
                if ($this->isMsSql()) {
                    $script .= "\t@" . $row->getColumnName() . ' ' . $row->getSqlDataType() . ",\n";
                } else {
                    $script .= "\tIN p" . $row->getColumnName() . ' ' . $row->getSqlDataType() . ",\n";
                }
            }
        }
        // remove last ",\n"
        $script = rtrim($script, ",\n");
        // add new line
        $script .= "\n";

        $script .= ")\n";
        $script .= ($this->isMsSql() ? "AS\n" : '');
        $script .= "BEGIN\n";
        $script .= "INSERT INTO $this->escape{$this->dTable->getNameUCFirst()}$this->escape\n";
        $script .= "\t(\n";
        /*
            list of column names
        */
        // $script .= $row->GetDefaultValueCrudMethod();
        // $script .= 'funtion' . $row->getDefaultValueFunctionName();
        foreach ($this->dTable->getRows() as $row) {
            // only if not pk, and not reserved for update method
            if (!$row->isPrimaryKey() and ($row->getDefaultValueCrudMethod() != 'UPDATE') and
                ($row->getDefaultValueFunctionName() != 'UPDATE')
            ) {
                $script .= "\t\t$this->escape{$this->dTable->getNameUCFirst()}$this->escape.$this->escape{$row->getColumnName()}$this->escape,\n";
            }
        }
        // remove last ",\n"
        $script = rtrim($script, ",\n");
        $script .= "\n";

        $script .= "\t)\n";
        $script .= "\tVALUES\n";
        $script .= "\t(\n";
        foreach ($this->dTable->getRows() as $row) {
            if (!$row->isPrimaryKey()) {
                if ($row->isSetByStoredProcedure()) {
                    if ($row->getDefaultValueCrudMethod() == 'INSERT') {
                        $parameter = $row->getDefaultValueFunctionParameterName();
                        if ($parameter == 'NOW()') {
                            if ($this->isMsSql()) {
                                $parameter = 'GETDATE()';
                            }
                        }
                        $script .= "\t\t{$parameter},\n";
                    }
                } elseif ($row->getDefaultValueFunctionName()) {
                    if ($row->getDefaultValueFunctionName() == 'INSERT') {
                        $script .= "\t\t" . ($this->isMsSql() ? '@' : 'p') . "{$row->getColumnName()},\n";
                    }
                } else {
                    $script .= "\t\t" . ($this->isMsSql() ? '@' : 'p') . "{$row->getColumnName()},\n";
                }
            }
        }
        // remove last ",\n"
        $script = rtrim($script, ",\n");
        $script .= "\n";


        $script .= "\t);\n";
        if ($this->isMsSql()) {
            $script .= "\tset @Id = SCOPE_IDENTITY();\n";
        } else {
            $script .= "\t-- return the Id of the inserted row\n";
            $script .= "\tSELECT LAST_INSERT_ID() INTO pId;\n";
        }
        $script .= "END " . ($this->isMySql() ? '//' : '') . "\n";
        $script .= ($this->isMySQL() ? 'DELIMITER ;' : 'GO');
        $script .= "\n\n";
        return $script;
    }

    /**
     * Create stored procedure update for one table
     * @return string
     */
    private function update()
    {
        $script = "-- modernways.be\n-- created by an orm apart\n";
        $script .= "-- Entreprise de modes et de manières modernes\n";
        $script .= "-- MySql DML\n";
        $script .= '-- Created : ' . date('l jS \of F Y h:i:s A') . "\n";
        $script .= "-- DML Update Stored Procedure for {$this->dTable->getNameUCFirst()}\n-- \n";
        $script .= $this->scriptDropStoredProcedure($this->dTable->getNameUCFirst() . 'Update');
        $script .= ($this->isMySQL() ? "DELIMITER //\n" : '');
        $script .= "CREATE PROCEDURE $this->escape{$this->dTable->getNameUCFirst()}Update$this->escape\n";
        $script .= "(\n";

        /*
        // parameterlist, no parameter needed for columns with SP- prefix in
        // Default Value column, because they are assigned in the
        // stored procedure itself
        // If function name is INSERT only, no parameter
        */
        foreach ($this->dTable->getRows() as $row) {
            if (!($row->isSetByStoredProcedure()) and
                ($row->getDefaultValueFunctionName() != 'INSERT')
            ) {
                $script .= "\t" . ($this->isMsSql() ? '@' : 'p') . "{$row->getColumnName()} {$row->getSqlDataType()},\n";
            }
        }
        // remove last ",\n"
        $script = rtrim($script, ",\n");
        // add new line
        $script .= "\n";

        $script .= ")\n";
        $script .= ($this->isMsSql() ? "AS\n" : '');
        $script .= "BEGIN\n";
        $script .= "UPDATE $this->escape{$this->dTable->getNameUCFirst()}$this->escape\n";
        $script .= "\tSET\n";

        // we gaan ervan uit de dat de primarykey kolomnaam Id is.
        $primaryKeyRow = $this->dTable->getRows()[0];
        // Create set valuelist for update stored procedure for one table
        foreach ($this->dTable->getRows() as $row) {
            if ($row->isPrimaryKey()) {
                $primaryKeyRow = $row;
            } else {
                if ($row->isSetByStoredProcedure()) {
                    if ($row->getDefaultValueCrudMethod() == 'UPDATE') {
                        $script .= "\t\t$this->escape{$row->getColumnName()}$this->escape = ";
                        $parameter = $row->getDefaultValueFunctionParameterName();
                        if ($parameter == 'NOW()') {
                            if ($this->isMsSql()) {
                                $parameter = 'GETDATE()';
                            }
                        }
                        $script .= "{$parameter},\n";
                    }
                } elseif ($row->getDefaultValueFunctionName()) {
                    if ($row->getDefaultValueFunctionName() == 'UPDATE') {
                        $script .= "\t\t$this->escape{$row->getColumnName()}$this->escape = ";
                        $script .= ($this->isMsSql() ? '@' : 'p') . "{$row->getColumnName()},\n";
                        // $script .= "\t\t$this->escape{$row->getColumnName()}$this->escape = p{$row->getColumnName()},\n";
                    }
                } else {
                    $script .= "\t\t$this->escape{$row->getColumnName()}$this->escape = ";
                    $script .= ($this->isMsSql() ? '@' : 'p') . "{$row->getColumnName()},\n";
                }
            }
        }
        // remove last ",\n"
        $script = rtrim($script, ",\n");
        $script .= "\n";

        $script .= "\tWHERE {$this->escape}{$this->dTable->getNameUCFirst()}{$this->escape}.{$this->escape}Id{$this->escape} = ";
        $script .= ($this->isMsSql() ? '@' : 'p') . "{$primaryKeyRow->getColumnName()};\n";
        $script .= "END " . ($this->isMySql() ? '//' : '') . "\n";
        $script .= ($this->isMySQL() ? 'DELIMITER ;' : 'GO');
        $script .= "\n\n";
        return $script;
    }

    /**
     * Create stored procedure delete for one table
     * @return string
     */
    private function delete()
    {
        // we gaan ervan uit de dat de primarykey kolomnaam Id is.
        $primaryKeyRow = $this->dTable->getRows()[0];
        foreach ($this->dTable->getRows() as $row) {
            if ($row->isPrimaryKey()) {
                $primaryKeyRow = $row;
            }
        }
        $script = "-- modernways.be\n-- created by an orm apart\n";
        $script .= "-- Entreprise de modes et de manières modernes\n";
        $script .= "-- MySql DML\n";
        $script .= '-- Created : ' . date('l jS \of F Y h:i:s A') . "\n";
        $script .= "-- DML Delete Stored Procedure for {$this->dTable->getNameUCFirst()} \n-- \n";
        $script .= $this->scriptDropStoredProcedure($this->dTable->getNameUCFirst() . 'Delete');
        $script .= ($this->isMySQL() ? "DELIMITER //\n" : '');
        $script .= "CREATE PROCEDURE $this->escape{$this->dTable->getNameUCFirst()}Delete$this->escape\n";
        $script .= "(\n";
        $script .= "\t " . ($this->isMsSql() ? '@' : 'p') . "{$primaryKeyRow->getColumnName()} {$primaryKeyRow->getSqlDataType()}\n";
        $script .= ")\n";
        $script .= ($this->isMsSql() ? "AS\n" : '');
        $script .= "BEGIN\n";
        $script .= "DELETE FROM $this->escape{$this->dTable->getNameUCFirst()}$this->escape\n";
        $script .= "\tWHERE {$this->escape}{$this->dTable->getNameUCFirst()}{$this->escape}.{$this->escape}{$primaryKeyRow->getColumnName()}{$this->escape} = ";
        $script .= ($this->isMsSql() ? '@' : 'p') . "{$primaryKeyRow->getColumnName()};\n";

        $script .= "END " . ($this->isMySql() ? '//' : '') . "\n";
        $script .= ($this->isMySQL() ? 'DELIMITER ;' : 'GO');
        $script .= "\n\n";
        return $script;
    }

    /**
     * Create stored procedure selectone for one table
     * @return string
     */
    // eigenlijk mag die weg, het volstaat om bij Id list op true te zetten
    // dat geldt ook voor stored procedure
    private function selectOne()
    {
        // we gaan ervan uit de dat de primarykey kolomnaam Id is.
        $primaryKeyRow = $this->dTable->getRows()[0];
        foreach ($this->dTable->getRows() as $row) {
            if ($row->isPrimaryKey()) {
                $primaryKeyRow = $row;
            }
        }
        $script = "-- modernways.be\n-- created by an orm apart\n";
        $script .= "-- Entreprise de modes et de manières modernes\n";
        $script .= "-- MySql DML\n";
        $script .= '-- Created : ' . date('l jS \of F Y h:i:s A') . "\n";
        $script .= "-- DML SelectOne Stored Procedure for {$this->dTable->getNameUCFirst()} \n-- \n";
        $script .= $this->scriptDropStoredProcedure($this->dTable->getNameUCFirst() . 'SelectOne');
        $script .= ($this->isMySQL() ? "DELIMITER //\n" : '');
        $script .= "CREATE PROCEDURE $this->escape{$this->dTable->getNameUCFirst()}SelectOne$this->escape\n";
        $script .= "(\n";
        $script .= "\t " . ($this->isMsSql() ? '@' : 'p') . "{$primaryKeyRow->getColumnName()} {$primaryKeyRow->getSqlDataType()}\n";
        $script .= ")\n";
        $script .= ($this->isMsSql() ? "AS\n" : '');
        $script .= "BEGIN\n";
        $script .= "SELECT {$this->getSelectParameterList(false)} \n";
        $script .= "FROM $this->escape{$this->dTable->getNameUCFirst()}$this->escape\n";
        $script .= $this->getInnerJoin(false);
        $script .= "\tWHERE {$this->escape}{$this->dTable->getNameUCFirst()}{$this->escape}.{$this->escape}{$primaryKeyRow->getColumnName()}{$this->escape} = ";
        $script .= ($this->isMsSql() ? '@' : 'p') . "{$primaryKeyRow->getColumnName()};\n";
        $script .= "END " . ($this->isMySql() ? '//' : '') . "\n";
        $script .= ($this->isMySQL() ? 'DELIMITER ;' : 'GO');
        $script .= "\n\n";
        return $script;
    }

    /**
     * Create stored procedure count for one table
     * @return string
     */
    private function Count()
    {
        $script = "-- modernways.be\n-- created by an orm apart\n";
        $script .= "-- Entreprise de modes et de manières modernes\n";
        $script .= "-- MySql DML\n";
        $script .= '-- Created : ' . date('l jS \of F Y h:i:s A') . "\n";
        $script .= "-- DML Count Stored Procedure for table {$this->dTable->getNameUCFirst()} \n-- \n";
        $script .= $this->scriptDropStoredProcedure($this->dTable->getNameUCFirst() . 'Count');
        $script .= ($this->isMySQL() ? "DELIMITER //\n" : '');
        $script .= "CREATE PROCEDURE $this->escape{$this->dTable->getNameUCFirst()}Count$this->escape\n";
        $script .= ($this->isMsSql() ? '' : "(\n");
        $script .= ($this->isMsSql() ? '' : ")\n");
        $script .= ($this->isMsSql() ? "AS\n" : '');
        $script .= "BEGIN\n";
        $script .= "\tSELECT count(*) FROM $this->escape{$this->dTable->getNameUCFirst()}$this->escape;\n";
        $script .= "END " . ($this->isMySql() ? '//' : '') . "\n";
        $script .= ($this->isMySQL() ? 'DELIMITER ;' : 'GO');
        $script .= "\n\n";
        return $script;
    }


    /**
     * Create stored procedure selectall for one table
     * selects only the list columns
     * @return string
     */
    private function SelectAll()
    {
        $script = "-- modernways.be\n-- created by an orm apart\n";
        $script .= "-- Entreprise de modes et de manières modernes\n";
        $script .= "-- MySql DML\n";
        $script .= '-- Created : ' . date('l jS \of F Y h:i:s A') . "\n";
        $script .= "-- DML SelectAll Stored Procedure for table {$this->dTable->getNameUCFirst()} \n-- \n";
        $script .= $this->scriptDropStoredProcedure($this->dTable->getNameUCFirst() . 'SelectAll');
        $script .= ($this->isMySQL() ? "DELIMITER //\n" : '');
        $script .= "CREATE PROCEDURE $this->escape{$this->dTable->getNameUCFirst()}SelectAll$this->escape\n";
        $script .= ($this->isMsSql() ? '' : "(\n");
        $script .= ($this->isMsSql() ? '' : ")\n");
        $script .= ($this->isMsSql() ? "AS\n" : '');
        $script .= "BEGIN\n";
        $script .= "SELECT {$this->getSelectParameterList()}";
        $script .= "\tFROM $this->escape{$this->dTable->getNameUCFirst()}$this->escape\n";
        $script .= $this->getInnerJoin();
        $orderBy = str_replace(' ', '', $this->dTable->getOrderBy());
        $orderBy = str_replace('.', "$this->escape.$this->escape", $orderBy);
        $orderBy = str_replace(',', "$this->escape,$this->escape", $orderBy);
        $script .= ($this->dTable->getOrderBy() ? "\tORDER BY $this->escape{$orderBy}$this->escape" : '');
        $script .= ";\n";
        $script .= "END " . ($this->isMySql() ? '//' : '') . "\n";
        $script .= ($this->isMySQL() ? 'DELIMITER ;' : 'GO');
        $script .= "\n\n";
        return $script;
    }

    /**
     * Create stored procedure SelectBy columnname for one table
     * selects only the searchable columns
     * @return string
     */
    private function SelectBy()
    {
        $script = '';

        foreach ($this->dTable->getRows() as $row) {
            // create for each searchable column a SelectBy Stored Procedure
            // select only searchable columns and of course for the
            // primary key colomn and the foreign key column
            if ($row->isSelectBy() || $row->isPrimaryKey() || $row->isForeignKey()) {
                $script .= "-- modernways.be\n-- created by an orm apart\n";
                $script .= "-- Entreprise de modes et de manières modernes\n";
                $script .= "-- MySql DML\n";
                $script .= '-- Created : ' . date('l jS \of F Y h:i:s A') . "\n";
                $script .= "-- DML SelectBy{$row->getColumnName()} Stored Procedure for table {$this->dTable->getNameUCFirst()}\n-- \n";
                $script .= $this->scriptDropStoredProcedure($this->dTable->getNameUCFirst() . "SelectBy{$row->getColumnName()}");
                $script .= ($this->isMySQL() ? "DELIMITER //\n" : '');
                $script .= "CREATE PROCEDURE $this->escape{$this->dTable->getNameUCFirst()}SelectBy{$row->getColumnName()}$this->escape\n";
                $script .= "(\n";
                $script .= "\t " . ($this->isMsSql() ? '@' : 'p') . "{$row->getColumnName()} {$row->getSqlDataType()}\n";
                $script .= ")\n";
                $script .= ($this->isMsSql() ? "AS\n" : '');
                $script .= "BEGIN\n";
                $script .= "SELECT {$this->getSelectParameterList()}\n";
                $script .= "\tFROM $this->escape{$this->dTable->getNameUCFirst()}$this->escape\n";
                $script .= $this->getInnerJoin();
                // $script .= "\n";
                $script .= "\tWHERE $this->escape{$this->dTable->getNameUCFirst()}$this->escape.$this->escape{$row->getColumnName()}$this->escape = ";
                $script .= ($this->isMsSql() ? '@' : 'p') . $row->getColumnName();
                $script .= ($row->getOrderBy() ? "\n\tORDER BY $this->escape{$this->dTable->getNameUCFirst()}$this->escape.$this->escape{$row->getColumnName()}$this->escape" : '');
                $script .= ";\n";
                $script .= "END " . ($this->isMySql() ? '//' : '') . "\n";
                $script .= ($this->isMySQL() ? 'DELIMITER ;' : 'GO');
                $script .= "\n\n";
            }
        }
        return $script;
    }


    /**
     * Create stored procedure SelectBy Like columnname for one table
     * selects only the searchable columns with Like
     * @return string
     */
    private function SelectLike()
    {
        $script = '';

        foreach ($this->dTable->getRows() as $row) {
            // create for each searchable column a SelectLike Stored Procedure
            // select only searchable columns
            // but not for foreign keys, these are integers and that
            // doesn't lake sense
            if ($row->IsSelectLike() and !$row->isForeignKey()) {
                $script .= "-- modernways.be\n-- created by an orm apart\n";
                $script .= "-- Entreprise de modes et de manières modernes\n";
                $script .= "-- MySql DML\n";
                $script .= '-- Created : ' . date('l jS \of F Y h:i:s A') . "\n";
                $script .= "-- DML SelectLike{$row->getColumnName()} Stored Procedure for table {$this->dTable->getNameUCFirst()} \n-- \n";
                $script .= $this->scriptDropStoredProcedure($this->dTable->getNameUCFirst() . "SelectLike{$row->getColumnName()}");
                $script .= ($this->isMySQL() ? "DELIMITER //\n" : '');
                $script .= "CREATE PROCEDURE $this->escape{$this->dTable->getNameUCFirst()}SelectLike{$row->getColumnName()}$this->escape\n";
                $script .= "(\n";
                $script .= "\t" . ($this->isMsSql() ? '@' : 'p') . "{$row->getColumnName()} {$row->getSqlDataType()}\n";
                $script .= ")\n";
                $script .= ($this->isMsSql() ? "AS\n" : '');
                $script .= "BEGIN\n";
                $script .= "SELECT {$this->getSelectParameterList()} \n";
                $script .= "\tFROM $this->escape{$this->dTable->getNameUCFirst()}$this->escape\n";
                $script .= $this->getInnerJoin();
                // $script .= "\n";
                $script .= "\tWHERE $this->escape{$this->dTable->getNameUCFirst()}$this->escape.$this->escape{$row->getColumnName()}$this->escape ";
                $script .= "like CONCAT(" . ($this->isMsSql() ? '@' : 'p') . "{$row->getColumnName()}, '%')";
                $script .= ($row->getOrderBy() ? "\n\tORDER BY $this->escape{$this->dTable->getNameUCFirst()}$this->escape.$this->escape{$row->getColumnName()}$this->escape" : '');
                $script .= ";\n";
                $script .= "END " . ($this->isMySql() ? '//' : '') . "\n";
                $script .= ($this->isMySQL() ? 'DELIMITER ;' : 'GO');
                $script .= "\n\n";
            }
        }
        return $script;
    }

    /**
     * Create stored procedure SelectBy Like eXentend (voor en na)
     * columnname for one table
     * selects only the searchable columns with Like
     * @return string
     */
    private function SelectLikeX()
    {
        $script = '';

        foreach ($this->dTable->getRows() as $row) {
            // create for each searchable column a SelectBy Stored Procedure
            // select only searchable columns
            // but not for foreign keys, these are integers and that
            // doesn't lake sense
            if ($row->isSelectLikeX() and !$row->isForeignKey()) {
                $script .= "-- modernways.be\n-- created by an orm apart\n";
                $script .= "-- Entreprise de modes et de manières modernes\n";
                $script .= "-- MySql DML\n";
                $script .= '-- Created : ' . date('l jS \of F Y h:i:s A') . "\n";
                $script .= "-- DML SelectLikeX{$row->getColumnName()} Stored Procedure for table {$this->dTable->getNameUCFirst()}\n-- \n";
                $script .= $this->scriptDropStoredProcedure($this->dTable->getNameUCFirst() . "SelectLikeX{$row->getColumnName()}");
                $script .= ($this->isMySQL() ? "DELIMITER //\n" : '');
                $script .= "CREATE PROCEDURE $this->escape{$this->dTable->getNameUCFirst()}SelectLikeX{$row->getColumnName()}$this->escape\n";
                $script .= "(\n";
                $script .= "\t" . ($this->isMsSql() ? '@' : 'p') . "{$row->getColumnName()} {$row->getSqlDataType()}\n";
                $script .= ")\n";
                $script .= ($this->isMsSql() ? "AS\n" : '');
                $script .= "BEGIN\n";
                $script .= "\tSELECT {$this->getSelectParameterList()}\n";
                $script .= "\tFROM $this->escape{$this->dTable->getNameUCFirst()}$this->escape\n";
                $script .= $this->getInnerJoin();
                // $script .= "\n";
                $script .= "\tWHERE $this->escape{$this->dTable->getNameUCFirst()}$this->escape.$this->escape{$row->getColumnName()}$this->escape ";
                $script .= "like CONCAT('%', " . ($this->isMsSql() ? '@' : 'p') . "{$row->getColumnName()}, '%')\n";
                $script .= ($row->getOrderBy() ? "\n\tORDER BY $this->escape{$this->dTable->getNameUCFirst()}$this->escape.$this->escape{$row->getColumnName()}$this->escape " : '');
                $script .= ";\n";
                $script .= "END " . ($this->isMySql() ? '//' : '') . "\n";
                $script .= ($this->isMySQL() ? 'DELIMITER ;' : 'GO');
                $script .= "\n\n";
            }
        }
        return $script;
    }

    /**
     * Create stored procedure CountBy columnname for one table
     * COUNT(columnname): counts by the columname where current column equals something
     * counts only this column
     * @return string
     */
    private function CountBy()
    {
        $script = '';

        foreach ($this->dTable->getRows() as $row) {
            // create for each searchable column with a count a CountBy Stored Procedure
            // select only the current column
            if ($row->isCountable()) {
                $script .= "-- modernways.be\n-- created by an orm apart\n";
                $script .= "-- Entreprise de modes et de manières modernes\n";
                $script .= "-- MySql DML\n";
                $script .= '-- Created : ' . date('l jS \of F Y h:i:s A') . "\n";
                $script .= "-- DML Count Stored Procedure for table {$this->dTable->getNameUCFirst()} \n-- \n";
                $script .= $this->scriptDropStoredProcedure($this->dTable->getNameUCFirst() . 'Count' .
                        $row->getSearchableParameterName() . "By{$row->getColumnName()}") . "\n";
                $script .= ($this->isMySQL() ? "DELIMITER //\n" : '');
                $script .= "CREATE PROCEDURE $this->escape{$this->dTable->getNameUCFirst()}Count" .
                    $row->getSearchableParameterName() . "By{$row->getColumnName()}$this->escape\n";
                $script .= "(\n";
                $script .= "\t\t" . ($this->isMsSql() ? '@' : 'p') . "{$row->getColumnName()} {$row->getSqlDataType()}\n";
                $script .= ")\n";
                $script .= ($this->isMsSql() ? "AS\n" : '');
                $script .= "BEGIN\n";
                $script .= "\tSELECT COUNT($this->escape{$row->getSearchableParameterName()}$this->escape) AS Counted\n";
                $script .= "FROM $this->escape{$this->dTable->getNameUCFirst()}$this->escape\n";
                $script .= "\t\tWHERE $this->escape{$row->getColumnName()}$this->escape = ";
                $script .= ($this->isMsSql() ? '@' : 'p') . "{$row->getColumnName()};\n";
                $script .= "END " . ($this->isMySql() ? '//' : '') . "\n";
                $script .= ($this->isMySQL() ? 'DELIMITER ;' : 'GO');
                $script .= "\n\n";
            }
        }
        return $script;
    }
}

?>

