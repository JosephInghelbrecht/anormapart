<?php
    /********************** C# DAL Catalog class ************************/
    /**
     * Catalog class
     * Generates DAL for C#
     *
     * You can use phpDocumentor to generate documentation
     *
     * 
     * @lastmodified 6/10/2013 Table klasse toegevoegd
     * @since 01/06/2012           
     * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M        
     * @version 3.0
     */
    namespace AnOrmApart\CSharp;
    
    class Dal extends \AnOrmApart\Catalog
    {
         /** ------------------ makeScriptDalClasses  --------------------------
         *
         * Make CRUD scripts for all tables in database for C#
         * @lastmodified 13/10/2015
         * @lastmodified 4/3/2013
         * @since 01/06/2012           
         * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M        
         * @version 0.2
         * @return string  
         */
        public function makeScriptDalClasses()
        {
            if ($this->CatalogExists())
            {
                $script = '';
                foreach ($this->catalog as $table)
                {
                    // onthoud met welke tabel we bezig zijn
                    $this->dTable = $table;
                    $fileName = "{$this->GetVendorToLower()}/{$this->GetDatabaseNameToLower()}/{$this->dTable->GetNameUCFirst()}.cs";
                    $script .= $this->makeScriptDalClass($fileName);
                }
                $script .= "\n";
           }
           return $script;
        }            

        
        /** ------------------ makeScriptDalClass  --------------------------
        *
        * Create Dal class for one table
        * @lastmodified 12/10/2015
        * @lastmodified 4/3/2013
        * @since 01/06/2012           
        * @author Jef Inghelbrecht - An Orm Apart        
        * @version 0.2
        * @return string  
        */
        public function makeScriptDalClass($fileName)
        {
            $script = "/* modernways.be\n";
            $script .= " * created by an orm apart\n";
            $script .= " * Entreprise de modes et de manières modernes\n";
            $script .= " * Dal for {$this->GetDatabaseName()} app\n";
            $script .= " * Created on " . date('l jS \of F Y h:i:s A') . "\n";
            $script .= " * FileName: $fileName\n";
            $script .= "*/ \n";
            // make the AnOrmApart classes
            $script .= "using System;\n";
            $script .= "using System.Data;\n";
            $script .= "using System.Data.SqlClient;\n";
            $script .= "using System.Configuration;\n";
            $script .= "using System.Collections.Generic;\n";

            // php directive and namespace
            $script .= "namespace {$this->GetDatabaseName()}.Dal\n";
            $script .= "{\n";
            // class Dal
            $script .= "\tclass {$this->dTable->GetNameUCFirst()} : ";
            $script .= "Dal.IDal<Models.{$this->dTable->GetNameUCFirst()}>\n";
            // begin class model code block
            $script .= "\t{\n";

            $script .= "\t\tprotected string message;\n";
            $script .= "\t\tpublic string Message\n";
            $script .= "\t\t{\n";
            $script .= "\t\t\tget { return this.message; }\n";
            $script .= "\t\t}\n";

            $script .= "\t\tprotected int rowCount;\n";
            $script .= "\t\tprotected Models.{$this->dTable->GetNameUCFirst()} model;\n";
            $script .= "\t\tpublic Models.{$this->dTable->GetNameUCFirst()} Model\n";
            $script .= "\t\t{\n";
            $script .= "\t\t\tget { return this.model; }\n";
            $script .= "\t\t\tset { this.model = value; }\n";
            $script .= "\t\t}\n";

            $script .= "\n";
            $script .= "\t\tpublic {$this->dTable->GetNameUCFirst()}(Models.{$this->dTable->GetNameUCFirst()} model)\n";
            $script .= "\t\t{\n";
            $script .= "\t\t\tthis.model = model;\n";
            $script .= "\t\t}\n\n";

            $script .= "\n";
            $script .= "\t\tpublic {$this->dTable->GetNameUCFirst()}()\n";
            $script .= "\t\t{\n";
            $script .= "\t\t}\n\n";

            $script .= $this->makeScriptCreateMethod();
            $script .= $this->makeScriptUpdateMethod();
            $script .= $this->makeScriptDeleteMethod();
            // eigenlijk mag die weg, het volstaat om bij Id searchable op true te zetten
            // dat geldt ook voor stored procedure
            $script .= $this->makeScriptReadOneMethod();
            $script .= $this->makeScriptReadAllMethod();
            //$script .= $this->makeScriptReadByMethods();
            //$script .= $this->makeScriptReadLikeMethods();
            //$script .= $this->makeScriptReadLikeXMethods();
            //$script .= $this->GetCountByMethods();
             // end class model block
            $script .= "\t}\n\n";
            // end namespace
            $script .= "}\n\n";
            return $script;
        }
    
         /** ------------------ makeScriptBindParamList  --------------------------
        *
        * Make paramater list for prepared statement PDO. The autoincrement primary column
        * is an OUT paramater and returns the Id of the newly inserted row if crud method is insert.
        * If crud method is update, all are in IN paramaters
        * @lastmodified 4/3/2013
        * @since 01/06/2012           
        * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M        
        * @version 0.2
        * @param array $rows
        * @param string $method
        * @return string  
        */
        private function makeScriptBindParamList($method)
        {
            $script = "\t\t\t//we use here a getter method to obtain the value to be saved,\n";
            foreach ($this->dTable->GetRows() as $row)
            {
                // no cloumns where the value is assigned inside the stored procedure itself
                // dafault value is prefixed by SP-
                if (!$row->isSetByStoredProcedure())
                {
                        $script .= $this->makeScriptBindParamOne($row, $method);
                }
            }
            return $script;
        }
    
         /** ------------------ makeScriptBindParamOne  --------------------------
        *
        * Make one paramater for prepared statement PDO. The autoincrement primary column
        * is an OUT paramater and returns the Id of the newly inserted row if crud method is insert.
        * If crud method is update, all are in IN paramaters
        * @lastmodified 4/5/2013
        * @since 01/06/2012           
        * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M        
        * @version 0.2
        * @param array $row
        * @return string  
        */
        private function makeScriptBindParamOne($row, $method)
        {
            $script = '';
            if ($row->IsPrimaryKey() && $method == 'INSERT') {
                $script .= "\t\t\tSqlParameter {$row->GetFieldName()} = new SqlParameter(";
                $script .= "\"@{$row->GetColumnName()}\", SqlDbType.";
                $script .= "{$row->getSqlDbType()}";
                $script .= ($row->getType()=='CHAR' ? ', ' . $row->getLength() : '');
                $script .= ");\n";
                $script .= "\t\t\t{$row->GetFieldName()}.Direction = ParameterDirection.Output;\n";
                $script .= "\t\t\tcommand.Parameters.Add({$row->GetFieldName()});\n";
            }
            elseif ($method != 'DELETE' ||  ($method == 'DELETE') && $row->IsPrimaryKey()){
                $script .= "\t\t\tcommand.Parameters.Add(new SqlParameter(";
                $script .= "\"@{$row->GetColumnName()}\", SqlDbType.";
                $script .= "{$row->getSqlDbType()}";
                $script .= ($row->getType()=='CHAR' ? ', ' . $row->getLength() : '');
                $script .= ")).Value = Model.{$row->GetColumnName()};\n";
            }
            return $script;
        }
    
        /** ------------------ toObject  --------------------------
        *
        * maps a row of a table to an object represenation
        * 
        * @lastmodified 13/3/2013
        * @since 01/06/2012           
        * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M        
        * @version 0.2
        * @param array $rows
        * @return string  
        */
        private function toObject($listOnly = false)
        {
            $script = '';
            foreach ($this->dTable->GetRows() as $row)
            {
                if ($listOnly) {
                    if ($row->isList()) {
                        $script .= $this->rowToObject($row);
                    }
                }
                else {
                    $script .= $this->rowToObject($row);
                }
            }
            if ($listOnly) {
                $script .= "\t\t\t\t\t\t\tlist.Add(Model);";
            }
            return $script;
        }

        /** ------------------ rowToObject  --------------------------
         *
         * maps a row of a table to an object represenation
         *
         * @lastmodified 13/3/2013
         * @since 01/06/2012
         * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M
         * @version 0.2
         * @param array $rows
         * @return string
         */
        private function rowToObject($row)
        {
            $script = "\t\t\t\t\t\t\tModel.{$row->GetColumnName()} = \n";
            $nullValue = ($row->getType() == 'CHAR' ? "\"\"" : ($row->getType() == 'FLOAT' ? "0f" : "0")) ;
            $script .= "\t\t\t\t\t\t\t\t(result.IsDBNull(result.GetOrdinal(\"{$row->getColumnName()}\")) ? {$row->getCSharpDataTypeNullValue()} : \n";
            $script .= "\t\t\t\t\t\t\t\t";
            $script .= ($row->getType() == 'CHAR' ? '' : rtrim($row->getCSharpDataType()) . '.Parse(');

            $script .= "result.GetOrdinal(\"{$row->GetColumnName()}\").ToString()";
            $script .= ($row->getType() == 'CHAR' ? '' : ')');
            $script .= ");\n";
            return $script;
        }

        public function makeScriptCreateMethod()
        {
            $script = "\t\tpublic int Create()\n";
            $script .= "\t\t{\n";
            $script .= "\t\t\tSqlConnection connection = new SqlConnection();\n";
            $script .= "\t\t\tconnection.ConnectionString = ConfigurationManager.\n";
            $script .= "\t\t\t\tConnectionStrings[\"{$this->GetDatabaseName()}\"].ToString();\n";
            $script .= "\t\t\tSqlCommand command = new SqlCommand();\n";
            $script .= "\t\t\t// in de CommandText eigenschap stoppen de naam\n";
            $script .= "\t\t\t// van de stored procedure\n";
            $script .= "\t\t\tstring sqlString = \"{$this->dTable->GetNameUCFirst()}Insert\";\n";
            $script .= $this->makeScriptBindParamList('INSERT');
            $script .= "\t\t\t// zeg aan het command object dat het een stored procedure\n";
            $script .= "\t\t\t// zal krijgen en geen SQL Statement\n";
            $script .= "\t\t\tcommand.CommandType = CommandType.StoredProcedure;\n";
            $script .= "\t\t\t// stop het sql statement in het command object\n";
            $script .= "\t\t\tcommand.CommandText = sqlString;\n";
            $script .= "\t\t\t// geeft het connection object door aan het command object\n";
            $script .= "\t\t\tcommand.Connection = connection;\n";
            $script .= "\t\t\tthis.message = \"Niets te melden\";\n";
            $script .= "\t\t\t// we gaan ervan uit dat het mislukt\n";
            $script .= "\t\t\tint result = 0;\n";
            $script .= "\t\t\tusing (connection)\n";
            $script .= "\t\t\t{\n";
            $script .= "\t\t\t\ttry\n";
            $script .= "\t\t\t\t{\n";
            $script .= "\t\t\t\t\tconnection.Open();\n";
            $script .= "\t\t\t\t\t//Verbinding geslaagd\n";
            $script .= "\t\t\t\t\tresult = command.ExecuteNonQuery();\n";
            $script .= "\t\t\t\t\t// we moeten kijken naar de waarde van out parameter\n";
            $script .= "\t\t\t\t\t// van Insert stored procedure. Als de naam van de\n";
            $script .= "\t\t\t\t\t// category al bestaat, retourneert de out parameter van\n";
            $script .= "\t\t\t\t\t// de stored procedure\n";
            $script .= "\t\t\t\t\t// -1\n";
            $script .= "\t\t\t\t\tif ((int)id.Value == -100)\n";
            $script .= "\t\t\t\t\t{\n";
            $script .= "\t\t\t\t\t\tthis.message = \" {$this->dTable->GetNameUCFirst()} bestaat al.\";\n";
            $script .= "\t\t\t\t\t\tresult = -100;\n";
            $script .= "\t\t\t\t\t}\n";
            $script .= "\t\t\t\t\telse if (result <= 0)\n";
            $script .= "\t\t\t\t\t{\n";
            $script .= "\t\t\t\t\t\tthis.message = \" {$this->dTable->GetNameUCFirst()} is niet geïnserted.\";\n";
            $script .= "\t\t\t\t\t}\n";
            $script .= "\t\t\t\t\telse\n";
            $script .= "\t\t\t\t\t{\n";
            $script .= "\t\t\t\t\t\tthis.message = \" {$this->dTable->GetNameUCFirst()} is geïnserted.\";\n";
            $script .= "\t\t\t\t\t\tresult = (int)id.Value;\n";
            $script .= "\t\t\t\t\t}\n";
            // einde try
            $script .= "\t\t\t\t}\n";
            $script .= "\t\t\t\tcatch (SqlException e)\n";
            $script .= "\t\t\t\t{\n";
            $script .= "\t\t\t\t\tthis.message = e.Message;\n";
            $script .= "\t\t\t\t}\n";
            // end using
            $script .= "\t\t\t}\n";
            // end class
            $script .= "\t\t\treturn result; // 0 of de Id van de nieuwe rij\n";
            $script .= "\t\t}\n\n";
            return $script;
        }

        public function makeScriptUpdateMethod()
        {
            $script = "\t\tpublic int Update()\n";
            $script .= "\t\t{\n";
            $script .= "\t\t\tSqlConnection connection = new SqlConnection();\n";
            $script .= "\t\t\tconnection.ConnectionString = ConfigurationManager.\n";
            $script .= "\t\t\t\tConnectionStrings[\"{$this->GetDatabaseName()}\"].ToString();\n";
            $script .= "\t\t\tSqlCommand command = new SqlCommand();\n";
            $script .= "\t\t\t// in de CommandText eigenschap stoppen de naam\n";
            $script .= "\t\t\t// van de stored procedure\n";
            $script .= "\t\t\tstring sqlString = \"{$this->dTable->GetNameUCFirst()}Update\";\n";
            $script .= $this->makeScriptBindParamList('UPDATE');
            $script .= "\t\t\t// zeg aan het command object dat het een stored procedure\n";
            $script .= "\t\t\t// zal krijgen en geen SQL Statement\n";
            $script .= "\t\t\tcommand.CommandType = CommandType.StoredProcedure;\n";
            $script .= "\t\t\t// stop het sql statement in het command object\n";
            $script .= "\t\t\tcommand.CommandText = sqlString;\n";
            $script .= "\t\t\t// geeft het connection object door aan het command object\n";
            $script .= "\t\t\tcommand.Connection = connection;\n";
            $script .= "\t\t\tthis.message = \"Niets te melden\";\n";
            $script .= "\t\t\t// we gaan ervan uit dat het mislukt\n";
            $script .= "\t\t\tint result = 0;\n";
            $script .= "\t\t\tusing (connection)\n";
            $script .= "\t\t\t{\n";
            $script .= "\t\t\t\ttry\n";
            $script .= "\t\t\t\t{\n";
            $script .= "\t\t\t\t\tconnection.Open();\n";
            $script .= "\t\t\t\t\t//Verbinding geslaagd\n";
            $script .= "\t\t\t\t\tresult = command.ExecuteNonQuery();\n";
            $script .= "\t\t\t\t\t// we moeten kijken naar de return van ExecuteNonQuery\n";
            $script .= "\t\t\t\t\t// retourneert het aantal rijen dat gedeleted is\n";
            $script .= "\t\t\t\t\tif (result == 0)\n";
            $script .= "\t\t\t\t\t{\n";
            $script .= "\t\t\t\t\t\tthis.message = \" {$this->dTable->GetNameUCFirst()} is niet geüpdated.\";\n";
            $script .= "\t\t\t\t\t}\n";
            $script .= "\t\t\t\t\telse\n";
            $script .= "\t\t\t\t\t{\n";
            $script .= "\t\t\t\t\t\tthis.message = \" {$this->dTable->GetNameUCFirst()} is geüpdated.\";\n";
            $script .= "\t\t\t\t\t}\n";
            // einde try
            $script .= "\t\t\t\t}\n";
            $script .= "\t\t\t\tcatch (SqlException e)\n";
            $script .= "\t\t\t\t{\n";
            $script .= "\t\t\t\t\tthis.message = e.Message;\n";
            $script .= "\t\t\t\t}\n";
            // end using
            $script .= "\t\t\t}\n";
            // end class
            $script .= "\t\t\treturn result; // 0 of de Id van de nieuwe rij\n";
            $script .= "\t\t}\n\n";

            return $script;
        }

        public function makeScriptDeleteMethod()
        {
            $script = "\t\tpublic int Delete()\n";
            $script .= "\t\t{\n";
            $script .= "\t\t\tSqlConnection connection = new SqlConnection();\n";
            $script .= "\t\t\tconnection.ConnectionString = ConfigurationManager.\n";
            $script .= "\t\t\t\tConnectionStrings[\"{$this->GetDatabaseName()}\"].ToString();\n";
            $script .= "\t\t\tSqlCommand command = new SqlCommand();\n";
            $script .= "\t\t\t// in de CommandText eigenschap stoppen de naam\n";
            $script .= "\t\t\t// van de stored procedure\n";
            $script .= "\t\t\tstring sqlString = \"{$this->dTable->GetNameUCFirst()}Delete\";\n";
            $script .= $this->makeScriptBindParamList('DELETE');
            $script .= "\t\t\t// zeg aan het command object dat het een stored procedure\n";
            $script .= "\t\t\t// zal krijgen en geen SQL Statement\n";
            $script .= "\t\t\tcommand.CommandType = CommandType.StoredProcedure;\n";
            $script .= "\t\t\t// stop het sql statement in het command object\n";
            $script .= "\t\t\tcommand.CommandText = sqlString;\n";
            $script .= "\t\t\t// geeft het connection object door aan het command object\n";
            $script .= "\t\t\tcommand.Connection = connection;\n";
            $script .= "\t\t\tthis.message = \"Niets te melden\";\n";
            $script .= "\t\t\t// we gaan ervan uit dat het mislukt\n";
            $script .= "\t\t\tint result = 0;\n";
            $script .= "\t\t\tusing (connection)\n";
            $script .= "\t\t\t{\n";
            $script .= "\t\t\t\ttry\n";
            $script .= "\t\t\t\t{\n";
            $script .= "\t\t\t\t\tconnection.Open();\n";
            $script .= "\t\t\t\t\t//Verbinding geslaagd\n";
            $script .= "\t\t\t\t\tresult = command.ExecuteNonQuery();\n";
            $script .= "\t\t\t\t\t// we moeten kijken naar de return van ExecuteNonQuery\n";
            $script .= "\t\t\t\t\t// retourneert het aantal rijen dat gedeleted is\n";
            $script .= "\t\t\t\t\tif (result == 0)\n";
            $script .= "\t\t\t\t\t{\n";
            $script .= "\t\t\t\t\t\tthis.message = \" {$this->dTable->GetNameUCFirst()} is niet gedeleted.\";\n";
            $script .= "\t\t\t\t\t}\n";
            $script .= "\t\t\t\t\telse\n";
            $script .= "\t\t\t\t\t{\n";
            $script .= "\t\t\t\t\t\tthis.message = \" {$this->dTable->GetNameUCFirst()} is gedeleted.\";\n";
            $script .= "\t\t\t\t\t}\n";
            // einde try
            $script .= "\t\t\t\t}\n";
            $script .= "\t\t\t\tcatch (SqlException e)\n";
            $script .= "\t\t\t\t{\n";
            $script .= "\t\t\t\t\tthis.message = e.Message;\n";
            $script .= "\t\t\t\t}\n";
            // end using
            $script .= "\t\t\t}\n";
            // end class
            $script .= "\t\t\treturn result;\n";
            $script .= "\t\t}\n\n";
            return $script;
        }

        public function makeScriptReadOneMethod()
        {
            $script = "\t\tpublic Models.{$this->dTable->GetNameUCFirst()} ReadOne()\n";
            $script .= "\t\t{\n";
            $script .= "\t\t\tSqlConnection connection = new SqlConnection();\n";
            $script .= "\t\t\tconnection.ConnectionString = ConfigurationManager.\n";
            $script .= "\t\t\t\tConnectionStrings[\"{$this->GetDatabaseName()}\"].ToString();\n";
            $script .= "\t\t\tSqlCommand command = new SqlCommand();\n";
            $script .= "\t\t\t// in de CommandText eigenschap stoppen we de naam\n";
            $script .= "\t\t\t// van de stored procedure\n";
            $script .= "\t\t\tstring sqlString = \"{$this->dTable->GetNameUCFirst()}SelectOne\";\n";
            $script .= $this->makeScriptBindParamList('DELETE');
            $script .= "\t\t\t// zeg aan het command object dat het een stored procedure\n";
            $script .= "\t\t\t// zal krijgen en geen SQL Statement\n";
            $script .= "\t\t\tcommand.CommandType = CommandType.StoredProcedure;\n";
            $script .= "\t\t\t// stop het sql statement in het command object\n";
            $script .= "\t\t\tcommand.CommandText = sqlString;\n";
            $script .= "\t\t\t// geeft het connection object door aan het command object\n";
            $script .= "\t\t\tcommand.Connection = connection;\n";
            $script .= "\t\t\tthis.message = \"Niets te melden\";\n";
            $script .= "\t\t\t// we gaan ervan uit dat het mislukt\n";
            $script .= "\t\t\tSqlDataReader result = null;\n";
            $script .= "\t\t\tusing (connection)\n";
            $script .= "\t\t\t{\n";
            $script .= "\t\t\t\ttry\n";
            $script .= "\t\t\t\t{\n";
            $script .= "\t\t\t\t\tconnection.Open();\n";
            $script .= "\t\t\t\t\t//Verbinding geslaagd\n";
            $script .= "\t\t\t\t\tthis.message = \"Connectie is open\";\n";
            $script .= "\t\t\t\t\tusing(result = command.ExecuteReader());\n";
            $script .= "\t\t\t\t\t{\n";
            $script .= "\t\t\t\t\t\tif (result.HasRows)\n";
            $script .= "\t\t\t\t\t\t{\n";
            $script .= "\t\t\t\t\t\t\t// lees de gevonden rij in\n";
            $script .= "\t\t\t\t\t\t\tthis.message = \" {$this->dTable->GetNameUCFirst()} is gevonden.\";\n";
            $script .= "\t\t\t\t\t\t\tresult.Read();\n";
            $script .= $this->toObject();
            $script .= "\t\t\t\t\t\t}\n";
            $script .= "\t\t\t\t\t\telse\n";
            $script .= "\t\t\t\t\t\t{\n";
            $script .= "\t\t\t\t\t\t\tthis.message = \" {$this->dTable->GetNameUCFirst()} rij niet gevonden.\";\n";
            $script .= "\t\t\t\t\t\t}\n";
            // einde if
            $script .= "\t\t\t\t\t}\n";
            // einde try
            $script .= "\t\t\t\t}\n";
            $script .= "\t\t\t\tcatch (SqlException e)\n";
            $script .= "\t\t\t\t{\n";
            $script .= "\t\t\t\t\tthis.message = e.Message;\n";
            $script .= "\t\t\t\t}\n";
            // end using
            $script .= "\t\t\t}\n";
            // end class
            $script .= "\t\t\treturn Model;\n";
            $script .= "\t\t}\n\n";
            return $script;
        }

        public function makeScriptReadAllMethod()
        {
            $script = "\t\tpublic List<Models.{$this->dTable->GetNameUCFirst()}> ReadAll()\n";
            $script .= "\t\t{\n";
            $script .= "\t\t\tList<Models.{$this->dTable->GetNameUCFirst()}> list = new List<Models.{$this->dTable->GetNameUCFirst()}>();\n";

            $script .= "\t\t\tSqlConnection connection = new SqlConnection();\n";
            $script .= "\t\t\tconnection.ConnectionString = ConfigurationManager.\n";
            $script .= "\t\t\t\tConnectionStrings[\"{$this->GetDatabaseName()}\"].ToString();\n";
            $script .= "\t\t\tSqlCommand command = new SqlCommand();\n";
            $script .= "\t\t\t// in de CommandText eigenschap stoppen we de naam\n";
            $script .= "\t\t\t// van de stored procedure\n";
            $script .= "\t\t\tstring sqlString = \"{$this->dTable->GetNameUCFirst()}SelectAll\";\n";
            $script .= "\t\t\t// zeg aan het command object dat het een stored procedure\n";
            $script .= "\t\t\t// zal krijgen en geen SQL Statement\n";
            $script .= "\t\t\tcommand.CommandType = CommandType.StoredProcedure;\n";
            $script .= "\t\t\t// stop het sql statement in het command object\n";
            $script .= "\t\t\tcommand.CommandText = sqlString;\n";
            $script .= "\t\t\t// geeft het connection object door aan het command object\n";
            $script .= "\t\t\tcommand.Connection = connection;\n";
            $script .= "\t\t\tthis.message = \"Niets te melden\";\n";
            $script .= "\t\t\t// we gaan ervan uit dat het mislukt\n";
            $script .= "\t\t\tSqlDataReader result = null;\n";
            $script .= "\t\t\tusing (connection)\n";
            $script .= "\t\t\t{\n";
            $script .= "\t\t\t\ttry\n";
            $script .= "\t\t\t\t{\n";
            $script .= "\t\t\t\t\tconnection.Open();\n";
            $script .= "\t\t\t\t\t//Verbinding geslaagd\n";
            $script .= "\t\t\t\t\tthis.message = \"Connectie is open\"\n";
            $script .= "\t\t\t\t\tusing(result = command.ExecuteReader();\n";
            $script .= "\t\t\t\t\t{\n";
            $script .= "\t\t\t\t\t\tif (result.HasRows)\n";
            $script .= "\t\t\t\t\t\t{\n";
            $script .= "\t\t\t\t\t\t\t// lees de gevonden rij in\n";
            $script .= "\t\t\t\t\t\t\tthis.message = \" {$this->dTable->GetNameUCFirst()} rijen gevonden.\";\n";
            $script .= "\t\t\t\t\t\t\tresult.Read();\n";
            $script .= $this->toObject(true);
            $script .= "\t\t\t\t\t\t}\n";
            $script .= "\t\t\t\t\t\telse\n";
            $script .= "\t\t\t\t\t\t{\n";
            $script .= "\t\t\t\t\t\t\tthis.message = \" {$this->dTable->GetNameUCFirst()} rijen niet gevonden.\";\n";
            $script .= "\t\t\t\t\t\t}\n";
            // einde if
            $script .= "\t\t\t\t\t}\n";
            // einde try
            $script .= "\t\t\t\t}\n";
            $script .= "\t\t\t\tcatch (SqlException e)\n";
            $script .= "\t\t\t\t{\n";
            $script .= "\t\t\t\t\tthis.message = e.Message;\n";
            $script .= "\t\t\t\t}\n";
            // end using
            $script .= "\t\t\t}\n";
            // end class
            $script .= "\t\t\treturn list;\n";
            $script .= "\t\t}\n\n";
            return $script;
        }

    }
