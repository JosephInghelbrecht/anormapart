<?php
/********************** Catalog class ************************/
/**
 * Document Object Model class for d2b Catalog
 * Created : 01/06/2012
 * Last edit :  03/06/2012 Jef
 *              08/06/2012
 *             
 * @version 0.2
 */
namespace AnOrmApart;

//define('SQLITE_MODE', 1);
//defining constants for different Catalog types
define("SQLITE", 1);
define("MSSQL", 2);
define("MYSQL", 3);

define("MYSQLDAL", 4);
define("MYSQLBLL", 5);

// constants for named options CRUD forms
define("WITH_AUTENTICATION", TRUE);
define("NO_AUTENTICATION", FALSE);
define("NO_ID", -1);
use DOMDocument;
use exception;

class Catalog
{
    public  $catalog = array();
    protected $action;
    // onthoud de naam van de courante tabel
    protected $dTable;
    protected $databaseName;
    protected $vendor;
    protected $appName;
    protected $sqlProvider;
    protected $escape;
    
    public function GetDatabaseName()
    {
        return $this->databaseName;
    }

    public function GetDatabaseNameToLower()
    {
        return strtolower($this->databaseName);
    }

    public function GetDatabaseNameUCFirst()
    {
        return ucfirst($this->databaseName);
    }

    public function GetAppName()
    {
        return $this->appName;
    }

    public function SetAppName($value)
    {
        $this->appName = $value;
    }

    public function setDatabaseName($name)
    {
        $this->databaseName =  preg_replace("/[^\w ]+/", '', $name);
    }

    public function GetAction()
    {
        return $this->action;
    }

    public function SetAction($value)
    {
        $this->action = $value;
    }

    public function isSql()
    {
        return (($this->getAction() == 'my-sql-ddl') or ($this->getAction() == 'my-sql-dml')
            or ($this->getAction() == 'ms-sql-dml') or ($this->getAction() == 'ms-sql-dml'));
    }

    public function isMsSql()
    {
        return (($this->getAction() == 'ms-sql-ddl') or ($this->getAction() == 'ms-sql-dml'));
    }

    public function isMySql()
    {
        return (($this->getAction() == 'my-sql-ddl') or ($this->getAction() == 'my-sql-dml'));
    }

    public function isDal()
    {
        return ($this->getAction() == 'php-dal-class') || ($this->getAction() == 'php-dal-codebehind-class');
    }

    public function isBllTest()
    {
        return ($this->getAction() == 'php-test-bll-class');
    }

    public function SetVendor($name)
    {
        $this->vendor = preg_replace("/[^\w\\\\ ]+/", '', (string) $name);
    }

    public function GetVendor()
    {
        if (strlen($this->vendor) > 0)
        {
            return $this->vendor;
        }
        else
        {
            return $this->GetDatabaseName();
        }
    }

    public function GetVendorToLower()
    {
        if (strlen($this->vendor) > 0)
        {
            return strtolower($this->vendor);
        }
        else
        {
            return '';
        }
    }
    /********************** Construct ************************/
    /**
     * Created : 01/06/2012
     * Last edit :  03/06/2012 Jef
     *              2/2/2015
     *             
     * @version 0.2
     * @param $action the action name from de coordinating controller
     * @param $string html for table
     */
    public function __construct($action, $htmlString)
    {
        $this->SetAction($action);
        // domify
        $doc = new DOMDocument();
        $doc->strictErrorChecking = TRUE;
        $doc->loadHtml($htmlString);
        $xml = simplexml_import_dom($doc);
        // let op de cast !!!!!!!!!!!!!!!!!!!!!!!!!!!!
        $this->SetDatabaseName((string) $xml->body->div->h2);
        $this->SetAppName((string) $xml->body->div->h3);
        $this->SetVendor((string) $xml->body->div->h4);
        $tables = $this->ReadTables($xml);

        // onmogelijk om hier $tableName door te geven
        $this->catalog = $tables;

        if ($this->isMsSql())
        {
            $this->escape = '"';
        }
        elseif ($this->isMySql())
        {
            $this->escape = '`';                
        }
    }

    public function ReadTableNames($xml)
    {
        $names = array();
        foreach($xml->body->div as $section)
        {
            $names[] = preg_replace("/[^\w ]+/", '', (string) $section->h3);
        }
        return $names;
    }

    public function ReadTables($xml)
    {
        //$table = $xml->body->div->article->section->div->table;
        //print_r($table);
        $tables = array();
        foreach($xml->body->div->div as $section)
        {
            $tableName = preg_replace("/[^\w ]+/", '', (string) $section->h2);
            $table = new \AnOrmApart\Table();
            $table->SetText($section->h3);
            
            $table->SetRows($this->ReadTableRows($section));
            $table->SetName($tableName);
            // if extend is specified, append base rows
            // only in my-sql-ddl and my-sql-dml modus
            $table->SetExtend($section->h4);
            $table->SetOrderBy($section->h5);
            if ($table->IsExtend() and file_exists('app/anormapart/data/catalog/base.html')
                and ($this->isSql() or $this->isDal() or $this->isBllTest()))
            {
                // domify 
                $doc = new DOMDocument();
                $doc->strictErrorChecking = TRUE;
                $doc->loadHtmlFile('app/anormapart/data/catalog/base.html');
                $extendXML = simplexml_import_dom($doc);
                foreach($extendXML->body->div as $extendSection)
                {
                    // recursion
                    $table->AddRows($this->ReadTableRows($extendSection));               
                }
            }
            // echo file_get_contents('app/anormapart/data/catalog/base.html');
            // print_r($table);
            $tables[$tableName] = $table;
        }
//        echo '<pre>';
//        var_dump($tables);
//        echo '</pre>';
        return $tables;
    }

    public function ReadTableRows($xml)
    {
        //$table = $xml->div->table;
        //print_r($table);
        foreach($xml->table->tbody->tr as $tr)
        {
            //$reflector = new \ReflectionClass($this->GetAction()); 
            //$row = $reflector->newInstance(); // new \D2b\D2bRow();
            $row = new \ANormApart\Row();
            $row->SetColumnName((string) $tr->td[0]);
            $row->SetDisplayText((string) $tr->td[1]);
            $row->SetSearchable((string) $tr->td[2]);
            $row->SetOrderBy((string) $tr->td[2]);
            $row->SetDefaultValue((string) $tr->td[3]);
            $row->SetUnique((string) $tr->td[4]);
            $row->setType((string) $tr->td[5]);
            $row->SetLength((string) $tr->td[6]);
            $row->SetVariable((string) $tr->td[7]);
            $row->SetInternational((string) $tr->td[8]);
            $row->SetRequired((string) $tr->td[9]);
            $row->setReferenceTable((string) $tr->td[10]);
            $row->SetReferenceColumn((string) $tr->td[11]);
            $row->SetReferenceDisplayColumn((string) $tr->td[12]);
            $row->SetList((string) $tr->td[13]);
            $row->setHtmlElement((string) $tr->td[14]);
            $row->SetHtmlTypeAttribute((string) $tr->td[15]);
            $row->SetHtmlNameAttribute((string) $tr->td[16]);
            $row->SetHtmlPatternAttribute((string) $tr->td[17]);
            $row->SetHtmlTitleAttribute((string) $tr->td[18]);
            $row->SetHtmlPlaceholder((string) $tr->td[19]);
            // voeg de array toe
            // fill an array with all rows from the html table
            $rows[] = $row;
        }
        return $rows;
   }
    
    public function CatalogExists()
    {
        if (!isset($this->catalog))
        {
            throw new AnOrmApart\AnOrmApartException('Catalog is missing.', 1);
        }
        return true;    
    }
} 
?>
