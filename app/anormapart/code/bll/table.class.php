<?php
    /**
     * Table Class
     * Created : 05/11/2013
     * JI
     *             
     * @version 0.3
     */
    
    namespace AnOrmApart;
    class Table
    {
        private $createAuthenticated;
        private $readAuthenticated;
        private $updateAuthenticated;
        private $deleteAuthenticated;
        private $rows;
        private $text; // text displayed in view
        private $name;
        private $extend;
        private $orderBy;
    
        public function IsReadAuthenticated()
        {
            return $this->readAuthenticated;
        }
    
        public function GetRows()
        {
            return $this->rows;
        }
    
        public function SetRows($rows)
        {
            $this->rows = $rows;
        }
    
        public function AddRows($rows)
        {
            $this->rows = array_merge($this->rows, $rows);
        }
        
        public function GetName()
        {
            return $this->name;
        }
    
        public function SetName($name)
        {
            $this->name = $name;
        }

        public function GetText()
        {
            return $this->text;
        }
    
        public function SetText($value)
        {
            $this->text = $value;
        }

        public function SetExtend($name)
        {
            $this->extend = strtoupper(preg_replace("/[^\w ]+/", '', (string) $name));
        }

        public function IsExtend()
        {
            return ($this->extend == 'EXTEND');
        }

        public function getExtend()
        {
            return $this->extend;
        }

        public function SetOrderBy($name)
        {
            if (strlen($name) > 0)
            {
                $this->orderBy = $name;
            }
            else
            {
                $this->orderBy = FALSE;
            }
        }

        public function GetOrderBy()
        {
            return $this->orderBy;
        }

        public function GetNameUCFirst()
        {
            return ucfirst($this->name);
        }
    
        public function GetNameLCFirst()
        {
            return lcfirst($this->name);
        }
    
        public function GetNameToLower()
        {
            return strtolower($this->name);
        }

        public function __construct()
        {
            $createAuthenticated = FALSE;
            $readAuthenticated = FALSE;
            $updateAuthenticated = FALSE;
            $deleteAuthenticated = FALSE;
        }
    }
?>

