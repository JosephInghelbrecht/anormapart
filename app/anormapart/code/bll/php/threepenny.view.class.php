<?php
/********************** Php Template code ************************/

/**
 * Template
 * Generates Templates for PHP
 *
 * You can use phpDocumentor to generate documentation
 *
 *
 * @lastmodified 11/06/2016
 * @since 01/06/2012
 * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M
 * @version 3.0
 */

namespace AnOrmApart\Php\Threepenny;
class View extends \AnOrmApart\Catalog
{
    /** ------------------ makeScriptTemplateAll  --------------------------
     *
     * Make Template scripts for all entities (tables) in database for PHP
     * @lastmodified 21/04/2021
     * @return string
     * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M
     * @version 4.0
     * @since 01/06/2012
     */
    public function makeScriptTemplateAll()
    {
        $scriptAdminIndex = "<!--  Index view for Admin entities\n";
        $scriptAdminIndex .= " modernways.be\n";
        $scriptAdminIndex .= " created by an orm apart\n";
        $scriptAdminIndex .= " Entreprise de modes et de manières modernes\n";
        $scriptAdminIndex .= " created on " . date('l jS \of F Y h:i:s A') . "\n";
        $scriptAdminIndex .= " file name Admin/Index.php\n";
        $scriptAdminIndex .= "-->\n";
        $script = '';
        if ($this->CatalogExists()) {
            $scriptAdminIndex .= "<main class=\"show-room index\">\n";
            foreach ($this->catalog as $table) {
                // var_dump($table);
                // onthoud met welke tabel we bezig zijn
                $this->dTable = $table;
                $scriptAdminIndex .= "<a href=\"/{$this->dTable->GetNameUCFirst()}/index\"";
                $scriptAdminIndex .= " class=\"tile\"><span>{$this->dTable->GetNameUCFirst()}</span></a>\n";
                $fileName = "Views/{$this->dTable->GetNameUCFirst()}";
                // php directive and namespace
                $script .= $this->makeScriptIndex($fileName);
                $script .= $this->makeScriptCreatingOne($fileName);
                $script .= $this->makeScriptReadingAll($fileName);
                $script .= $this->makeScriptReadingOne($fileName);
                $script .= $this->makeScriptUpdatingOne($fileName);
                $script .= "\n";
            }
            $scriptAdminIndex .= "</main>\n";
             $script = $scriptAdminIndex . $script;
        }
        return $script;
    }

    /** ------------------ makeScriptIndex  --------------------------
     *
     * Make script for Index View
     * @lastmodified 21/04/2021
     * @param
     * @return string
     * @version 3.0
     * @since 01/06/2012
     * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M
     */
    private function makeScriptIndex($fileName)
    {

        $script = "<!--  Index view for {$this->dTable->GetName()} entity\n";
        $script .= " modernways.be\n";
        $script .= " created by an orm apart\n";
        $script .= " Entreprise de modes et de manières modernes\n";
        $script .= " created on " . date('l jS \of F Y h:i:s A') . "\n";
        $script .= " file name $fileName/Index.php\n";
        $script .= "-->\n";
        $script .= "<main class=\"show-room entity\">\n";
        $script .= "\t<section class=\"detail\">\n";
        $script .= "\t<header>\n";
        $script .= "\t\t<h2 class=\"banner\">Index {$this->dTable->GetName()}</h2>\n";
        $script .= "\t\t<nav class=\"command-panel\">\n";
        $script .= "\t\t\t<a href=\"/{$this->dTable->GetName()}/CreatingOne\" class=\"tile\">\n";
        $script .= "\t\t\t\t<span class=\"icon-plus\"></span>\n";
        $script .= "\t\t\t\t<span class=\"screen-reader-text\">Creating One</span>\n";
        $script .= "\t\t\t</a>\n";
        $script .= "\t\t</nav>\n";
        $script .= "\t</header>\n";
        $script .= "\t<fieldset>\n";
        $script .= "\t</fieldset>\n";
        $script .= "\t\t<footer class=\"feedback\">\n";
        $script .= "\t\t\t<p><?php echo \$model['message']; ?></p>\n";
        $script .= "\t\t\t<p><?php echo isset(\$model['error']) ? \$model['error'] : '';?></p>\n";
        $script .= "\t\t</footer>\n";
        $script .= "\t</section>\n";
        $script .= "\t<?php include('ReadingAll.php'); ?>\n";
        $script .= "</main>\n";
        return $script;
    }

    /** ------------------ makeScriptReadingAll  --------------------------
     *
     * Make script for ReadingAll View
     * @lastmodified 22/04/2021
     * @param
     * @return string
     * @version 4.0
     * @since 01/06/2012
     * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M
     */
    private function makeScriptReadingAll($fileName)
    {
        // Create Reading all View
        $script = "<!--  ReadingAll view for {$this->dTable->GetName()} entity\n";
        $script .= " modernways.be\n";
        $script .= " created by an orm apart\n";
        $script .= " Entreprise de modes et de manières modernes\n";
        $script .= " created on " . date('l jS \of F Y h:i:s A') . "\n";
        $script .= " file name $fileName/ReadingAll.php\n";
        $script .= "-->\n";
        $script .= "<aside class=\"list\">\n";
        $script .= "\t<?php\n";
        $script .= "\t\tif (\$model['list'])\n";
        $script .= "\t\t{\n";
        $script .= "\t?>\n";
        $script .= "\t<table>\n";
        $script .= "\t\t<?php\n";

        $script .= "\t\t\tforeach (\$model['list'] as \$item)\n";
        $script .= "\t\t\t{\n";
        $script .= "\t\t?>\n";
        $script .= "\t\t<tr>\n";
        $script .= "\t\t\t<td>\n";
        $script .= "\t\t\t\t<a class=\"tile\"\n";
        $script .= "\t\t\t\thref=\"/{$this->dTable->GetNameUCFirst()}/readingOne/<?php echo \$item['Id'];?>\">\n";
        $script .= "\t\t\t\t<span class=\"icon-arrow-right\"></span>\n";
        $script .= "\t\t\t\t<span class=\"screen-reader-text\">Select</span></a>\n";
        $script .= "\t\t\t</td>\n";
        // all searchable columns
        foreach ($this->dTable->GetRows() as $row) {
            // create for each searchable column a column in the table or
            // a list item in the list
            if ($row->isList()) {
                $script .= "\t\t\t<td>\n";
                // if foreignkey list corresponding name and not foreignkey itself
                if ($row->isForeignKey()) {
                    $script .= "\t\t\t\t<?php echo \$item['{$row->GetColumnName()}{$row-> GetReferenceDisplayColumn()}'];?>\n";

                } else {
                    $script .= "\t\t\t\t<?php echo \$item['{$row->GetColumnName()}'];?>\n";
                }
                $script .= "\t\t\t</td>\n";
            }
        }
        $script .= "\n";
        $script .= "\t\t</tr>\n";
        $script .= "\t\t<?php\n";
        $script .= "\t\t}\n";
        $script .= "\t\t?>\n";
        $script .= "\t</table>\n";
        $script .= "\t<?php\n";
        $script .= "\t\t}\n";
        $script .= "\t\telse\n";
        $script .= "\t\t{\n";
        $script .= "\t?>\n";
        $script .= "\t<p>Geen {$this->dTable->GetText()}</p>\n";
        $script .= "\t<?php\n";
        $script .= "\t}\n";
        $script .= "\t?>\n";
        $script .= "</aside>\n\n";
        return $script;
    }

    /** ------------------ makeScriptCreatingOne  --------------------------
     *
     * Make script for CreatingOne view
     * @lastmodified 23/04/2021
     * @param
     * @return string
     * @version 4.0
     * @since 01/06/2012
     * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M
     */
    private function makeScriptCreatingOne($fileName)
    {
        // Create CreatingOne View
        $script = "<!--  CreatingOne View for {$this->dTable->GetName()} entity\n";
        $script .= " modernways.be\n";
        $script .= " created by an orm apart\n";
        $script .= " Entreprise de modes et de manières modernes\n";
        $script .= " created on " . date('l jS \of F Y h:i:s A') . "\n";
        $script .= " file name $fileName/CreatingOne.php\n";
        $script .= "-->\n";

        $script .= "<main class=\"show-room entity\">\n";
        $script .= "\t<form class=\"detail\" id=\"form\" ";
        $script .= "action=\"/{$this->dTable->GetName()}/createOne\" ";
        $script .= "method=\"post\">\n";

        $script .= "\t\t<header>\n";
        $script .= "\t\t\t<h2 class=\"banner\">Creating One {$this->dTable->GetName()}</h2>\n";
        $script .= "\t\t\t<nav class=\"command-panel\">\n";
        $script .= "\t\t\t\t<button type=\"submit\" value=\"createOne\" name=\"createOne\" class=\"tile\">\n";
        $script .= "\t\t\t\t\t<span class=\"icon-floppy-disk\"></span>\n";
        $script .= "\t\t\t\t\t<span class=\"screen-reader-text\">Create One</span>\n";
        $script .= "\t\t\t\t</button>\n";
        $script .= "\t\t\t\t<a href=\"/{$this->dTable->GetName()}/Index\" class=\"tile\">\n";
        $script .= "\t\t\t\t\t<span class=\"icon-cross\"></span>\n";
        $script .= "\t\t\t\t\t<span class=\"screen-reader-text\">Annuleren</span>\n";
        $script .= "\t\t\t\t</a>\n";
        $script .= "\t\t\t</nav>\n";
        $script .= "\t\t</header>\n";

        $script .= $this->makeScriptFieldset('CreatingOne');

        $script .= "\t\t<footer class=\"feedback\">\n";
        $script .= "\t\t\t<p><?php echo \$model['message']; ?></p>\n";
        $script .= "\t\t\t<p><?php echo isset(\$model['error']) ? \$model['error'] : '';?></p>\n";
        $script .= "\t\t</footer>\n";
        $script .= "\t</form>\n";
        $script .= "\t<?php include('ReadingAll.php'); ?>\n";
        $script .= "</main>\n";

        return $script;
    }

    /** ------------------ makeScriptReadingOne  --------------------------
     *
     * Make script for ReadingOne view
     * @lastmodified 24/04/2021
     * @param
     * @return string
     * @version 4.0
     * @since 01/06/2012
     * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M
     */
    private function makeScriptReadingOne($fileName)
    {
        // Create ReadingOne View
        $script = "<!--  ReadingOne View for {$this->dTable->GetName()} entity\n";
        $script .= " modernways.be\n";
        $script .= " created by an orm apart\n";
        $script .= " Entreprise de modes et de manières modernes\n";
        $script .= " created on " . date('l jS \of F Y h:i:s A') . "\n";
        $script .= " file name $fileName/ReadingOne.php\n";
        $script .= "-->\n";

        $script .= "<main class=\"show-room entity\">\n";
        $script .= "\t<section class=\"detail\" id=\"form\" ";
        $script .= "action=\"/{$this->dTable->GetName()}/createOne\" ";
        $script .= "method=\"post\">\n";

        $script .= "\t\t<header>\n";
        $script .= "\t\t\t<h2 class=\"banner\">Reading One {$this->dTable->GetName()}</h2>\n";
        $script .= "\t\t\t<nav class=\"command-panel\">\n";
        $script .= "\t\t\t\t<a href=\"/{$this->dTable->GetName()}/UpdatingOne/<?php echo \$model['row']['Id'];?>\" class=\"tile\">\n";
        $script .= "\t\t\t\t\t<span class=\"icon-pencil\"></span>\n";
        $script .= "\t\t\t\t\t<span class=\"screen-reader-text\">Updating One</span>\n";
        $script .= "\t\t\t\t</a>\n";
        $script .= "\t\t\t\t<a href=\"/{$this->dTable->GetName()}/CreatingOne\" class=\"tile\">\n";
        $script .= "\t\t\t\t\t<span class=\"icon-plus\"></span>\n";
        $script .= "\t\t\t\t\t<span class=\"screen-reader-text\">Creating One</span>\n";
        $script .= "\t\t\t\t</a>\n";
        $script .= "\t\t\t\t<a href=\"/{$this->dTable->GetName()}/DeleteOne/<?php echo \$model['row']['Id'];?>\" class=\"tile\">\n";
        $script .= "\t\t\t\t\t<span class=\"icon-bin\"></span>\n";
        $script .= "\t\t\t\t\t<span class=\"screen-reader-text\">Delete One</span>\n";
        $script .= "\t\t\t\t</a>\n";
        $script .= "\t\t\t\t<a href=\"/{$this->dTable->GetName()}/Index\" class=\"tile\">\n";
        $script .= "\t\t\t\t\t<span class=\"icon-cross\"></span>\n";
        $script .= "\t\t\t\t\t<span class=\"screen-reader-text\">Annuleren</span>\n";
        $script .= "\t\t\t\t</a>\n";
        $script .= "\t\t\t</nav>\n";
        $script .= "\t\t</header>\n";

        $script .= $this->makeScriptFieldset('ReadingOne');

        $script .= "\t\t<footer class=\"feedback\">\n";
        $script .= "\t\t\t<p><?php echo \$model['message']; ?></p>\n";
        $script .= "\t\t\t<p><?php echo isset(\$model['error']) ? \$model['error'] : '';?></p>\n";
        $script .= "\t\t</footer>\n";
        $script .= "\t</section>\n";
        $script .= "\t<?php include('ReadingAll.php'); ?>\n";
        $script .= "</main>\n";

        return $script;
    }

    /** ------------------ makeScriptUpdatingOne  --------------------------
     *
     * Make script for UpdatingOne view
     * @lastmodified 24/04/2021
     * @param
     * @return string
     * @version 4.0
     * @since 01/06/2012
     * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M
     */
    private function makeScriptUpdatingOne($fileName)
    {
        // Create UpdatingOne View
        $script = "<!--  UpdatingOne View for {$this->dTable->GetName()} entity\n";
        $script .= " modernways.be\n";
        $script .= " created by an orm apart\n";
        $script .= " Entreprise de modes et de manières modernes\n";
        $script .= " created on " . date('l jS \of F Y h:i:s A') . "\n";
        $script .= " file name $fileName/UpdatingOne.php\n";
        $script .= "-->\n";

        $script .= "<main class=\"show-room entity\">\n";
        $script .= "\t<form class=\"detail\" id=\"form\" ";
        $script .= "action=\"/{$this->dTable->GetName()}/UpdateOne\" ";
        $script .= "method=\"post\">\n";

        $script .= "\t\t<header>\n";
        $script .= "\t\t\t<h2 class=\"banner\">Updating One {$this->dTable->GetName()}</h2>\n";
        $script .= "\t\t\t<nav class=\"command-panel\">\n";
        $script .= "\t\t\t\t<button type=\"submit\" value=\"updateOne\" name=\"updateOne\" class=\"tile\">\n";
        $script .= "\t\t\t\t\t<span class=\"icon-floppy-disk\"></span>\n";
        $script .= "\t\t\t\t\t<span class=\"screen-reader-text\">Update One</span>\n";
        $script .= "\t\t\t\t</button>\n";
        $script .= "\t\t\t\t<a href=\"/{$this->dTable->GetName()}/Index\" class=\"tile\">\n";
        $script .= "\t\t\t\t\t<span class=\"icon-cross\"></span>\n";
        $script .= "\t\t\t\t\t<span class=\"screen-reader-text\">Annuleren</span>\n";
        $script .= "\t\t\t\t</a>\n";
        $script .= "\t\t\t</nav>\n";
        $script .= "\t\t</header>\n";

        $script .= $this->makeScriptFieldset('UpdatingOne');

        $script .= "\t\t<footer class=\"feedback\">\n";
        $script .= "\t\t\t<p><?php echo \$model['message']; ?></p>\n";
        $script .= "\t\t\t<p><?php echo isset(\$model['error']) ? \$model['error'] : '';?></p>\n";
        $script .= "\t\t</footer>\n";
        $script .= "\t</form>\n";
        $script .= "\t<?php include('ReadingAll.php'); ?>\n";
        $script .= "</main>\n";

        return $script;
    }

    /** ------------------ makeScriptFieldset  --------------------------
     *
     * Make script for fields in fieldset in form
     * @lastmodified 21/03/2015
     * @param
     * @return string
     * @version 3.0
     * @since 01/06/2012
     * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M
     */
    private function makeScriptFieldset($action, $tabNum = 2)
    {
        $tab = str_repeat("\t", $tabNum);
        $script = "$tab<fieldset>\n";
        $disabled = (($action == 'ReadingOne') ? 'disabled' : '');
        $readOnly = (($action == 'ReadingOne') ? 'disabled' : '');
        foreach ($this->dTable->GetRows() as $row) {
            // required only for editing and inserting
            $required = (($action == 'CreatingOne' || $action == 'UpdatingOne') && $row->isRequired() ? 'required' : '');
            $checked = '';
            $value = '';
            if (!($row->IsPrimaryKey() && $action == 'CreatingOne')) {
                if ($row->isForeignKey()) {
                    if ($row->getHtmlTypeAttribute() == 'radio') {
                        $script .= $this->makeScriptRadioField($action, $row, $readOnly, $required);
                    } elseif ($row->getHtmlElement() == 'select') {
                        $script .= $this->makeScriptSelectField($action, $row, $readOnly, $required);
                    }
                } else {
                    if ($action == 'CreatingOne') {
                        switch ($row->getHtmlTypeAttribute()) {
                            case 'hidden' :
                                $readOnly = '';
                                $value = '';
                                break;
                            case 'text' :
                            case 'textarea' :
                            case 'email' :
                                $readOnly = $disabled;
                                $value = '';
                                break;
                            case 'checkbox' :
                                $readOnly = $disabled;
                                $value = '1';
                                break;
                            default:
                                $readOnly = $disabled;
                                $value = '';
                                break;
                        }
                    } else {
                        switch ($row->getHtmlTypeAttribute()) {
                            case 'hidden' :
                                // hidden fields must be posted!!! Because Id is in it.
                                $readOnly = '';
                                $value = "<?php echo \$model['row']['{$row->GetColumnName()}'];?>";
                                break;
                            case 'text' :
                            case 'textarea' :
                            case 'email' :
                                $readOnly = $disabled;
                                $value = "<?php echo \$model['row']['{$row->GetColumnName()}'];?>";
                                break;
                            case 'checkbox' :
                                $readOnly = $disabled;
                                $checked = "<?php echo (\model['row']['{$row->GetColumnName()}'] == 1 ? ' checked' : '');?>";
                                $value = '1';
                                break;
                            default:
                                $readOnly = $disabled;
                                $value = '';
                                break;
                        }
                    }
                    $script .= "$tab\t<div class=\"field\">\n";
                    $label = $this->makeScriptLabelField($row, 4);
                    // echo $row->getHtmlElement();
                    if ($row->getHtmlElement() == 'textarea') {
                        $script .= $label;
                        $script .= "$tab\t\t<textarea id=\"{$this->dTable->GetName()}-{$row->GetColumnName()}\" ";
                        $script .= "name=\"{$this->dTable->GetName()}-{$row->GetColumnName()}\" $required $readOnly";
                        $script .= ">$value</textarea>\n";
                    } else {
                        $input = "$tab\t\t<input id=\"{$this->dTable->GetName()}-{$row->GetColumnName()}\" name=\"{$this->dTable->GetName()}-{$row->GetColumnName()}\" ";
                        $input .= ($row->getCssClassName() ? "class=\"{$row->getCssClassName()}\" " : '');
                        $input .= $row->getCssInLineStyle();

                        $input .= "type=\"";
                        $input .= ($row->getHtmlTypeAttribute() ? $row->getHtmlTypeAttribute() : 'text');
                        $input .= "\" value=\"{$value}\" $required $readOnly $checked/>\n";
                        switch ($row->getHtmlTypeAttribute()) {
                            case 'hidden' :
                                $script .= $input;
                                break;
                            case 'checkbox' :
                                $script .= $input . $label;
                                break;
                            default:
                                $script .= $label . $input;
                                break;
                        }
                    }
                    $script .= "$tab\t</div>\n";
                }
            }
        }
        $script .= "$tab</fieldset>\n";
        return $script;
    }

    /** ------------------ makeScriptRadioField  --------------------------
     *
     * Make script for radio fields in form
     * @lastmodified 21/05/2015
     * @param
     * @return string
     * @version 3.0
     * @since 01/06/2012
     * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M
     */
    private function makeScriptRadioField($action, $row, $disabled, $required)
    {
        $checked = '';
        if ($action == 'CreatingOne') {
            $checked = "<?php echo (\$i++ == 1 ? ' checked' : '');?>";
        } elseif ($action == 'ReadOne' || $action == 'UpdatingOne') {
            $checked = "<?php echo (\$entity['Id'] == ";
            $checked .= "\$this->model->get{$this->dTable->GetNameUCFirst()}()->";
            $checked .= "geEntity()->get{$row->GetColumnName()}() ? ' checked' : '');?>";
        }

        $script = "\t\t\t<?php\n";
        $script .= "\t\t\t\tif (count(\$this->model->get{$row->getReferenceTable()}()->getEntitySet()) > 0)\n";
        $script .= "\t\t\t\t{\n";
        $script .= "\t\t\t\t\t\$i = 1;\n";
        $script .= "\t\t\t\t\tforeach (\$this->model->get{$row->getReferenceTable()}()->getEntitySet() as \$entity)\n";
        $script .= "\t\t\t\t\t{\n";
        $script .= "\t\t\t?>\n";
        $script .= "\t\t\t\t<div>\n";
        $script .= "\t\t\t\t\t<input type=\"radio\" name=\"{$this->dTable->GetName()}-{$row->GetColumnName()}\" \n";
        $script .= "\t\t\t\t\t\tvalue=\"<?php echo \$entity['Id'];?>\"\n";
        $script .= "\t\t\t\t\t\t$checked $disabled/>\n";
        $script .= $this->makeScriptLabelField($row, 5);
        $script .= "\t\t\t\t</div>\n";
        $script .= "\t\t\t<?php\n";
        $script .= "\t\t\t\t\t}\n";
        $script .= "\t\t\t\t}\n";
        $script .= "\t\t\t?>\n";
        return $script;
    }

    /** ------------------ makeScriptSelectField  --------------------------
     *
     * Make script for radio fields in form
     * @lastmodified 21/05/2015
     * @param
     * @return string
     * @version 3.0
     * @since 01/06/2012
     * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M
     */
    private function makeScriptSelectField($action, $row, $readOnly, $required)
    {
        $checked = '';
        if ($action == 'CreatingOne') {
            $selected = "<?php echo (\$i++ == 1 ? ' selected' : '');?>";
        } elseif ($action == 'ReadingOne' || $action == 'UpdatingOne') {
            $selected = "<?php echo (\$entity['Id'] == ";
            $selected .= "\$model->get{$row->GetColumnName()}() ? ' selected' : '');?>";
        }

        $script = "\t\t\t\t<div class=\"field\">\n";
        $script .= $this->makeScriptLabelField($row, 5);
        $script .= "\t\t\t\t\t<select id=\"{$this->dTable->GetName()}-{$row->GetColumnName()}\" ";
        $script .= "name=\"{$this->dTable->GetName()}-{$row->GetColumnName()}\" $required $readOnly>";

        $script .= "\t\t\t<?php\n";
        $script .= "\t\t\t\tif (count(\$model->get{$row->getColumnNameForeignKeyList()}()) > 0)\n";
        $script .= "\t\t\t\t{\n";
        $script .= "\t\t\t\t\t\$i = 1;\n";
        $script .= "\t\t\t\t\tforeach (\$model->get{$row->getColumnNameForeignKeyList()}() as \$entity)\n";
        $script .= "\t\t\t\t\t{\n";
        $script .= "\t\t\t?>\n";
        $script .= "\t\t\t\t\t<option value=\"<?php echo \$entity['Id'];?>\" $selected>\n";
        $script .= "\t\t\t\t\t\t<?php echo \$entity['{$row->GetReferenceDisplayColumn()}'];?>\n";
        $script .= "\t\t\t\t\t</option>\n";
        $script .= "\t\t\t<?php\n";
        $script .= "\t\t\t\t\t}\n";
        $script .= "\t\t\t\t}\n";
        $script .= "\t\t\t?>\n";

        $script .= "\t\t\t\t\t</select>\n";
        $script .= "\t\t\t\t</div>\n";
        return $script;
    }

    /** ------------------ makeScriptLabelField  --------------------------
     *
     * Make script for label fields in form
     * @lastmodified 21/05/2015
     * @param
     * @return string
     * @version 3.0
     * @since 01/06/2012
     * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M
     */
    private function makeScriptLabelField($row, $tabNum = 4, $seperator = '')
    {
        $tab = str_repeat("\t", $tabNum);
        return "$tab<label for=\"{$this->dTable->GetNameUCFirst()}-{$row->GetColumnName()}\">{$row->GetDisplayText()}$seperator</label>\n";
    }
}


