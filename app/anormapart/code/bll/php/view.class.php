<?php
/********************** Php View Catalog class ************************/
/**
 * Catalog class
 * Generates View classes PHP
 * @lastmodified 11/06/2016
 * @since 10/08/2015
 * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M
 * @version 4.0
 */
namespace AnOrmApart\Php;

class View extends \AnOrmApart\Catalog
{

    /** ------------------ addZipScriptViewClasses  --------------------------
     *
     * Create View classes for all tables and add to zip file
     * @lastmodified 11/06/2016
     * @since 01/06/2012
     * @author Jef Inghelbrecht - An Orm Apart
     * @version 3.0
     * @return string
     */
    public function zipAddScriptViewClasses($zip)
    {
        $script = '';
        if ($this->CatalogExists()) {
            // Make View class
            foreach ($this->catalog as $table) {
                // onthoud met welke tabel we bezig zijn
                $this->dTable = $table;
                $fileName = "{$this->GetDatabaseNameToLower()}/src/View/{$this->dTable->GetNameUCFirst()}.php";
                $zip->addFromString($fileName, $this->makeScriptViewClass($fileName));
            }
            return true;
        }
        return false;
    }

    /** ------------------ makeScriptViewClasses  --------------------------
     *
     * Create View classes for all entities
     * @lastmodified 11/06/2016
     * @since 01/06/2012
     * @author Jef Inghelbrecht - An Orm Apart
     * @version 3.0
     * @return string
     */
    public function makeScriptViewClasses()
    {
        $script = '';
        if ($this->CatalogExists()) {
            // Make View class
            foreach ($this->catalog as $table) {
                // onthoud met welke tabel we bezig zijn
                $this->dTable = $table;
                $fileName = "{$this->GetDatabaseNameToLower()}/src/View/{$this->dTable->GetNameUCFirst()}.php";
                $script .= $this->makeScriptViewClass($fileName);
            }
        }
        return $script;
    }

    /** ------------------ makeScriptViewClass  --------------------------
     *
     * Create View classes for one table
     * @lastmodified 10/08/2015
     * @since 01/06/2012
     * @author Jef Inghelbrecht - An Orm Apart
     * @version 3.0
     * @return string
     */
    public function makeScriptViewClass($fileName)
    {
        $script = "<?php\n";
        $script .= "/* modernways.be\n";
        $script .= " * created by an orm apart\n";
        $script .= " * Entreprise de modes et de manières modernes\n";
        $script .= " * View for {$this->GetDatabaseName()} app\n";
        $script .= " * Created on " . date('l jS \of F Y h:i:s A') . "\n";
        $script .= " * $fileName\n";
        $script .= "*/ \n";
        // first make the inherited classes\n";
        $script .= "// Write here your own code to change the\n";
        $script .= "// standard behaviour of the code generated\n";
        // php directive and namespace
        $script .= "namespace {$this->GetVendor()}\\{$this->GetDatabaseName()}\\View;\n";
        // class View
        $script .= "class {$this->dTable->GetNameUCFirst()}";
        // use always base class
        $script .= " extends \\{$this->GetVendor()}\\{$this->GetDatabaseName()}\\AnOrmApart\\View\\{$this->dTable->GetNameUCFirst()}";
        $script .= "\n";
        // begin class bll code block
        $script .= "{\n";
        $script .= "}\n";
        $script .= "\n";
        return $script;
    }

    /** ------------------ zipAddScriptViewAnOrmApartClasses  --------------------------
     *
     * Make CRUD scripts for all tables in database for PHP and add them to zip
     * @lastmodified 10/08/2015
     * @since 30/04/2015
     * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M
     * @version 0.2
     * @return void
     */
    public function zipAddScriptViewAnOrmApartClasses($zip)
    {
        if ($this->CatalogExists()) {
            foreach ($this->catalog as $table) {
                // onthoud met welke tabel we bezig zijn
                $this->dTable = $table;
                $fileName = "{$this->GetVendorToLower()}/{$this->GetDatabaseNameToLower()}/src/AnOrmApart/View/{$this->dTable->GetNameUCFirst()}.php";
                $zip->addFromString($fileName, $this->makeScriptViewAnOrmApartClass($fileName));
            }
        }
        return true;
    }

    /** ------------------ makeScriptViewAnOrmApartClasses  --------------------------
     *
     * Make CRUD scripts for all tables in database for PHP
     * @lastmodified 10/08/2015
     * @since 01/06/2012
     * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M
     * @version 0.2
     * @return string
     */
    public function makeScriptViewAnOrmApartClasses()
    {
        $script = '';
        if ($this->CatalogExists()) {
            $script = '';
            foreach ($this->catalog as $table) {
                // onthoud met welke tabel we bezig zijn
                $this->dTable = $table;
                $fileName = "{$this->GetVendorToLower()}/{$this->GetDatabaseNameToLower()}/src/AnOrmApart/View/{$this->dTable->GetNameUCFirst()}.php";
                $script .= $this->makeScriptViewAnOrmApartClass($fileName);
            }
            $script .= "\n";
        }
        return $script;
    }


    /** ------------------ makeScriptViewAnOrmApartClass  --------------------------
     *
     * Create View class for one table
     * @lastmodified 9/08/2014
     * @since 01/06/2012
     * @author Jef Inghelbrecht - An Orm Apart
     * @version 3.0
     * @return string
     */
    public function makeScriptViewAnOrmApartClass($fileName)
    {
        $script = "<?php\n";
        $script .= "/* modernways.be\n";
        $script .= " * created by an orm apart\n";
        $script .= " * Entreprise de modes et de manières modernes\n";
        $script .= " * View for {$this->GetDatabaseName()} app\n";
        $script .= " * Created on " . date('l jS \of F Y h:i:s A') . "\n";
        $script .= " * FileName: $fileName\n";
        $script .= "*/ \n";
        // make the AnOrmApart classes
        $script .= "// Code generated by An Orm Apart\n";
        $script .= "// do not modify the contents of this namespace\n";
        // php directive and namespace
        $script .= "namespace {$this->GetVendor()}\\{$this->GetDatabaseName()}\\AnOrmApart\\View;\n";
        // class View
        $script .= "class {$this->dTable->GetNameUCFirst()}";
        $script .= " extends \\{$this->GetVendor()}\\Helpers\\View\\Base";
        $script .= "\n";
        // begin class View code block
        $script .= "{\n";

        $script .= $this->makeScriptEditing();
        $script .= $this->makeScriptCreateOne();
        $script .= $this->makeScriptCreatingOne();
        $script .= $this->makeScriptDeleteOne();
        $script .= $this->makeScriptReadingAll();
        $script .= $this->makeScriptReadingOne();
        $script .= $this->makeScriptUpdateOne();
        $script .= $this->makeScriptUpdatingOne();

        // end class View block
        $script .= "}\n\n";
        return $script;
    }

    private function makeScriptCreateOne()
    {
        $script = "\tpublic function  createOne()\n";
        $script .= "\t{\n";
        $script .= "\t\tinclude('Template/{$this->dTable->GetNameUCFirst()}/CreatingOne.php');\n";
        $script .= "\t}\n";
        return $script;
    }


    private function makeScriptCreatingOne()
    {
        $script = "\tpublic function  creatingOne()\n";
        $script .= "\t{\n";
        $script .= "\t\tinclude('Template/{$this->dTable->GetNameUCFirst()}/CreatingOne.php');\n";
        $script .= "\t}\n";
        return $script;
    }

    private function makeScriptDeleteOne()
    {
        $script = "\tpublic function  deleteOne()\n";
        $script .= "\t{\n";
        $script .= "\t\tinclude('Template/{$this->dTable->GetNameUCFirst()}/Editing.php');\n";
        $script .= "\t}\n";
        return $script;
    }


    private function makeScriptEditing()
    {
        $script = "\tpublic function  editing()\n";
        $script .= "\t{\n";
        $script .= "\t\tinclude('Template/{$this->dTable->GetNameUCFirst()}/Editing.php');\n";
        $script .= "\t}\n";
        return $script;
    }

    private function makeScriptReadingAll()
    {
        $script = "\tpublic function  readingAll()\n";
        $script .= "\t{\n";
        $script .= "\t\tinclude('Template/{$this->dTable->GetNameUCFirst()}/ReadingAll.php');\n";
        $script .= "\t}\n";
        return $script;
    }

    private function makeScriptReadingOne()
    {
        $script = "\tpublic function  readingOne()\n";
        $script .= "\t{\n";
        $script .= "\t\tinclude('Template/{$this->dTable->GetNameUCFirst()}/ReadingOne.php');\n";
        $script .= "\t}\n";
        return $script;
    }

    private function makeScriptUpdateOne()
    {
        $script = "\tpublic function  updateOne()\n";
        $script .= "\t{\n";
        $script .= "\t\tinclude('Template/{$this->dTable->GetNameUCFirst()}/ReadingOne.php');\n";
        $script .= "\t}\n";
        return $script;
    }


    private function makeScriptUpdatingOne()
    {
        $script = "\tpublic function  updatingOne()\n";
        $script .= "\t{\n";
        $script .= "\t\tinclude('Template/{$this->dTable->GetNameUCFirst()}/UpdatingOne.php');\n";
        $script .= "\t}\n";
        return $script;
    }

}
    