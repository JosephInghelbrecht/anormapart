<?php
/********************** Php Template code ************************/
/**
 * Template
 * Generates Templates for PHP
 *
 * You can use phpDocumentor to generate documentation
 *
 *
 * @lastmodified 11/06/2016
 * @since 01/06/2012
 * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M
 * @version 3.0
 */
namespace AnOrmApart\Php;
class Template extends \AnOrmApart\Catalog
{
    /** ------------------ zipAddScriptTemplateAll  --------------------------
     *
     * Make Template scripts for all entities (tables) in database for PHP
     * @lastmodified 11/06/2016
     * @since 01/06/2012
     * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M
     * @version 3.0
     * @return string
     */
    public function zipAddScriptTemplateAll($zip)
    {
        if ($this->CatalogExists()) {
            foreach ($this->catalog as $table) {
                // onthoud met welke tabel we bezig zijn
                $this->dTable = $table;
                $filePath = "src/View/{$this->dTable->GetNameUCFirst()}";
                // php directive and namespace
                $zip->addFromString($filePath . '/Editing.php', $this->makeScriptFirstLevelDetail($filePath, 'Editing'));
                $zip->addFromString($filePath . '/CreatingOne.php', $this->makeScriptFirstLevelDetail($filePath, 'CreatingOne'));
                $zip->addFromString($filePath . '/ReadingOne.php', $this->makeScriptFirstLevelDetail($filePath, 'ReadingOne'));
                $zip->addFromString($filePath . '/UpdatingOne.php', $this->makeScriptFirstLevelDetail($filePath, 'UpdatingOne'));
                $zip->addFromString($filePath . '/ReadingAll.php', $this->makeScriptReadingAll($filePath));
            }
            return TRUE;
        }
        return false;
    }

    /** ------------------ makeScriptTemplateAll  --------------------------
     *
     * Make Template scripts for all entities (tables) in database for PHP
     * @lastmodified 11/06/2016
     * @since 01/06/2012
     * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M
     * @version 4.0
     * @return string
     */
    public function makeScriptTemplateAll()
    {
        $script = '';
        if ($this->CatalogExists()) {
            foreach ($this->catalog as $table) {
                // var_dump($table);
                // onthoud met welke tabel we bezig zijn
                $this->dTable = $table;
                $fileName = "src/View/Template{$this->dTable->GetNameUCFirst()}";
                // php directive and namespace
                $script .= $this->makeScriptFirstLevelDetail($fileName, 'Editing');
                $script .= $this->makeScriptFirstLevelDetail($fileName, 'CreatingOne');
                $script .= $this->makeScriptReadingAll($fileName);
                $script .= $this->makeScriptFirstLevelDetail($fileName, 'ReadingOne');
                $script .= $this->makeScriptFirstLevelDetail($fileName, 'UpdatingOne');
                $script .= "\n";
            }
        }
        return $script;
    }

    /** ------------------ makeScriptTemplateAll  --------------------------
     *
     * Make Template components scripts for all entities (tables) in database for PHP
     * @lastmodified 25/08/2016
     * @since 01/06/2012
     * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M
     * @version 4.0
     * @return string
     */
    public function makeScriptTemplateComponentsAll()
    {
        $script = '';
        if ($this->CatalogExists()) {
            foreach ($this->catalog as $table) {
                // var_dump($table);
                // onthoud met welke tabel we bezig zijn
                $this->dTable = $table;
                $fileName = "src/View/Template{$this->dTable->GetNameUCFirst()}";
                // php directive and namespace
                $script .= $this->makeScriptFieldSetShowOne();
                $script .= "\n";
            }
        }
        return $script;
    }

    /** ------------------ makeScriptFront  --------------------------
     *
     * Make script for Editing Template
     * @lastmodified 11/06/2016
     * @since 01/06/2012
     * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M
     * @version 4.0
     * @param
     * @return string
     */
    private function makeScriptFront()
    {
        $script = "\t\t<header class=\"front\">\n";
        $script .= "\t\t\t<nav class=\"control-panel\">\n";
        $script .= $this->makeScriptButton('Home', 'Index', 'icon-menu2', 'Sluiten');
        $script .= "\t\t\t</nav>\n";
        $script .= "\t\t\t<h1>{$this->getAppName()}</h1>\n";
        $script .= "\t\t</header>\n";
        return $script;
    }

    /** ------------------ makeScriptFirstLevelDetail  --------------------------
     *
     * Make script for Editing Template
     * @lastmodified 11/06/2016
     * @since 01/06/2012
     * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M
     * @version 3.0
     * @param
     * @return string
     */
    private function makeScriptFirstLevelDetail($fileName, $action)
    {
        // Create Editing Template
        $script = "<!--  $action Template for {$this->dTable->GetName()} entity\n";
        $script .= " modernways.be\n";
        $script .= " created by an orm apart\n";
        $script .= " Entreprise de modes et de manières modernes\n";
        $script .= " created on " . date('l jS \of F Y h:i:s A') . "\n";
        $script .= " file name {$this->GetDatabaseNameUCFirst()}/$fileName/$action.php\n";
        $script .= "-->\n";
        $script .= "<div class=\"tower\">\n";
        $script .= "\t<form class=\"floor\" id=\"single-entity-floor\" ";
        $script .= "action=\"<?php echo htmlentities(\$_SERVER['PHP_SELF'], ENT_QUOTES);?>\" ";
        $script .= "method=\"post\">\n";

        $script .= $this->makeScriptFront();

        $script .= "\t\t<article class=\"showroom\">\n";
        $script .= "\t\t<?php \$partialView('{$this->dTable->GetName()}', 'ReadingAll', \$model); ?>\n";
        $script .= $this->makeScriptField($action);
        $script .= "\t\t</article>\n";
        $script .= "\t</form>\n";
        $script .= "</div>\n";
        $script .= "<?php \$appStateView(); ?>\n\n";
        return $script;
    }

    /** ------------------ makeScriptLabelField  --------------------------
     *
     * Make script for label fields in form
     * @lastmodified 21/05/2015
     * @since 01/06/2012
     * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M
     * @version 3.0
     * @param
     * @return string
     */
    private function makeScriptLabelField($tabNum = 4, $row, $seperator = '')
    {
        $tab = str_repeat("\t", $tabNum);
        return "$tab<label for=\"{$this->dTable->GetNameUCFirst()}-{$row->GetColumnName()}\">{$row->GetDisplayText()}$seperator</label>\n";
    }

    /** ------------------ makeScriptRadioField  --------------------------
     *
     * Make script for radio fields in form
     * @lastmodified 21/05/2015
     * @since 01/06/2012
     * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M
     * @version 3.0
     * @param
     * @return string
     */
    private function makeScriptRadioField($action, $row, $disabled, $required)
    {
        $checked = '';
        if ($action == 'CreatingOne') {
            $checked = "<?php echo (\$i++ == 1 ? ' checked' : '');?>";
        } elseif ($action == 'ReadOne' || $action == 'UpdatingOne') {
            $checked = "<?php echo (\$entity['Id'] == ";
            $checked .= "\$this->model->get{$this->dTable->GetNameUCFirst()}()->";
            $checked .= "geEntity()->get{$row->GetColumnName()}() ? ' checked' : '');?>";
        }

        $script = "\t\t\t<?php\n";
        $script .= "\t\t\t\tif (count(\$this->model->get{$row->getReferenceTable()}()->getEntitySet()) > 0)\n";
        $script .= "\t\t\t\t{\n";
        $script .= "\t\t\t\t\t\$i = 1;\n";
        $script .= "\t\t\t\t\tforeach (\$this->model->get{$row->getReferenceTable()}()->getEntitySet() as \$entity)\n";
        $script .= "\t\t\t\t\t{\n";
        $script .= "\t\t\t?>\n";
        $script .= "\t\t\t\t<div>\n";
        $script .= "\t\t\t\t\t<input type=\"radio\" name=\"{$this->dTable->GetName()}-{$row->GetColumnName()}\" \n";
        $script .= "\t\t\t\t\t\tvalue=\"<?php echo \$entity['Id'];?>\"\n";
        $script .= "\t\t\t\t\t\t$checked $disabled/>\n";
        $script .= $this->makeScriptLabelField(5, $row);
        $script .= "\t\t\t\t</div>\n";
        $script .= "\t\t\t<?php\n";
        $script .= "\t\t\t\t\t}\n";
        $script .= "\t\t\t\t}\n";
        $script .= "\t\t\t?>\n";
        return $script;
    }

    /** ------------------ makeScriptSelectField  --------------------------
     *
     * Make script for radio fields in form
     * @lastmodified 21/05/2015
     * @since 01/06/2012
     * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M
     * @version 3.0
     * @param
     * @return string
     */
    private function makeScriptSelectField($action, $row, $readOnly, $required)
    {
        $checked = '';
        if ($action == 'CreatingOne') {
            $selected = "<?php echo (\$i++ == 1 ? ' selected' : '');?>";
        } elseif ($action == 'ReadingOne' || $action == 'UpdatingOne') {
            $selected = "<?php echo (\$entity['Id'] == ";
            $selected .= "\$model->get{$row->GetColumnName()}() ? ' selected' : '');?>";
        }

        $script = "\t\t\t\t<div class=\"field\">\n";
        $script .= $this->makeScriptLabelField(5, $row);
        $script .= "\t\t\t\t\t<select id=\"{$this->dTable->GetName()}-{$row->GetColumnName()}\" ";
        $script .= "name=\"{$this->dTable->GetName()}-{$row->GetColumnName()}\" $required $readOnly>";

        $script .= "\t\t\t<?php\n";
        $script .= "\t\t\t\tif (count(\$model->get{$row->getColumnNameForeignKeyList()}()) > 0)\n";
        $script .= "\t\t\t\t{\n";
        $script .= "\t\t\t\t\t\$i = 1;\n";
        $script .= "\t\t\t\t\tforeach (\$model->get{$row->getColumnNameForeignKeyList()}() as \$entity)\n";
        $script .= "\t\t\t\t\t{\n";
        $script .= "\t\t\t?>\n";
        $script .= "\t\t\t\t\t<option value=\"<?php echo \$entity['Id'];?>\" $selected>\n";
        $script .= "\t\t\t\t\t\t<?php echo \$entity['{$row->GetReferenceDisplayColumn()}'];?>\n";
        $script .= "\t\t\t\t\t</option>\n";
        $script .= "\t\t\t<?php\n";
        $script .= "\t\t\t\t\t}\n";
        $script .= "\t\t\t\t}\n";
        $script .= "\t\t\t?>\n";

        $script .= "\t\t\t\t\t</select>\n";
        $script .= "\t\t\t\t</div>\n";
        return $script;
    }

    private function makeScriptDetailHeader($action)
    {
        $script = "\t\t\t<header>\n";
        $script .= "\t\t\t\t<h2>{$this->dTable->GetText()} $action</h2>\n";
        $script .= "\t\t\t\t<nav id=\"command-panel\" class=\"command-panel\">\n";
        switch ($action) {
            case 'Editing' :
                $script .= $this->makeScriptButton($this->dTable->GetNameUCFirst(), 'creatingOne', 'icon-plus', 'Nieuw');
                break;
            case 'CreatingOne' :
                $script .= $this->makeScriptButton($this->dTable->GetNameUCFirst(), 'editing', 'icon-close', 'Sluiten');
                $script .= $this->makeScriptButton($this->dTable->GetNameUCFirst(), 'createOne', 'icon-disk', 'Opslaan');
                break;
            case 'ReadingOne' :
                $script .= $this->makeScriptButton($this->dTable->GetNameUCFirst(), 'deleteOne', 'icon-remove', 'Delete');
                $script .= $this->makeScriptButton($this->dTable->GetNameUCFirst(), 'creatingOne', 'icon-plus', 'Nieuw');
                $script .= $this->makeScriptButton($this->dTable->GetNameUCFirst(), 'updatingOne', 'icon-pencil', 'Updaten');
                break;
            case 'UpdatingOne' :
                $script .= $this->makeScriptButton($this->dTable->GetNameUCFirst(), 'editing', 'icon-close', 'Sluiten');
                $script .= $this->makeScriptButton($this->dTable->GetNameUCFirst(), 'updateOne', 'icon-disk', 'Opslaan');
                break;
        }

        $script .= "\t\t\t\t</nav>\n";
        $script .= "\t\t\t</header>\n";
        return $script;
    }

    /** ------------------ makeScriptField  --------------------------
     *
     * Make script for fields in form
     * @lastmodified 21/03/2015
     * @since 01/06/2012
     * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M
     * @version 3.0
     * @param
     * @return string
     */
    private function makeScriptField($action)
    {
        $script = "\t\t<aside class=\"detail\">\n";
        $script .= $this->makeScriptDetailHeader($action);
        if ($action != 'Editing') {
            $script .= "\t\t\t<fieldset>\n";
            $disabled = (($action == 'ReadingOne') ? 'disabled' : '');
            $readOnly = (($action == 'ReadingOne') ? 'disabled' : '');
            foreach ($this->dTable->GetRows() as $row) {
                // required only for editing and inserting
                $required = (($action == 'CreatingOne' || $action == 'UpdatingOne') && $row->isRequired() ? 'required' : '');
                $checked = '';
                $value = '';
                if (!($row->IsPrimaryKey() && $action == 'CreatingOne')) {
                    if ($row->isForeignKey()) {
                        if ($row->getHtmlTypeAttribute() == 'radio') {
                            $script .= $this->makeScriptRadioField($action, $row, $readOnly, $required);
                        } elseif ($row->getHtmlElement() == 'select') {
                            $script .= $this->makeScriptSelectField($action, $row, $readOnly, $required);
                        }
                    } else {
                        if ($action == 'CreatingOne') {
                            switch ($row->getHtmlTypeAttribute()) {
                                case 'hidden' :
                                    $readOnly = '';
                                    $value = '';
                                    break;
                                case 'text' :
                                case 'textarea' :
                                case 'email' :
                                    $readOnly = $disabled;
                                    $value = '';
                                    break;
                                case 'checkbox' :
                                    $readOnly = $disabled;
                                    $value = '1';
                                    break;
                                default:
                                    $readOnly = $disabled;
                                    $value = '';
                                    break;
                            }
                        } else {
                            switch ($row->getHtmlTypeAttribute()) {
                                case 'hidden' :
                                    // hidden fields must be posted!!! Because Id is in it.
                                    $readOnly = '';
                                    $value = "<?php echo \$model->get{$row->GetColumnName()}();?>";
                                    break;
                                case 'text' :
                                case 'textarea' :
                                case 'email' :
                                    $readOnly = $disabled;
                                    $value = "<?php echo \$model->get{$row->GetColumnName()}();?>";
                                    break;
                                case 'checkbox' :
                                    $readOnly = $disabled;
                                    $checked = "<?php echo (\$model->get{$row->GetColumnName()}() == 1 ? ' checked' : '');?>";
                                    $value = '1';
                                    break;
                                default:
                                    $readOnly = $disabled;
                                    $value = '';
                                    break;
                            }
                        }
                        $script .= "\t\t\t\t<div class=\"field\">\n";
                        $label = $this->makeScriptLabelField(4, $row);
                        // echo $row->getHtmlElement();
                        if ($row->getHtmlElement() == 'textarea') {
                            $script .= $label;
                            $script .= "\t\t\t\t\t<textarea id=\"{$this->dTable->GetName()}-{$row->GetColumnName()}\" ";
                            $script .= "name=\"{$this->dTable->GetName()}-{$row->GetColumnName()}\" $required $readOnly";
                            $script .= ">$value</textarea>\n";
                        } else {
                            $input = "\t\t\t\t\t<input id=\"{$this->dTable->GetName()}-{$row->GetColumnName()}\" name=\"{$this->dTable->GetName()}-{$row->GetColumnName()}\" ";
                            $input .= ($row->getCssClassName() ? "class=\"{$row->getCssClassName()}\" " : '');
                            $input .= $row->getCssInLineStyle();

                            $input .= "type=\"";
                            $input .= ($row->getHtmlTypeAttribute() ? $row->getHtmlTypeAttribute() : 'text');
                            $input .= "\" value=\"{$value}\" $required $readOnly $checked/>\n";
                            switch ($row->getHtmlTypeAttribute()) {
                                case 'hidden' :
                                    $script .= $input;
                                    break;
                                case 'checkbox' :
                                    $script .= $input . $label;
                                    break;
                                default:
                                    $script .= $label . $input;
                                    break;
                            }
                        }
                        $script .= "\t\t\t\t</div>\n";
                    }
                }
            }
            $script .= "\t\t\t</fieldset>\n";
        }
        $script .= "\t\t</aside>\n";
        $script .= "\t\t<div class=\"feedback\">\n";
        $script .= "\t\t</div>\n";
        return $script;
    }

    /** ------------------ makeScriptReadingAll  --------------------------
     *
     * Make script for Read All Template
     * @lastmodified 11/08/2015
     * @since 01/06/2012
     * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M
     * @version 3.0
     * @param
     * @return string
     */
    private function makeScriptReadingAll($fileName)
    {
        // Create Reading all Template
        $script = "<!--  reading all Template for {$this->dTable->GetName()} entity\n";
        $script .= " modernways.be\n";
        $script .= " created by an orm apart\n";
        $script .= " Entreprise de modes et de manières modernes\n";
        $script .= " created on " . date('l jS \of F Y h:i:s A') . "\n";
        $script .= " file name $fileName/ReadAll.php\n";
        $script .= "-->\n";
        $script .= "<aside class=\"list\">\n";
        $script .= "\t<?php\n";
        $script .= "\t\tif (count(\$model->getList()) > 0)\n";
        $script .= "\t\t{\n";
        $script .= "\t?>\n";
        $script .= "\t<table class=\"select\">\n";
        $script .= "\t\t<?php\n";

        $script .= "\t\t\tforeach (\$model->getList() as \$item)\n";
        $script .= "\t\t\t{\n";
        $script .= "\t\t?>\n";
        $script .= "\t\t<tr>\n";
        $script .= "\t\t\t<td>\n";
        $script .= "\t\t\t\t<button type=\"submit\" class=\"tile\" value=\"{$this->dTable->GetNameUCFirst()}-readingOne_<?php echo \$item['Id'];?>\" name=\"uc\">\n";
        $script .= "\t\t\t\t<span class=\"icon-arrow-right\"></span><span class=\"screen-reader-text\">Select</span></button>\n";
        $script .= "\t\t\t</td>\n";
        // all searchable columns
        foreach ($this->dTable->GetRows() as $row) {
            // create for each searchable column a column in the table or
            // a list item in the list
            if ($row->isList()) {
                $script .= "\t\t\t<td>\n";
                // if foreignkey list corresponding name and not foreignkey itself
                if ($row->isForeignKey()) {
                    $script .= "\t\t\t\t<?php echo \$item['{$row->GetColumnName()}{$row-> GetReferenceDisplayColumn()}'];?>\n";

                } else {
                    $script .= "\t\t\t\t<?php echo \$item['{$row->GetColumnName()}'];?>\n";
                }
                $script .= "\t\t\t</td>\n";
            }
        }
        $script .= "\n";
        $script .= "\t\t</tr>\n";
        $script .= "\t\t<?php\n";
        $script .= "\t\t}\n";
        $script .= "\t\t?>\n";
        $script .= "\t</table>\n";
        $script .= "\t<?php\n";
        $script .= "\t\t}\n";
        $script .= "\t\telse\n";
        $script .= "\t\t{\n";
        $script .= "\t?>\n";
        $script .= "\t<p>Geen {$this->dTable->GetText()}</p>\n";
        $script .= "\t<?php\n";
        $script .= "\t}\n";
        $script .= "\t?>\n";
        $script .= "</aside>\n\n";
        return $script;
    }

    private function makeScriptButton($entity, $action, $icon, $text)
    {
        $script = "\t\t\t\t<button class=\"tile\" ";
        if ($icon == 'icon-close' || $icon == 'icon-menu2') {
            $script .= "type=\"button\" onclick=\"window.location='<?php echo htmlentities(\$_SERVER['PHP_SELF'], ENT_QUOTES)?>?uc=$entity-$action'\">\n";
        } else {
            $script .= "type=\"submit\" value=\"$entity-$action\" name=\"uc\">\n";
        }
        $script .= "\t\t\t\t\t<span class=\"$icon\"></span><span class=\"screen-reader-text\">$text</span></button>\n";
        return $script;
    }

    /** ------------------ makeScriptField  --------------------------
     *
     * Make script for fields in form Denis style
     * @lastmodified 25/08/2016
     * @since 01/06/2012
     * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M
     * @version 3.0
     * @param
     * @return string
     */
    public function makeScriptFieldSetShowOne()
    {
        $script = "\t\t\t<fieldset>\n";

        foreach ($this->dTable->GetRows() as $row) {
            if ($row->isPrimaryKey()) {
                $script .= "\t\t\t\t<input id=\"{$this->dTable->GetName()}-{$row->GetColumnName()}\" name=\"{$this->dTable->GetName()}-{$row->GetColumnName()}\" ";
                $script .= "type=\"hidden\" value=\"<?php echo \$model->get{$row->GetColumnName()}();?>\"/>\n";

            } else {
                $script .= "\t\t\t\t<div class=\"field\">\n";

                $script .= $this->makeScriptLabelField(5, $row, ': ');
                $script .= "\t\t\t\t\t<span id=\"{$this->dTable->GetName()}-{$row->GetColumnName()}\" name=\"{$this->dTable->GetName()}-{$row->GetColumnName()}\" ";
                $script .= ($row->getCssClassName() ? "class=\"{$row->getCssClassName()}\" " : '');
                $script .= $row->getCssInLineStyle();

                if  ($row->isForeignKey()) {
                    $script .= "><?php echo \$model->get{$row->GetReferenceTable()}{$row->GetReferenceDisplayColumn()}();?></span>\n";
                } else {
                    if ($row->isBool()) {
                        $script .= "><?php echo (\$model->get{$row->GetColumnName()}() == 1 ? 'Ja' : 'Neen');?></span>\n";
                    } else {
                        $script .= "><?php echo \$model->get{$row->GetColumnName()}();?></span>\n";
                    }
                }
                $script .= "\t\t\t\t</div>\n";

            }
        }
        $script .= "\t\t\t</fieldset>\n";
        return $script;
    }
}



