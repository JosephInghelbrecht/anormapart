<?php
/********************** Php Unit test Catalog class ************************
* Unittest class
* Generates Unittest for DAL table class PHP
*
* You can use phpDocumentor to generate documentation
*
* Met Unittesten worden afzonderlijke softwaremodulen of stukken broncode (units) afzonderlijk getest. 
* Bij unittesten wordt voor iedere unit een of meerdere tests ontwikkeld ontwikkeld die verschillende testcases doorlopen. 
* In het ideale geval zijn alle testcases onafhankelijk van andere tests. 
* Eventueel worden hiertoe zogenaamde mockobjecten gemaakt om de unittests gescheiden uit te kunnen voeren.
* Bovenstanande definitie komt van Wikipedia.
* 
* De testcase hier bestaat uit het testen van de DAL klasse van een tabel.
*
* @lastmodified 28/10/2013 Start
* @since 01/06/2012           
* @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M        
* @version 0.2
*/
namespace AnOrmApart\Php;
    
class DalTest extends \AnOrmApart\Catalog
{
    
    /** ------------------ MakeScriptDalTestAll  --------------------------
    *
    * Make Unittest scripts for all tables in database for PHP
    * @lastmodified 28/10/2014
    * @since 01/06/2012           
    * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M        
    * @version 0.2
    * @return string  
    */
    public function makeScriptDalTestAll()
    {
        $script = '';
        if ($this->CatalogExists())
        {
            foreach ($this->catalog as $table)
            {
                // onthoud met welke tabel we bezig zijn
                $this->dTable = $table;
                $script .= "<?php\n";
                $script .= $this->MakeScriptDalTest();
                $script .= '?>';
                $script .= "\n";
                $script .= $this->makeScriptLogView(TRUE);
                $script .= "\n";
            }
        }
        return $script;
    }
    
    /** ------------------ makeScriptBllTestAll  --------------------------
    *
    * Make Unittest scripts for all tables in database for PHP
    * @lastmodified 28/10/2014
    * @since 01/06/2012           
    * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M        
    * @version 0.2
    * @return string  
    */
    public function makeScriptBllTestAll()
    {
        $script = '';
        if ($this->CatalogExists())
        {
            foreach ($this->catalog as $table)
            {
                // onthoud met welke tabel we bezig zijn
                $this->dTable = $table;
                $script .= "<?php\n";
                $script .= $this->makeScriptBllTest();
                $script .= '?>';
                $script .= "\n";
                $script .= $this->makeScriptLogView(FALSE);
                $script .= "\n";
            }
        }
        return $script;
    }
    
    /** ------------------ makeScriptTestSetters  --------------------------
    *
    * Make Unit Tests for setters in scripts for CRUD class in PHP
    * @lastmodified 4/5/2014
    * @since 01/06/2012           
    * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M        
    * @version 0.2
    * @param $method: insert, update
    * @return string  
    */
    public function makeScriptTestSetters($method)
    {
        $script = '';
        foreach ($this->dTable->GetRows() as $row)
        {
            if ($row->getType() == 'TIMESTAMP')
            {
                $script .= "\t\$bll->set{$row->GetColumnName()}();\n";
            }
            elseif ($row->IsPassword())
            {
                $script .= "\t\$bll->hash{$row->GetColumnName()}('password');\n";
            }
            elseif ($row->getType() == 'CHAR')
            {
                $script .= "\t\$bll->set{$row->GetColumnName()}('{$row->GetColumnName()}');\n";
            }
            elseif ($row->getType() == 'INT')
            {
                $script .= "\t\$bll->set{$row->GetColumnName()}(999);\n";
            }
                elseif ($row->getType() == 'BOOL')
            {
                $script .= "\t\$bll->set{$row->GetColumnName()}(TRUE);\n";
            }
            elseif ($row->getType() == 'FLOAT')
            {
                $script .= "\t\$bll->set{$row->GetColumnName()}(0.999);\n";
            }
        }
        return $script;
    }
    
    /** ------------------ makeScriptDalTest  --------------------------
    *
    * Make Unit Test scripts for CRUD class in PHP
    * @lastmodified 28/10/2014
    * @since 01/06/2012           
    * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M        
    * @version 0.2
    * @param array $rows
    * @return string  
    */
    public function makeScriptDalTest()
    {
        // include connection class
        $script = "\t// Begin DAL Test\n";
        $script .= "\tdefine('VENDOR_PATH', \$_SERVER['DOCUMENT_ROOT'] . '/{$this->GetNamespace()}/');\n";
        $script .= "\tdefine('APP_PATH', \$_SERVER['DOCUMENT_ROOT'] . '/{$this->GetNamespace()}/{$this->GetDatabaseName()}/');\n";
        // include feedback class
        $script .= "\t// include Feedback and Log class\n";
        $script .= "\tinclude (VENDOR_PATH . 'Helpers/Feedback.php');\n";
        $script .= "\tinclude (VENDOR_PATH . 'Helpers/Log.php');\n";            
        $script .= "\t// include connection class\n";
        $script .= "\tinclude (VENDOR_PATH . 'Helpers/Dal/Connection.php');\n";            
        $script .= "\t// Include the connection class for the {$this->GetNamespaceToLower()} app\n";
        $script .= "\tinclude (APP_PATH . 'Dal/Provider.php');\n";            
        if ($this->dTable->IsExtend())
        {
            $script .= "\tinclude (VENDOR_PATH . 'Helpers/Bll/Base.php');\n";            
            $script .= "\tinclude (VENDOR_PATH . 'Helpers/Dal/Base.php');\n";            
        }
        // include bll for table
        $script .= "\t// include bll class for table\n";
        $script .= "\tinclude (APP_PATH . 'Bll/Codebehind/{$this->dTable->GetNameUCFirst()}.php');\n";
        $script .= "\tinclude (APP_PATH . 'Bll/{$this->dTable->GetNameUCFirst()}.php');\n";
        // include dal class for table
        $script .= "\t// include dal class for table\n";
        $script .= "\tinclude (APP_PATH . 'Dal/Codebehind/{$this->dTable->GetNameUCFirst()}.php');\n";
        $script .= "\tinclude (APP_PATH . 'Dal/{$this->dTable->GetNameUCFirst()}.php');\n";
        $script .= "\t// only required when there is a password property\n";
        $script .= "\t// include (VENDOR_PATH . 'Helpers/Password.php');\n";            
        // instantiate log class
        $script .= "\t\$log = new \{$this->GetNamespace()}\Helpers\Log();\n";
        // connection
        $script .= "\t// connect using the {$this->GetNamespace()} Connection class\n";
        $script .= "\t\$connection = new \\{$this->GetNamespace()}\\{$this->GetDatabaseName()}\\Dal\\Provider(\$log);\n";
        $script .= "\t\$connection->open();\n";
        // code for class instanciation of the dal table class
        $script .= "\t// create an instance of the DAL class for this table\n";
        $script .= "\t\$dal = ";
        $script .= "new \\{$this->GetNamespace()}\\{$this->GetDatabaseName()}\\";
        $script .= "Dal\\{$this->dTable->GetNameUCFirst()}(\$log);\n";
        // code for class instanciation of the bll table class
        $script .= "\t// create an instance of the Bll class for this table\n";
        $script .= "\t\$bll = ";
        $script .= "new \\{$this->GetNamespace()}\\{$this->GetDatabaseName()}\\Bll";
        $script .= "\\{$this->dTable->GetNameUCFirst()}(\$log);\n";
    
        // we start with making a business object
        $script .= "\t// we start with making a business object\n";
        $script .= $this->makeScriptTestSetters('I');
   
        // insert
        $script .= "\t// insert\n";
        $script .= "\t// pass the business dataobject to the DAL class\n";
        $script .= "\t\$dal->setBdo(\$bll);\n";
        $script .= "\t// pass the connection object to the DAL class\n";
        $script .= "\t\$dal->setConnection(\$connection);\n";

        $script .= "\t// and now it's time to insert\n";
        $script .= "\t\$dal->insert();\n\n";        
        
        // select one
        $script .= "\t// de laatste insert zette de nieuw Id in\n";
        $script .= "\t// het id veld van het BLL object\n";
        $script .= "\t\$dal->selectOne();\n\n";

        // select all
        $script .= "\t\$list = \$dal->selectAll();\n\n";

        // update
        $script .= "\t// update section\n";
        $script .= "\t// first change bdo\n";
        $script .= "\t// now update\n";
        $script .= "\t// No need to pass bdo object again to dal\n";
        $script .= "\t// the bdo instance of the dal still\n";
        $script .= "\t// references the \$bbl here.\n";
        $script .= "\t\$dal->update();\n";

        // delete
        $script .= "\t// delete row with Id set in bll object\n";
        $script .= "\t\$dal->delete();\n";
        $script .= "\t// set Id to not existant number\n";
        $script .= "\t\$bll->setId(-1);\n";
        $script .= "\t\$dal->delete();\n";

        // disconnect
        $script .= "\t// disconnect\n";
        $script .= "\t\$connection->close();\n\n";
        $script .= "\n";
        return $script;
    }

    
    /** ------------------ MakeScriptBllTest  --------------------------
    *
    * Make Unit Test scripts for bLL class in PHP
    * @lastmodified 28/10/2014
    * @since 01/06/2012           
    * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M        
    * @version 0.2
    * @return string  
    */
    public function makeScriptBllTest()
    {
        $script = "\tdefine('VENDOR_PATH', \$_SERVER['DOCUMENT_ROOT'] . '/{$this->GetNamespace()}/');\n";
        $script .= "\tdefine('APP_PATH', \$_SERVER['DOCUMENT_ROOT'] . '/{$this->GetNamespace()}/{$this->GetDatabaseName()}/');\n";
        // include feedback class
        // normaal gezien staat de BLL base in dezelfde map als log en feedback

        $script .= "\tinclude (VENDOR_PATH . 'Helpers/Feedback.php');\n";
        $script .= "\tinclude (VENDOR_PATH . 'Helpers/Log.php');\n";            
        // include bll for table
        if ($this->dTable->IsExtend())
        {
            $script .= "\tinclude (VENDOR_PATH . 'Helpers/Bll/Base.php');\n";
        }
        $script .= "\tinclude (APP_PATH . '/Bll/CodeBehind/{$this->dTable->GetNameUCFirst()}.php');\n";
        $script .= "\tinclude (APP_PATH . '/Bll/{$this->dTable->GetNameUCFirst()}.php');\n";
        $script .= "\t// only required when there is a password property\n";
        $script .= "\t// include (VENDOR_PATH . 'Helpers/Password.php');\n";
        $script .= "\t\$log = new {$this->GetNamespace()}\Helpers\Log();\n";
        // code for class instanciation of the table class
        $script .= "\t\$bll = ";
        $script .= "new {$this->GetNamespace()}\\{$this->GetDatabaseName()}\\";
        $script .= "Bll\\{$this->dTable->GetNameUCFirst()}(\$log);\n";
    
        $script .= "\t// test setters\n";
        $script .= "\t// remember start timestap for later testing\n";
        $script .= "\t\$startTimeStamp = date(\"Y-m-d H:i:s\");\n";
        $script .= $this->makeScriptTestSetters('I');
        return $script;
    }

    /** ------------------ makeScriptLogView  --------------------------
    *
    * Make Unit Test script for Log View
    * @lastmodified 28/10/2014
    * @since 01/06/2012           
    * @author Jef Inghelbrecht - An Orm Apart        
    * @version 0.2
    * @return string  
    */
    public function makeScriptLogView($list = FALSE)
    {
        // 
        $script = "<!DOCTYPE html>\n";
        $script .= "<html lang=\"en\">\n";
        $script .= "\t<head>\n";
        $script .= "\t\t<meta charset=\"utf-8\" />\n";
        $script .= "\t\t<title>Test BLL/DAL</title>\n";
        $script .= "\t</head>\n";
        $script .= "\t<body>\n";
        $script .= $this->makeScriptLogToHtml(2);
        $script .= $this->makeScriptBdoToHtml(2);
        if ($list)
        {
            $script .= $this->makeScriptListToHtml(2);
        }
        $script .= "\t</body>\n";
        $script .= "</html>\n";
        return $script;
    }

    /********************** Php View Catalog section ***********************
    * View class
    * Generates Simple CGI views for PHP and / or Ajax enabled views
    * uses simple html views and multiple pages for each action of a one page ajax form
    * You can use phpDocumentor to generate documentation
    *
    * @lastmodified 20/5/2014
    * @lastmodified 16/3/2013
    * @since 01/06/2012           
    * @author Jef Inghelbrecht - An Orm Apart       
    * @version 0.2
    */

    /** ------------------ makeScriptBdoToHtml  --------------------------
    *
    * a simple display view for business data object
    * @since 20/05/2014           
    * @author Jef Innghelbrecht - An Orm Apart       
    * @version 0.2
    * @return string  
    */
    public function makeScriptBdoToHtml($tabNum = 1)
    {
        $tab = str_repeat("\t", $tabNum);
        $script = "$tab<fieldset>\n";
        $script .= "$tab\t<legend>{$this->dTable->GetNameUCFirst()}</legend>\n";
        foreach ($this->dTable->GetRows() as $row)
        {
            $script .= "$tab\t<div>\n";
            $script .= "$tab\t\t<label>{$row->GetDisplayText()}</label>\n";
            $script .= "$tab\t\t<span><?php echo \$bll->get{$row->GetColumnName()}();?></span>\n";
            $script .= "$tab\t</div>\n";
        }
        $script .= "$tab</fieldset>\n";
        return $script;
    }


    /** ------------------ makeScriptFeedbackToHtml  --------------------------
    *
    * a simple display view for feedback object
    * @since 20/05/2014           
    * @author Jef Innghelbrecht - An Orm Apart       
    * @version 0.2
    * @return string  
    */
    public function makeScriptFeedbackToHtml($tabNum)
    {
        $tab = str_repeat("\t", $tabNum);
        $script = "$tab<h3><?php echo \$key;?></h3>\n";
        $script .= "$tab<h4><?php echo \$feedback->getName();?></h4>\n";
        $script .= "$tab<div><label>Feedback</label><span><?php echo \$feedback->getText();?></span></div>\n";
        $script .= "$tab<div><label>Error code</label><span><?php echo \$feedback->getErrorCode();?></span></div>\n";
        $script .= "$tab<div><label>Error message</label><span><?php echo \$feedback->getErrorMessage();?></span></div>\n";
        $script .= "$tab<div><label>Error Code Driver</label><span><?php echo \$feedback->getErrorCodeDriver();?></span></div>\n";
        $script .= "$tab<div><label>Is error</label><span><?php echo \$feedback->getIsError();?></span></div>\n";
        $script .= "$tab<div><label>Start</label><span><?php echo \$feedback->getStartTime();?></span></div>\n";
        $script .= "$tab<div><label>End</label><span><?php echo \$feedback->getEndTime();?></span></div>\n";
        return $script;
    }

    /** ------------------ makeScriptLogToHtml  --------------------------
    *
    * a simple display view for Log object
    * @since 20/05/2014           
    * @author Jef Innghelbrecht - An Orm Apart       
    * @version 0.2
    * @return string  
    */
    public function makeScriptLogToHtml($tabNum)
    {
        $tab = str_repeat("\t", $tabNum);
        $script = "$tab<?php\n";
        $script .= "{$tab}if (count(\$log->getBook()) > 0)\n";
		$script .= "$tab{\n";
        $script .= "$tab\tforeach (\$log->getBook() as \$key => \$feedback)\n";
        $script .= "$tab\t{?>\n";

        $script .= $this->makeScriptFeedbackToHtml($tabNum + 1);

        $script .= "$tab\t<?php\n";
        $script .= "$tab\t}\n";
        $script .= "$tab}\n";
        $script .= "{$tab}else\n";
		$script .= "$tab{?>\n";
		$script .= "$tab\t<h1>No errors</h1>\n";

        $script .= "$tab<?php\n{$tab}}?>\n";
        return $script;
    }

    /** ------------------ makeScriptListToHtml  --------------------------
    *
    * a simple display view for list array
    * @since 20/05/2014           
    * @author Jef Innghelbrecht - An Orm Apart       
    * @version 0.2
    * @return string  
    */
    public function makeScriptListToHtml($tabNum = 1)
    {
        $tab = str_repeat("\t", $tabNum);
        $script = "$tab<?php\n";
        $script .= "{$tab}if (count(\$list) > 0)\n";
        $script .= "$tab{\n";
        $script .= "{$tab}?>\n";

        $script .= "$tab<table>\n";
        $script .= "$tab\t<caption>{$this->dTable->GetNameUCFirst()}</caption>\n";
        // make the header row
        $script .= "$tab\t<tr>\n";
        foreach ($this->dTable->GetRows() as $row)
        {
            if ($row->isSearchable())
            {
                $script .= "$tab\t\t<th>";
                $script .= $row->getDisplayText();
                $script .= "</th>\n";
            }
        }
        $script .= "$tab\t</tr>\n";
        // the data rows
        $script .= "$tab\t<?php\n";
        $script .= "$tab\tforeach (\$list as \$row)\n";
        $script .= "$tab\t{\n";
        $script .= "$tab\t?>\n";
        $script .= "$tab\t<tr>\n";

        foreach ($this->dTable->GetRows() as $row)
        {
            // only if column is searchable
            if ($row->isSearchable())
            {
                $script .= "$tab\t\t<td><?php echo \$row['{$row->GetColumnName()}'];?></td>\n";
             }
        }
        $script .= "$tab\t</tr>\n";
        $script .= "$tab\t<?php\n";
        $script .= "$tab\t}\n";
        $script .= "$tab\t?>\n";

        $script .= "$tab</table>\n";
        $script .= "$tab<?php\n";
        $script .= "$tab}\n";
		$script .= "{$tab}else\n";
		$script .= "$tab{?>\n";
        $script .= "$tab\t<h1>Lege lijst</h1>\n";
        $script .= "$tab<?php\n";
        $script .= "$tab}\n";
        $script .= "{$tab}?>\n";
        return $script;
    }
}
?>
