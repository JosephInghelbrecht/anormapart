        <footer>
            <br/>
            <br/>
            <p class="copy">concept & design - A N<span>orm</span> Apart 2012-2014</p>
            
            <div class="vcard">
                <h3>Contact</h3>
                <p class="fn org">Ten Clercken</p>
                <div class="adr">
                    <div class="street-address">Lange Dijkstraat</div>
                    <div class="postal-code">2000</div>
                    <div class="locality">Antwerpen</div>
                    <div class="country-name">België</div>
                </div>
            </div>
        </footer>
