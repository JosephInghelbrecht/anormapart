<?php
    /********************** Php DAL Catalog class ************************/
    /**
     * Catalog class
     * Generates DAL and BLL for PHP
     *
     * You can use phpDocumentor to generate documentation
     *
     * 
     * @lastmodified 6/10/2013 Table klasse toegevoegd
     * @since 01/06/2012           
     * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M        
     * @version 3.0
     */
    namespace AnOrmApart\Php;
    
    class Dal extends \AnOrmApart\Catalog
    {

        /** ------------------ addZipScriptDalClasses  --------------------------
        *
        * Create Dal classes for all tables and add to zip file
        * @lastmodified 23/04/2015
        * @since 01/06/2012           
        * @author Jef Inghelbrecht - An Orm Apart       
        * @version 0.2
        * @return string  
        */
        public function zipAddScriptDalClasses($zip)
        {
          $script = '';
          if ($this->CatalogExists())
          {
                // Make Dal class
                foreach ($this->catalog as $table)
                {
                    // onthoud met welke tabel we bezig zijn
                    $this->dTable = $table;
                    $fileName = "{$this->GetVendorToLower()}/{$this->GetDatabaseNameToLower()}/src/Dal/{$this->dTable->GetNameUCFirst()}.php";
                    $zip->addFromString($fileName, $this->makeScriptDalClass($fileName));
                }
                return true;
            }
            return false;
        }

        /** ------------------ makeScriptDalClasses  --------------------------
        *
        * Create Dal classes for all table
        * @lastmodified 23/01/2015
        * @since 01/06/2012           
        * @author Jef Inghelbrecht - An Orm Apart       
        * @version 0.2
        * @return string  
        */
        public function makeScriptDalClasses()
        {
          $script = '';
          if ($this->CatalogExists())
          {
                // Make Dal class
                foreach ($this->catalog as $table)
                {
                    // onthoud met welke tabel we bezig zijn
                    $this->dTable = $table;
                    $fileName = "{$this->GetVendorToLower()}/{$this->GetDatabaseNameToLower()}/src/Dal/{$this->dTable->GetNameUCFirst()}.php";
                    $script .= $this->makeScriptDalClass($fileName);
                }
             }
            return $script;
        }

        /** ------------------ makeScriptDalClass  --------------------------
        *
        * Create Dal classes for one table
        * @lastmodified 23/01/2015
        * @since 01/06/2012           
        * @author Jef Inghelbrecht - An Orm Apart       
        * @version 0.2
        * @return string  
        */
        public function makeScriptDalClass($fileName)
        {
            $script = "<?php\n";
            $script .= "/* modernways.be\n";
            $script .= " * created by an orm apart\n";
            $script .= " * Entreprise de modes et de manières modernes\n";
            $script .= " * Dal for {$this->GetDatabaseName()} app\n";
            $script .= " * Created on " . date('l jS \of F Y h:i:s A') . "\n";
            $script .= " * $fileName\n";
            $script .= "*/ \n";
            // first make the inherited classes\n";
            $script .= "// Write here your own code to change the\n";
            $script .= "// standard behaviour of the code generated\n";
            // php directive and namespace
            $script .= "namespace {$this->GetVendor()}\\{$this->GetDatabaseName()}\\Dal;\n";
            // class Dal
            $script .= "class {$this->dTable->GetNameUCFirst()}";
            // use always base class
            $script .= " extends \\{$this->GetVendor()}\\{$this->GetDatabaseName()}\\AnOrmApart\\Dal\\{$this->dTable->GetNameUCFirst()}";
            $script .= "\n";
            // begin class bll code block
            $script .= "{\n";
            $script .= "}\n";
            $script .= "\n";    
            return $script;
        }
        
         /** ------------------ zipAddScriptDalAnOrmApartClasses  --------------------------
         *
         * Make CRUD scripts for all tables in database for PHP and add them to zip
         * @lastmodified 30/04/2015
         * @since 30/04/2015          
         * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M        
         * @version 0.2
         * @return void 
         */
        public function zipAddScriptDalAnOrmApartClasses($zip)
        {
          if ($this->CatalogExists())
          {
                foreach ($this->catalog as $table)
                {
                    // onthoud met welke tabel we bezig zijn
                    $this->dTable = $table;
                    $fileName= "{$this->GetVendorToLower()}/{$this->GetDatabaseNameToLower()}/src/AnOrmApart/Dal/{$this->dTable->GetNameUCFirst()}.php";
                    $zip->addFromString($fileName, $this->makeScriptDalAnOrmApartClass($fileName));
                }
           }
           return true;
        }   

         /** ------------------ makeScriptDalAnOrmApartClasses  --------------------------
         *
         * Make CRUD scripts for all tables in database for PHP
         * @lastmodified 7/02/2015
         * @lastmodified 4/3/2013
         * @since 01/06/2012           
         * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M        
         * @version 0.2
         * @return string  
         */
        public function makeScriptDalAnOrmApartClasses()
        {
            if ($this->CatalogExists())
            {
                $script = '';
                foreach ($this->catalog as $table)
                {
                    // onthoud met welke tabel we bezig zijn
                    $this->dTable = $table;
                    $fileName = "{$this->GetVendorToLower()}/{$this->GetDatabaseNameToLower()}/src/AnOrmApart/Dal/{$this->dTable->GetNameUCFirst()}.php";
                    $script .= $this->makeScriptDalAnOrmApartClass($fileName);
                }
                $script .= "\n";
           }
           return $script;
        }            

        
        /** ------------------ makeScriptDalAnOrmApartClass  --------------------------
        *
        * Create Dal class for one table
        * @lastmodified 12/5/2014
        * @lastmodified 4/3/2013
        * @since 01/06/2012           
        * @author Jef Inghelbrecht - An Orm Apart        
        * @version 0.2
        * @return string  
        */
        public function makeScriptDalAnOrmApartClass($fileName)
        {
            $script = "<?php\n";
            $script .= "/* modernways.be\n";
            $script .= " * created by an orm apart\n";
            $script .= " * Entreprise de modes et de manières modernes\n";
            $script .= " * Dal for {$this->GetDatabaseName()} app\n";
            $script .= " * Created on " . date('l jS \of F Y h:i:s A') . "\n";
            $script .= " * FileName: $fileName\n";
            $script .= "*/ \n";
            // make the AnOrmApart classes
            $script .= "// Code generated by An Orm Apart\n";
            $script .= "// do not modify the contents of this namespace\n";
            // php directive and namespace
            $script .= "namespace {$this->GetVendor()}\\{$this->GetDatabaseName()}\\AnOrmApart\\Dal;\n";
            // class Dal
            $script .= "class {$this->dTable->GetNameUCFirst()}";
            // use base class if extend
           if ($this->dTable->IsExtend())
            {
                $script .= " extends \\{$this->GetVendor()}\Helpers\Dal\Base";
            }
            $script .= "\n";
            // begin class model code block
            $script .= "{\n";

            if (!$this->dTable->IsExtend())
            {
                // log field
                $script .= "\t// log field\n";
                $script .= "\tprotected \$log;\n";
            
                // data related fields
                $script .= "\tprotected \$rowCount;\n";
                $script .= "\tprotected \$connection;\n";
                $script .= "\tprotected \$bdo;\n";
            
                // log getter and setter
                $script .= "\t// log getter and setter\n";
                $script .= "\tpublic function setLog(\$value)\n";
                $script .= "\t{\n";
            
                $script .= "\t\t \$this->log = \$value;\n";
                $script .= "\t}\n";

                $script .= "\tpublic function getLog()\n";
                $script .= "\t{\n";
                $script .= "\t\treturn \$this->log;\n";
                $script .= "\t}\n";
                $script .= "\n";

                // database related getters
                $script .= "\t// database related getters\n";
                $script .= "\tpublic function getRowCount()\n";
                $script .= "\t{\n";
                $script .= "\t\treturn \$this->rowCount;\n";
                $script .= "\t}\n";
                $script .= "\n";

                // business object getter en setter
                $script .= "\tpublic function getBdo()\n";
                $script .= "\t{\n";
                $script .= "\t\treturn \$this->entity;\n";
                $script .= "\t}\n";
                $script .= "\n";
                $script .= "\tpublic function setBdo(\$value)\n";
                $script .= "\t{\n";
                $script .= "\t\t\$this->entity = \$value;\n";
                $script .= "\t}\n";
                $script .= "\n";

                // connection setter en getter
                $script .= "\tpublic function setConnection(\$value)\n";
                $script .= "\t{\n";
                $script .= "\t\t\$this->connection = \$value;\n";
                $script .= "\t}\n";
                $script .= "\n";
                $script .= "\tpublic function getConnection()\n";
                $script .= "\t{\n";
                $script .= "\t\treturn \$this->connection;\n";
                $script .= "\t}\n";
                $script .= "\n";

                // constructor
                $script .= "\tpublic function __construct(\$log)\n";
                $script .= "\t{\n";
                $script .= "\t\t\$this->log = \$log;\n";
                //$script .= "\t\t\$this->entity = new Bll();\n";
                //$script .= "\t\t\$this->connection = new \AnOrmApart\Dal\Connection();\n";
                $script .= "\t}\n";
            }
            // if table is Base do not make CRUD methods
            if ($this->dTable->GetNameToLower() != 'base')
            {
                $script .= $this->makeScriptCreateMethod();
                $script .= $this->makeScriptUpdateMethod();
                $script .= $this->makeScriptDeleteMethod();
                // eigenlijk mag die weg, het volstaat om bij Id searchable op true te zetten
                // dat geldt ook voor stored procedure
                $script .= $this->makeScriptReadOneMethod();
                $script .= $this->makeScriptReadAllMethod();
                $script .= $this->makeScriptReadByMethods();
                $script .= $this->makeScriptReadLikeMethods();
                $script .= $this->makeScriptReadLikeXMethods();
                //$script .= $this->GetCountByMethods();
            }
            // end class model block
            $script .= "}\n\n";
            return $script;
        }
    
    
        /** ------------------ makeScriptParametersPreparedStatementCreate  --------------------------
        *
        * Make paramater list for prepared statement PDO. The autoincrement primary column
        * is an OUT paramater and returns the Id of the newly inserted row.
        * @lastmodified 4/3/2013
        * @since 01/06/2012           
        * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M        
        * @version 0.2
        * @param array $rows
        * @param string $method
        * @return string  
        */
        private function makeScriptParametersPreparedStatement($method)
        {
            $script = '';
            foreach ($this->dTable->GetRows() as $row)
            {
                if( ($method=='INSERT' && $row->GetDefaultValueFunctionName() != 'UPDATE' && !$row->isSetByStoredProcedure()) || 
                    ($method=='UPDATE' && $row->GetDefaultValueFunctionName() != 'INSERT'  && !$row->isSetByStoredProcedure()) || 
                    ($row->IsPrimaryKey() && $method=='DELETE' || $method=='SELECTONE') )
    
                $script .=  ($row->IsPrimaryKey() && $method=='INSERT' ? '@' : ':') . 'p'. $row->GetColumnName() . ', ';
            }
            $script = rtrim($script, ', ');
            return $script;
        }
    
         /** ------------------ makeScriptBindParamList  --------------------------
        *
        * Make paramater list for prepared statement PDO. The autoincrement primary column
        * is an OUT paramater and returns the Id of the newly inserted row if crud method is insert.
        * If crud method is update, all are in IN paramaters
        * @lastmodified 4/3/2013
        * @since 01/06/2012           
        * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M        
        * @version 0.2
        * @param array $rows
        * @param string $method
        * @return string  
        */
        private function makeScriptBindParamList($method)
        {
            $script = "\t\t\t\t// we use here a getter method to obtain the value to be saved,\n"; 
            $script = "\t\t\t\t// so we cannot use bindParam that requires a variable by value\n";
            $script .= "\t\t\t\t// if you want to use a variable, use then bindParam\n";
            foreach ($this->dTable->GetRows() as $row)
            {
                // no cloumns where the value is assigned inside the stored procedure itself
                // dafault value is prefixed by SP-
                if (!$row->isSetByStoredProcedure())
                {
                    // if method is insert, no primarykey and no update only columns,
                    // default value = UPDATE()
                    // if method is update no insert only coloumns
                    // if method is DELETE, SELECTONE, UPDATE include primary key
                    if((!$row->IsPrimaryKey() && $method=='INSERT' && $row->GetDefaultValueFunctionName() != 'UPDATE') || 
                        ($row->IsPrimaryKey() && ($method=='DELETE' || $method=='SELECTONE')) || 
                        ($method=='UPDATE' && $row->GetDefaultValueFunctionName() != 'INSERT'))
                    {
                        $script .= $this->makeScriptBindParamOne($row);
                    }
                }
            }
            return $script;
        }
    
         /** ------------------ makeScriptBindParamOne  --------------------------
        *
        * Make one paramater for prepared statement PDO. The autoincrement primary column
        * is an OUT paramater and returns the Id of the newly inserted row if crud method is insert.
        * If crud method is update, all are in IN paramaters
        * @lastmodified 4/5/2013
        * @since 01/06/2012           
        * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M        
        * @version 0.2
        * @param array $row
        * @return string  
        */
        private function makeScriptBindParamOne($row)
        {
            $script = "\t\t\t\t\$preparedStatement->bindValue(':p";
            $script .=  $row->GetColumnName(). "', ";
            $script .= '$this->entity->get' .  $row->GetColumnName() . '(), \PDO::PARAM_';
            $script .=  ($row->getType()=='BOOL' ? 'BOOL' : ($row->getType()=='INT' ? 'INT' : 'STR')) . ");\n";
            return $script;
        }
    
        protected function makeSetFeedbackScript(
                $tabNum, 
                $text, 
                $errorMessage = 'none', 
                $errorCode = 'none', 
                $errorCodeDriver = 'none')
        {
            $tab = str_repeat("\t", $tabNum);
            $script = "$tab\$this->log->setText($text);\n";
            $script .= "$tab\$this->log->setErrorMessage($errorMessage);\n";
            $script .= "$tab\$this->log->setErrorCodeDriver($errorCodeDriver);\n";
            $script .= "$tab\$this->log->setErrorCode($errorCode);\n";
            $script .= "$tab\$this->log->setContext('{$this->dTable->GetNameUCFirst()}');\n";
            return $script;
        }

        protected function makeSetFeedbackFromSQLErrorInfoScript($tabNum) 
        {
            
            $tab = str_repeat("\t", $tabNum);
            $script = "$tab\$this->log->setErrorCode(\$sQLErrorInfo[0]);\n";
            $script .= "$tab\$this->log->setErrorCodeDriver(\$sQLErrorInfo[1]);\n";
            $script .= "$tab\$this->log->setErrorMessage(\$sQLErrorInfo[2]);\n";
            $script .= "$tab\$this->log->setContext('{$this->dTable->GetNameUCFirst()}');\n";
            return $script;
        }
    
        /** ------------------ toObject  --------------------------
        *
        * maps a row of a table to an object represenation
        * 
        * @lastmodified 13/3/2013
        * @since 01/06/2012           
        * @author Jef Inghelbrecht - Entreprise de Modes et de Manieres Modernes - e3M        
        * @version 0.2
        * @param array $rows
        * @return string  
        */
        private function toObject()
        {
            $script = '';
            foreach ($this->dTable->GetRows() as $row)
            {
                $script .= "\t\t\t\t\t\$this->entity->set" . $row->GetColumnName() . 
                    "(\$array['" . $row->GetColumnName() . "']);\n";
            }
            return $script;
        }
    

        public function makeScriptCreateMethod()
        {
            $script = "\tpublic function create()\n";
            $script .= "\t{\n";
            $script .= "\t\t\$this->log->startTimeInKey('insert');\n";
            // we gaan ervan uit dat alles verkeerd aflooopt
            $script .= "\t\t\$result = false;\n";
            // best to check once more
            $script .= "\t\tif (!\$this->entity->isValid())\n";
            $script .= "\t\t{\n";
            $script .= $this->makeSetFeedbackScript(3, "\$this->log->validationInvalidInput(\\ModernWays\\Helpers\\LogLocale::INVALIDINPUT_INSERT)",
                "'see BLL feedback for details'",
                "'DAL INSERT {$this->dTable->GetNameUCFirst()}'",
                "'none'");
            $script .= "\t\t\t\$this->log->log();\n";
            $script .= "\t\t\treturn;\n";
            $script .= "\t\t}\n";
            $script .= "\t\tif (\$this->connection->isConnected())\n";
            $script .= "\t\t{\n";
            $script .= "\t\t\ttry\n";
            $script .= "\t\t\t{\n";
            $script .= "\t\t\t\t// Prepare stored procedure call\n";
            $script .= "\t\t\t\t\$preparedStatement = \$this->connection->getPdo()->\n";
            $script .= "\t\t\t\tprepare('CALL {$this->dTable->GetNameUCFirst()}Insert(";
            $script .= $this->makeScriptParametersPreparedStatement('INSERT');
            $script .= ")');\n";
            $script .= $this->makeScriptBindParamList('INSERT');
          
            $script .= "\t\t\t\t// Returns true on success or false on failure\n";
            $script .= "\t\t\t\t\$result = \$preparedStatement->execute();\n";
            $script .= "\t\t\t\t\$this->rowCount = \$preparedStatement->rowCount();\n";
            $script .= "\t\t\t\tif (\$result)\n";
            $script .= "\t\t\t\t{\n";
            $script .= "\t\t\t\t\t// fetch the output\n";
		    $script .= "\t\t\t\t\t\$this->entity->setId(\$this->connection->getPdo()->query('select @pId')->fetchColumn());\n";
            $script .= "\t\t\t\t\t\$this->log->setText(\$this->log->created(true, \$this->entity->getId()));\n";
            $script .= "\t\t\t\t\t\$result = true;\n";
            $script .= "\t\t\t\t}\n";

            // error stored procedure

            $script .= "\t\t\t\telse\n";
            $script .= "\t\t\t\t{\n";
            $script .= "\t\t\t\t\t\$text = \$this->log->sp('{$this->dTable->GetNameUCFirst()}Insert');\n";
            $script .= "\t\t\t\t\t\$text .= ' ';\n";
            $script .= "\t\t\t\t\t\$text .= \$this->log->created(false, 0);\n";
			$script .= "\t\t\t\t\t\$this->log->setText(\$text);\n";
            $script .= "\t\t\t\t}\n";
            $script .= "\t\t\t\t\$sQLErrorInfo = \$preparedStatement->errorInfo();\n";
            $script .= $this->makeSetFeedbackFromSQLErrorInfoScript(4);            

            // catch block
            $script .= "\t\t\t}\n";
            $script .= "\t\t\tcatch (\\PDOException \$e)\n";
            $script .= "\t\t\t{\n";
            $script .= $this->makeSetFeedbackScript(4, 
                "\$this->log->created(false, 0)",
                "\$e->getMessage()",
                "'DAL {$this->dTable->GetNameUCFirst()}Create'",
                "\$e->getCode()");
            $script .= "\t\t\t}\n";

            // feedback no connection
            $script .= "\t\t}\n";
            $script .= "\t\telse\n";
            $script .= "\t\t{\n";
            $script .= $this->makeSetFeedbackScript(3, "\$this->log->created(false, 0)",
                "\$this->log->connection(\$this->connection->getHostName(), \$this->connection->getDatabaseName(), \ModernWays\Helpers\LogLocale::CONNECTION_FAILED)",
                "'DAL INSERT {$this->dTable->GetNameUCFirst()}'",
                "'none'");
            $script .= "\t\t}\n";

            // log block and return
            $script .= "\t\t\$this->log->log();\n";
            $script .= "\t\treturn \$result;\n";
            $script .= "\t}\n\n";
            return $script;
        }

        public function makeScriptUpdateMethod()
        {
            $script = "\tpublic function update()\n";
            $script .= "\t{\n";
            $script .= "\t\t\$this->log->startTimeInKey('update');\n";
            // we gaan ervan uit dat alles verkeerd aflooopt
            $script .= "\t\t\$result = false;\n";
            // best to check once more
            $script .= "\t\tif (!\$this->entity->isValid())\n";
            $script .= "\t\t{\n";
            $script .= $this->makeSetFeedbackScript(3, "\$this->log->validationInvalidInput(\\ModernWays\\Helpers\\LogLocale::INVALIDINPUT_UPDATE)",
                "'see BLL feedback for details'",
                "'DAL UPDATE {$this->dTable->GetNameUCFirst()}'",
                "'none'");
            $script .= "\t\t\t\$this->log->log();\n";
            $script .= "\t\t\treturn;\n";
            $script .= "\t\t}\n";
            $script .= "\t\tif (\$this->connection->isConnected())\n";
            $script .= "\t\t{\n";
            $script .= "\t\t\ttry\n";
            $script .= "\t\t\t{\n";

            $script .= "\t\t\t\t// Prepare stored procedure call\n";
            $script .= "\t\t\t\t\$preparedStatement = \$this->connection->getPdo()->\n";
            $script .= "\t\t\t\tprepare('CALL {$this->dTable->GetNameUCFirst()}Update(";
            $script .= $this->makeScriptParametersPreparedStatement('UPDATE');
            $script .= ")');\n";
            $script .= "\t\t\t\t// no getter method, bindParam requires a reference variable\n";
            $script .= "\t\t\t\t// if you want to use a method, use then binndValue\n";
            $script .= $this->makeScriptBindParamList('UPDATE');
            
            $script .= "\t\t\t\t// Returns true on success or false on failure\n";
            $script .= "\t\t\t\t\$result = \$preparedStatement->execute();\n";
            $script .= "\t\t\t\t\$this->rowCount = \$preparedStatement->rowCount();\n";
            $script .= "\t\t\t\tif (\$result)\n";
            $script .= "\t\t\t\t{\n";
            $script .= "\t\t\t\t\tif (\$this->rowCount > 0)\n";
            $script .= "\t\t\t\t\t{\n";
            $script .= "\t\t\t\t\t\t// De rij met de opgegeven Id is geüpdated.\n";
            $script .= "\t\t\t\t\t\t// Dat leiden we af uit het feit dat er geen\n";
            $script .= "\t\t\t\t\t\t// foutmelding werd geretourneerd maar dat\n";
            $script .= "\t\t\t\t\t\t// het aantal geaffecteerde rijen groter dan 0 is.\n";
            $script .= "\t\t\t\t\t\t\$this->log->setText(\$this->log->updated(true, \$this->entity->getId()));\n";
            $script .= "\t\t\t\t\t\t\$result = true;\n";
            $script .= "\t\t\t\t\t}\n";
            $script .= "\t\t\t\t\telse\n";
            $script .= "\t\t\t\t\t{\n";
            $script .= "\t\t\t\t\t\t// De rij met de opgegeven Id bestaat niet.\n";
            $script .= "\t\t\t\t\t\t// Dat leiden we af uit het feit dat er geen\n";
            $script .= "\t\t\t\t\t\t// foutmelding werd geretourneerd maar dat\n";
            $script .= "\t\t\t\t\t\t// het aantal geaffecteerde rijen gelijk is aan 0.\n";
            $script .= "\t\t\t\t\t\t\$text = \$this->log->\$this->log->updated(false, \$this->entity->getId());\n";
            $script .= "\t\t\t\t\t\t\$text .= ' ';\n";
            $script .= "\t\t\t\t\t\t\$text .= \$this->log->updated(false, \$this->entity->getId());\n";
            $script .= "\t\t\t\t\t\t\$result = false;\n";
            $script .= "\t\t\t\t\t}\n";
            $script .= "\t\t\t\t}\n";
            $script .= "\t\t\t\telse\n";

            // feedback sql
            $script .= "\t\t\t\t{\n";
            $script .= "\t\t\t\t\t\$text = \$this->log->sp('{$this->dTable->GetNameUCFirst()}Update');\n";
            $script .= "\t\t\t\t\t\$text .= ' ';\n";
            $script .= "\t\t\t\t\t\$text .= \$this->log->updated(false, \$this->entity->getId());\n";
			$script .= "\t\t\t\t\t\$this->log->setText(\$text);\n";
            $script .= "\t\t\t\t}\n";
            $script .= "\t\t\t\t\$sQLErrorInfo = \$preparedStatement->errorInfo();\n";
            $script .= $this->makeSetFeedbackFromSQLErrorInfoScript(4);            
            
            // catch block
            $script .= "\t\t\t}\n";
            $script .= "\t\t\tcatch (\\PDOException \$e)\n";
            $script .= "\t\t\t{\n";
            $script .= $this->makeSetFeedbackScript(4, 
                "\$this->log->updated(false, \$this->entity->getId())",
                "\$e->getMessage()",
                "'DAL {$this->dTable->GetNameUCFirst()}Update'",
                "\$e->getCode()");
            $script .= "\t\t\t}\n";

            // feedback no connection
            $script .= "\t\t}\n";
            $script .= "\t\telse\n";
            $script .= "\t\t{\n";
            $script .= $this->makeSetFeedbackScript(3, "\$this->log->updated(false, \$this->entity->getId())",
                "\$this->log->connection(\$this->connection->getHostName(), \$this->connection->getDatabaseName(), \ModernWays\Helpers\LogLocale::CONNECTION_FAILED)",
                "'DAL UPDATE {$this->dTable->GetNameUCFirst()}'",
                "'none'");
            $script .= "\t\t}\n";

            // log block and return
            $script .= "\t\t\$this->log->log();\n";
            $script .= "\t\treturn \$result;\n";
            $script .= "\t}\n\n";
            return $script;
        }

        public function makeScriptDeleteMethod()
        {
            $script = "\tpublic function delete()\n";
            $script .= "\t{\n";
            $script .= "\t\t\$this->log->startTimeInKey('delete');\n";
            $script .= "\t\t\$result = false;\n";
            $script .= "\t\tif (\$this->connection->isConnected())\n";
            $script .= "\t\t{\n";
            $script .= "\t\t\ttry\n";
            $script .= "\t\t\t{\n";

            $script .= "\t\t\t\t// Prepare stored procedure call\n";
            $script .= "\t\t\t\t\$preparedStatement = \$this->connection->getPdo()->\n";
            $script .= "\t\t\t\tprepare('CALL {$this->dTable->GetNameUCFirst()}Delete(";
            $script .= $this->makeScriptParametersPreparedStatement('DELETE');
            $script .= ")');\n";
            $script .= "\t\t\t\t// no getter method, bindParam requires a reference variable\n";
            $script .= "\t\t\t\t// if you want to use a method, use then bindValue\n";
            $script .= $this->makeScriptBindParamList('DELETE');

            $script .= "\t\t\t\t// Returns true on success or false on failure\n";
            $script .= "\t\t\t\t\$result = \$preparedStatement->execute();\n";
            $script .= "\t\t\t\t\$this->rowCount = \$preparedStatement->rowCount();\n";
            $script .= "\t\t\t\tif (\$result)\n";
            $script .= "\t\t\t\t{\n";
            $script .= "\t\t\t\t\tif (\$this->rowCount > 0)\n";
            $script .= "\t\t\t\t\t{\n";
            $script .= "\t\t\t\t\t\t// De rij met de opgegeven Id is gedeleted.\n";
            $script .= "\t\t\t\t\t\t// Dat leiden we af uit het feit dat er geen\n";
            $script .= "\t\t\t\t\t\t// foutmelding werd geretourneerd maar dat\n";
            $script .= "\t\t\t\t\t\t// het aantal geaffecteerde rijen groter dan 0 is.\n";
            $script .= "\t\t\t\t\t\t\$this->log->setText(\$this->log->deleted(true, \$this->entity->getId()));\n";
            $script .= "\t\t\t\t\t\t\$result = true;\n";
            $script .= "\t\t\t\t\t}\n";
            $script .= "\t\t\t\t\telse\n";
            $script .= "\t\t\t\t\t{\n";
            $script .= "\t\t\t\t\t\t// De rij met de opgegeven Id bestaat niet.\n";
            $script .= "\t\t\t\t\t\t// Dat leiden we af uit het feit dat er geen\n";
            $script .= "\t\t\t\t\t\t// foutmelding werd geretourneerd maar dat\n";
            $script .= "\t\t\t\t\t\t// het aantal geaffecteerde rijen gelijk is aan 0.\n";
            $script .= "\t\t\t\t\t\t\$this->log->setText(\$this->log->deleted(false, \$this->entity->getId()));\n";
            $script .= "\t\t\t\t\t\t\$result = false;\n";
            $script .= "\t\t\t\t\t}\n";
            $script .= "\t\t\t\t}\n";
            $script .= "\t\t\t\telse\n";

            // feedback sql
            $script .= "\t\t\t\t{\n";
            $script .= "\t\t\t\t\t\$text = \$this->log->sp('{$this->dTable->GetNameUCFirst()}Delete');\n";
            $script .= "\t\t\t\t\t\$text .= ' ';\n";
            $script .= "\t\t\t\t\t\$text .= \$this->log->deleted(false, \$this->entity->getId());\n";
			$script .= "\t\t\t\t\t\$this->log->setText(\$text);\n";
            $script .= "\t\t\t\t}\n";
            $script .= "\t\t\t\t\$sQLErrorInfo = \$preparedStatement->errorInfo();\n";
            $script .= $this->makeSetFeedbackFromSQLErrorInfoScript(4);            

            // catch block
            $script .= "\t\t\t}\n";
            $script .= "\t\t\tcatch (\\PDOException \$e)\n";
            $script .= "\t\t\t{\n";
            $script .= $this->makeSetFeedbackScript(4, 
                "'Rij is niet gedeleted.'",
                "\$e->getMessage()",
                "'DAL {$this->dTable->GetNameUCFirst()}Delete'",
                "\$e->getCode()");
            $script .= "\t\t\t}\n";

            $script .= "\t\t}\n";
            // feedback no connection
            $script .= "\t\telse\n";
            $script .= "\t\t{\n";
            $script .= $this->makeSetFeedbackScript(3, "\$this->log->deleted(false, \$this->entity->getId())",
                "\$this->log->connection(\$this->connection->getHostName(), \$this->connection->getDatabaseName(), \ModernWays\Helpers\LogLocale::CONNECTION_FAILED)",
                "'DAL DELETE {$this->dTable->GetNameUCFirst()}'",
                "'none'");
            $script .= "\t\t}\n";

            // log block and return
            $script .= "\t\t\$this->log->log();\n";
            $script .= "\t\treturn \$result;\n";
            $script .= "\t}\n\n";
            return $script;
        }

        public function makeScriptReadOneMethod()
        {
            $script = "\tpublic function readOne()\n";
            $script .= "\t{\n";
            $script .= "\t\t\$this->log->setContext('{$this->dTable->GetNameUCFirst()}');\n";
            $script .= "\t\t\$result = false;\n";
            $script .= "\t\tif (\$this->connection->isConnected())\n";
            $script .= "\t\t{\n";
            $script .= "\t\t\ttry\n";
            $script .= "\t\t\t{\n";
            
            $script .= "\t\t\t\t// Prepare stored procedure call\n";
            $script .= "\t\t\t\t\$preparedStatement = \$this->connection->getPdo()->\n";
            $script .= "\t\t\t\tprepare('CALL {$this->dTable->GetNameUCFirst()}SelectOne(:pId)');\n";

            $script .= $this->makeScriptBindParamList('SELECTONE');

            $script .= "\t\t\t\t\$preparedStatement->execute();\n";
            $script .= "\t\t\t\t// fetch the output\n";
            $script .= "\t\t\t\t\$array = \$preparedStatement->fetch(\PDO::FETCH_ASSOC);\n";
            $script .= "\t\t\t\tif (\$array)\n";
            $script .= "\t\t\t\t{\n";
            $script .= $this->toObject();
            $script .= "\t\t\t\t\t\$this->log->startTimeInKey('read one');\n";
            $script .= "\t\t\t\t\t\$this->log->setText(\$this->log->read(true, \$this->entity->getId()));\n";
            $script .= "\t\t\t\t\t//Since column in the where must be primary key\n";
            $script .= "\t\t\t\t\t// rowCount() should return 1. Can be used as validation.\n";
            $script .= "\t\t\t\t\t\$this->rowCount = \$preparedStatement->rowCount();\n";
            $script .= "\t\t\t\t\t\$result = (\$preparedStatement->rowCount() == 1);\n";
            $script .= "\t\t\t\t}\n";

            // sql feedback
            $script .= "\t\t\t\telse\n";
            $script .= "\t\t\t\t{\n";

            $script .= "\t\t\t\t\t\$text = \$this->log->sp('{$this->dTable->GetNameUCFirst()}SelectOne');\n";
            $script .= "\t\t\t\t\t\$text .= ' ';\n";
            $script .= "\t\t\t\t\t\$text .= \$this->log->read(false, \$this->entity->getId());\n";
            $script .= "\t\t\t\t\t\$this->log->startTimeInKey('read one');\n";
			$script .= "\t\t\t\t\t\$this->log->setText(\$text);\n";

            $script .= "\t\t\t\t}\n";
            $script .= "\t\t\t\t\$sQLErrorInfo = \$preparedStatement->errorInfo();\n";
            $script .= $this->makeSetFeedbackFromSQLErrorInfoScript(4);            
            $script .= "\t\t\t}\n";

            // connection error feedback
            $script .= "\t\t\tcatch (\\PDOException \$e)\n";
            $script .= "\t\t\t{\n";
            $script .= "\t\t\t\t\t\$this->log->startTimeInKey('read one');\n";
            $script .= $this->makeSetFeedbackScript(4,
                "\$this->log->read(false, \$this->entity->getId())",
                "\$e->getMessage()",
                "'DAL {$this->dTable->GetNameUCFirst()}ReadOne'",
                "\$e->getCode()");
            $script .= "\t\t\t\t\$this->rowCount = -1;\n";
            $script .= "\t\t\t}\n";
            $script .= "\t\t}\n";

            // feedback no connection
            $script .= "\t\telse\n";
            $script .= "\t\t{\n";
            $script .= "\t\t\t\t\t\$this->log->startTimeInKey('read one');\n";
            $script .= $this->makeSetFeedbackScript(3, "\$this->log->read(false, \$this->entity->getId())",
                "\$this->log->connection(\$this->connection->getHostName(), \$this->connection->getDatabaseName(), \ModernWays\Helpers\LogLocale::CONNECTION_FAILED)",
                "'DAL DELETE {$this->dTable->GetNameUCFirst()}'",
                "'none'");
            $script .= "\t\t}\n";

            // log block and return
            $script .= "\t\t\$this->log->log();\n";
            $script .= "\t\treturn \$result;\n";
            $script .= "\t}\n\n";
            return $script;
        }

        public function makeScriptReadAllMethod()
        {
            $script = "\tpublic function readAll()\n";
            $script .= "\t{\n";
            $script .= "\t\t\$this->log->startTimeInKey('read all');\n";
            $script .= "\t\t\$result = false;\n";
            $script .= "\t\tif (\$this->connection->isConnected())\n";
            $script .= "\t\t{\n";
            $script .= "\t\t\ttry\n";
            $script .= "\t\t\t{\n";

            $script .= "\t\t\t\t// Prepare stored procedure call\n";
            $script .= "\t\t\t\t\$preparedStatement = \$this->connection->getPdo()->";
            $script .= "prepare('CALL {$this->dTable->GetNameUCFirst()}SelectAll()');\n";

            $script .= "\t\t\t\t\$preparedStatement->execute();\n";
            $script .= "\t\t\t\t\$this->rowCount = \$preparedStatement->rowCount();\n";
    
            $script .= "\t\t\t\tif (\$result = \$preparedStatement->fetchAll())\n";
            $script .= "\t\t\t\t{\n";
            $script .= "\t\t\t\t\t\$this->log->setText(\$this->log->readAll(true, \$this->rowCount));\n";
            $script .= "\t\t\t\t}\n";

            $script .= "\t\t\t\telse\n";
            // no rows found
            $script .= "\t\t\t\t{\n";
            $script .= "\t\t\t\t\t\$text = \$this->log->sp('{$this->dTable->GetNameUCFirst()}SelectAll');\n";
            $script .= "\t\t\t\t\t\$text .= ' ';\n";
            $script .= "\t\t\t\t\t\$text .= \$this->log->readAll(false, 0);\n";
			$script .= "\t\t\t\t\t\$this->log->setText(\$text);\n";
            $script .= "\t\t\t\t}\n";
            $script .= "\t\t\t\t\$sQLErrorInfo = \$preparedStatement->errorInfo();\n";
            $script .= $this->makeSetFeedbackFromSQLErrorInfoScript(4);
            $script .= "\t\t\t}\n";

            $script .= "\t\t\tcatch (\\PDOException \$e)\n";
            $script .= "\t\t\t{\n";
            $script .= $this->makeSetFeedbackScript(4, 
                "\$this->log->readAll(false, 0)",
                "\$e->getMessage()",
                "'DAL {$this->dTable->GetNameUCFirst()}ReadOne'",
                "\$e->getCode()");
            $script .= "\t\t\t\t\$this->rowCount = -1;\n";
            $script .= "\t\t\t}\n";
            $script .= "\t\t}\n";

            // feedback no connection
            $script .= "\t\telse\n";
            $script .= "\t\t{\n";
            $script .= $this->makeSetFeedbackScript(3, "\$this->log->readAll(false, 0)",
                "\$this->log->connection(\$this->connection->getHostName(), \$this->connection->getDatabaseName(), \ModernWays\Helpers\LogLocale::CONNECTION_FAILED)",
                "'DAL DELETE {$this->dTable->GetNameUCFirst()}'",
                "'none'");
            $script .= "\t\t}\n";

            // log block and return
            $script .= "\t\t\$this->log->log();\n";
            $script .= "\t\treturn \$result;\n";
            $script .= "\t}\n\n";
            return $script;
        }

        /********************** makeScriptReadByMethods ***************************************
        * Create DAL method ReadBy columnname for one table
        * @param $modus: gewone read by of read like
        * @modified: 22/5/2014
        * @return string  
        */
        private function makeScriptReadByMethods()
        {
            $script = '';
    
            foreach ($this->dTable->GetRows() as $row)
            {
                // create for each searchable column a ReadBy Stored Procedure
                // read only searchable columns and of course for the
                // primary key colomn and the foreign key column
                // but only for the read by methods
                if($row->IsSelectBy() || $row->IsPrimaryKey() || $row->isForeignKey())
                {
                    $script .= $this->makeScriptReadMethods('By', $row);
                }
            }
            return $script;
        }

        /********************** makeScriptReadLikeMethods ***************************************
        * Create DAL method Read Like columnname for one table
        * @param $modus: gewone read by of read like
        * @modified: 22/5/2014
        * @return string  
        */
        private function makeScriptReadLikeMethods()
        {
            $script = '';
    
            foreach ($this->dTable->GetRows() as $row)
            {
                // create for each searchable column a ReadBy Stored Procedure
                // read only searchable columns and of course for the
                // primary key colomn and the foreign key column
                // but only for the read by methods
                if($row->IsSelectLike())
                {
                    $script .= $this->makeScriptReadMethods('Like', $row);
                }
            }
            return $script;
        }

        /********************** makeScriptReadLikeMethods ***************************************
        * Create DAL method Read Like columnname for one table
        * @param $modus: gewone read by of read like
        * @modified: 22/5/2014
        * @return string  
        */
        private function makeScriptReadLikeXMethods()
        {
            $script = '';
    
            foreach ($this->dTable->GetRows() as $row)
            {
                // create for each searchable column a ReadBy Stored Procedure
                // read only searchable columns and of course for the
                // primary key colomn and the foreign key column
                // but only for the read by methods
                if($row->IsSelectLikeX())
                {
                    $script .= $this->makeScriptReadMethods('LikeX', $row);
                }
            }
            return $script;
        }
         /********************** makeScriptBindParamOne ***************************************
        * Create DAL method ReadBy columnname for one table
        * @param $modus: gewone read by of read like
        * @modified: 22/5/2014
        * @return string  
        */
        private function makeScriptReadMethods($modus = 'By', $row)
        {
            $script = '';
            $script .= "\tpublic function read{$modus}{$row->GetColumnName()}()\n";
            $script .= "\t{\n";
            $script .= "\t\t\$this->log->startTimeInKey('{$this->dTable->GetNameUCFirst()}Read{$modus}{$row->GetcolumnName()}');\n";
            $script .= "\t\t\$result = false;\n";
            $script .= "\t\tif (\$this->connection->isConnected())\n";
            $script .= "\t\t{\n";
            $script .= "\t\t\ttry\n";
            $script .= "\t\t\t{\n";

            $script .= "\t\t\t\t// Prepare stored procedure call\n";
            $script .= "\t\t\t\t\$preparedStatement = \$this->connection->getPdo()->\n";
            $script .= "\t\t\t\tprepare(\"CALL {$this->dTable->GetNameUCFirst()}Select{$modus}{$row->GetcolumnName()}(";
            $script .= ":p{$row->GetcolumnName()})\");\n";
            $script .= "\t\t\t\t// no getter method, bindParam requires a reference variable\n";
            $script .= "\t\t\t\t// if you want to use a method, use then bindValue\n";
            $script .= $this->makeScriptBindParamOne($row);
            $script .= "\t\t\t\t\$preparedStatement->execute();\n";

            $script .= "\t\t\t\t\$this->rowCount = \$preparedStatement->rowCount();\n";
            $script .= "\t\t\t\t// fetch the output\n";
            $script .= "\t\t\t\tif (\$result = \$preparedStatement->fetchAll())\n";
            $script .= "\t\t\t\t{\n";
            $script .= "\t\t\t\t\t\$text = \$this->log->readBy(true, \$this->entity->get{$row->GetColumnName()}(), \$preparedStatement->rowCount());\n";
            $script .= "\t\t\t\t\t\$this->log->setText(\$text);\n";                  
            // sla de eerste rij op in de eigenschappen van de dal dlasse
            $script .= "\t\t\t\t\t\$array = \$result[0];\n";
            foreach ($this->dTable->GetRows() as $row)
            {
                if($row->isList() || $row->IsPrimaryKey())
                {                       
                    $script .= "\t\t\t\t\t\$this->entity->set" . $row->GetColumnName() . 
                    "(\$array['" . $row->GetColumnName() . "']);\n";
                }
            }
    
            $script .= "\t\t\t\t}\n";

            // sql niet gelukt
            $script .= "\t\t\t\telse\n";
            $script .= "\t\t\t\t{\n";
            $script .= "\t\t\t\t\t\$text = \$this->log->readBy(false, \$this->entity->get{$row->GetColumnName()}(), 0);\n";
            $script .= "\t\t\t\t\t\$this->log->setText(\$text);\n";
            $script .= "\t\t\t\t}\n";
            $script .= "\t\t\t\t\$sQLErrorInfo = \$preparedStatement->errorInfo();\n";
            $script .= $this->makeSetFeedbackFromSQLErrorInfoScript(4);            
            $script .= "\t\t\t}\n";

            $script .= "\t\t\tcatch (\\PDOException \$e)\n";
            $script .= "\t\t\t{\n";
            $script .= $this->makeSetFeedbackScript(4, 
                "\$this->log->readBy(false, \$this->entity->get{$row->GetColumnName()})",
                "\$e->getMessage()",
                "'DAL {$this->dTable->GetNameUCFirst()}ReadOne'",
                "\$e->getCode()");
            $script .= "\t\t\t\t\$this->rowCount = -1;\n";
            $script .= "\t\t\t}\n";
            $script .= "\t\t}\n";

            // feedback no connection
            $script .= "\t\telse\n";
            $script .= "\t\t{\n";
            $script .= $this->makeSetFeedbackScript(3, "\$this->log->readBy(false, \$this->entity->get{$row->GetColumnName()}(), 0)",
                "\$this->log->connection(\$this->connection->getHostName(), \$this->connection->getDatabaseName(), \ModernWays\Helpers\LogLocale::CONNECTION_FAILED)",
                "'DAL DELETE {$this->dTable->GetNameUCFirst()}'",
                "'none'");
            $script .= "\t\t}\n";

            // log block and return
            $script .= "\t\t\$this->log->log();\n";
            $script .= "\t\treturn \$result;\n";
            $script .= "\t}\n\n";
            return $script;
        }
    }
?>

