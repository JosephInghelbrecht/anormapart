<?php
    /**
     * Document Object Model base class for Row
     * Created : 01/06/2012
     * Last edit :  03/06/2012 Jef
     *              08/06/2012
     *              05/02/2013 JI
     *             
     * @version 0.3
     */
    
     // ik zou hier een algemene dom klasse voor een cataloog table van maken
    namespace AnOrmApart;
    
    use DOMDocument;
    use exception;
    
    class Row
    {
        private $columnName;
        private $displayText;
        private $searchable;
        private $orderBy;
        private $defaultValue;
        private $unique;
        private $type;
        private $length;
        private $variable;
        private $international;
        private $required;
        private $referenceTable;
        private $referenceColumn;
        private $referenceDisplayColumn;
        private $list;
        private $htmlElement;
        private $htmlTypeAttribute;
        private $htmlNameAttribute;
        private $htmlPatternAttribute;
        private $htmlTitleAttribute;
        private $htmlPlaceholder;
    
        public function getColumnName()
        {
            return $this->columnName;
        }
    
        public function getColumnNameToLower()
        {
            return strtolower($this->columnName);
        }

        public function getColumnNameForeignKey()
        {
            // fields in classes are in CamelCase
            $fieldName = $this->getReferenceTable();
            $fieldName .= $this->getReferenceDisplayColumn();
            return $fieldName;
        }

        public function getColumnNameForeignKeyList()
        {
            // fields in classes are in CamelCase
            $fieldName = $this->columnName;
            $length = strlen($fieldName);
            return substr($fieldName, 0, $length - 2) . 'List';
        }

        public function getFieldName()
        {
            // fields in classes are in CamelCase
            return lcfirst($this->columnName);
        }

        public function getFieldNameForeignKeyList()
        {
            return lcfirst($this->getColumnNameForeignKeyList());
        }

        public function getFieldNameForeignKey()
        {
            return lcfirst($this->getColumnNameForeignKey());
        }


        public function setColumnName($value)
        {
            if(isset($value))
            {
                $this->columnName = preg_replace("/[^\w ]+/", '', $value);
            }
            else
            {
                throw new \AnOrmApart\AnOrmApartException('Column name column: ' . 
                    $value . ' does not exist.', 1);
            }
        }
    
        public function getDisplayText()
        {
            return $this->displayText;
        }
    
        public function setDisplayText($value)
        {
            if(isset($value))
            { 
                $this->displayText = preg_replace("/[^\w ]+/", '', $value);
            }
            else
            {
                throw new \AnOrmApart\AnOrmApartException('Display text column: ' . 
                    $value . ' is missing.', 1);
            }
        }
    
        public function getSearchable()
        {
            return $this->searchable;
        }
    
        public function setSearchable($value)
        {
            $value = strtoupper($value);
            if(isset($value))
            { 
                if ($value == 'NO' || $value == 'NA')
                {
                    $this->searchable = 'NO';
                }
                else 
                {
                     $this->searchable = $value;
                }
             }
            else
            {
                $this->searchable = FALSE;
            }
        }

        public function isSearchable()
        {
            if ($this->searchable == 'NO')
            {
                return false;
            }
            return true;
        }

        public function getOrderBy()
        {
            return $this->orderBy;
        }
    
        public function setOrderBy($value)
        {
            if(isset($value))
            { 
                $uvalue = strtoupper($value);
                if ($uvalue == 'NO' || $uvalue == 'NA' || $uvalue == 'YES')
                {
                    $this->orderBy = FALSE;
                }
                else 
                {
                     $this->orderBy = $value;
                }
            }
            else
            {
                $this->orderBy = FALSE;
            }
        }
    
        public function getDefaultValue()
        {
            return $this->defaultValue;
        }
    
        public function setDefaultValue($value)
        {
            if(isset($value))
            { 
                switch (strtoupper($value))
                {
                    case 'PK':
                    case 'PRIMARY KEY':
                    case 'PRIMARY KEY':
                    // voorlopig moet die er ook bij
                    // heel wat cursisten hebben hun tabel al ingevuld
                    // en vroeger heette die kolom Primary Key
                    case 'YES':
                       $this->defaultValue = 'PK';
                       break;
                    default :
                       $this->defaultValue = strtoupper($value);
                       break;
                }        
            }
            else
            {
                $this->defaultValue = FALSE;
            }
        }
    
        public function getUnique()
        {
            return $this->unique;
        }
    
        public function setUnique($value)
        {
            if(isset($value))
            { 
                $this->unique = strtoupper($value);
            }
            else
            {
                $this->unique = FALSE;
            }
        }
    
        public function getType()
        {
            return $this->type;
        }
    
        public function setType($value)
        {
            // if not defined it is char
            $this->type = 'CHAR';
            switch (strtoupper($value))
            {
                case 'STRING':
                case 'CHAR':
                case 'TEKST':
                case 'TEXT': //access
                case 'MEMO': //access
                case 'KARAKTER':
                   $this->type = 'CHAR';
                   break;
                case 'DATE':
                case 'DATUM': 
                   $this->type = 'DATE';
                   break;
                case 'DATETIME':
                case 'DATE/TIME': //access
                   $this->type = 'DATETIME';
                   break;
                case 'BOOL':
                case 'BOOLEAN':
                case 'BIT':
                case 'YES/NO': //access
                   $this->type = 'BOOL';
                   break;
                case 'FLOAT':
                case 'DOUBLE': //access 
                case 'SINGLE': //access
                   $this->type = 'FLOAT';
                   break;
                 case 'AUTONUMBER': //access
                case 'INTEGER': //access
                case 'INT': //access
                case 'BYTE': //access
                   $this->type = 'INT';
                   break;
                case 'SMALLINT':
                   $this->type = 'SMALLINT';
                   break;
                case 'TINYINT': //
                   $this->type = 'TINYINT';
                   break;
                case 'TIMESTAMP':
                   $this->type = 'TIMESTAMP';
                   break;
                case 'LONG INTEGER': //access
                case 'BIGINT': //access
                    $this->type = 'BIGINT';
                    break;
                case 'DECIMAL':
                    $this->type = 'DECIMAL';
                    break;

            }
            // echo $this->getType();
        }
    
        /**
         * Determine type of Sql column  
         * @return string   
         */
        public function getSqlDataType()
        {
            switch ($this->getType())
            {
                case 'STRING':
                case 'CHAR':
                case 'TEKST':
                case 'TEXT': //access
                case 'MEMO': //access
                case 'KARAKTER':
                    $result = ($this->GetInternational() ? 'N' : '');
                    $result .= ($this->GetVariable() ? 'VARCHAR ' : 'CHAR ');
                    $result .= ($this->getLength() ? '(' . $this->getLength() . ') ' : '');
                    return $result;
                case 'DATUM': 
                case 'DATE':
                    return 'DATE ';
                case 'DATETIME':
                case 'DATE/TIME': //access
                    return 'DATETIME ';
                case 'TIMESTAMP':
                    return 'TIMESTAMP ';
                case 'BOOL':
                case 'BOOLEAN':
                case 'BIT':
                case 'YES/NO': //access
                    return 'BIT ';
                case 'FLOAT':
                case 'DOUBLE': //access 
                case 'SINGLE': //access
                    return 'FLOAT ';
                case 'AUTONUMBER': //access
                case 'INTEGER': //access
                case 'INT': //access
                case 'BYTE': //access
                    return 'INT ';
                case 'LONG INTEGER': //access
                case 'BIGINT': //access
                    return 'BIGINT ';
                case 'DECIMAL' :
                     return 'DECIMAL(' . $this->getLength() . ') ';
            }
            return $this->getType() . ' ';
        }

        /**
         * Determine type of Sql column
         * @return string
         */
        public function getSqlDbType()
        {
            switch ($this->getType())
            {
                case 'STRING':
                case 'CHAR':
                case 'TEKST':
                case 'TEXT': //access
                case 'MEMO': //access
                case 'KARAKTER':
                    $result = ($this->GetInternational() ? 'N' : '');
                    $result .= ($this->GetVariable() ? 'VarChar' : 'Char');
                    return $result;
                case 'DATUM':
                case 'DATE':
                    return 'Date';
                case 'DATETIME':
                case 'DATE/TIME': //access
                    return 'DateTime';
                case 'TIMESTAMP':
                    return 'Timestamp ';
                case 'BOOL':
                case 'BOOLEAN':
                case 'BIT':
                case 'YES/NO': //access
                    return 'Bit';
                case 'FLOAT':
                case 'DOUBLE': //access
                case 'SINGLE': //access
                    return 'Float';
                case 'AUTONUMBER': //access
                case 'INTEGER': //access
                case 'INT': //access
                case 'BYTE': //access
                    return 'Int';
                case 'LONG INTEGER': //access
                case 'BIGINT': //access
                    return 'BigInt';
            }
            return $this->getType() . ' ';
        }

        /**
         * Determine type of Sql column
         * @return string
         */
        public function getCSharpDataType()
        {
            switch ($this->getType())
            {
                case 'STRING':
                case 'CHAR':
                case 'TEKST':
                case 'TEXT': //access
                case 'MEMO': //access
                case 'KARAKTER':
                    return 'String ';
                case 'DATUM':
                case 'DATE':
                    return 'DateTime ';
                case 'DATETIME':
                case 'DATE/TIME': //access
                    return 'DateTime ';
                case 'TIMESTAMP':
                    return 'DateTime ';
                case 'BOOL':
                case 'BOOLEAN':
                case 'BIT':
                case 'YES/NO': //access
                    return 'Boolean ';
                case 'FLOAT':
                    return 'Float ';
                case 'DOUBLE': //access
                    return 'Double ';
                case 'SINGLE': //access
                    return 'Float ';
                case 'LONG INTEGER': //access
                    return 'Int64 ';
                case 'AUTONUMBER': //access
                case 'INTEGER': //access
                case 'INT': //access
                case 'BYTE': //access
                    return 'Int32 ';
            }
            return $this->getType() . ' ';
        }

        /**
         * Determine type of Sql column
         * @return string
         */
        public function getAnormApartDataType()
        {
            switch ($this->getType())
            {
                case 'STRING':
                case 'CHAR':
                case 'TEKST':
                case 'TEXT': //access
                case 'MEMO': //access
                case 'KARAKTER':
                    return 'TYPE_STR ';
                case 'DATUM':
                case 'DATE':
                    return 'TYPE_STR ';
                case 'DATETIME':
                case 'DATE/TIME': //access
                    return 'TYPE_STR ';
                case 'TIMESTAMP':
                    return 'TYPE_STR ';
                case 'BOOL':
                case 'BOOLEAN':
                case 'BIT':
                case 'YES/NO': //access
                    return 'TYPE_INT ';
                case 'FLOAT':
                    return 'Float ';
                case 'DOUBLE': //access
                    return 'TYPE_STR ';
                case 'SINGLE': //access
                    return 'TYPE_STR ';
                case 'LONG INTEGER': //access
                    return 'Int64 ';
                case 'AUTONUMBER': //access
                case 'INTEGER': //access
                case 'INT': //access
                case 'BYTE': //access
                    return 'TYPE_INT ';
            }
            return $this->getType() . ' ';
        }
        /**
         * Determine type of Sql column
         * @return string
         */
        public function getCSharpDataTypeNullValue()
        {
            switch ($this->getType())
            {
                case 'STRING':
                case 'CHAR':
                case 'TEKST':
                case 'TEXT': //access
                case 'MEMO': //access
                case 'KARAKTER':
                    return '"" ';
                case 'DATUM':
                case 'DATE':
                case 'DATETIME':
                case 'DATE/TIME': //access
                case 'TIMESTAMP':
                    return 'DateTime.MinValue';
                case 'BOOL':
                case 'BOOLEAN':
                case 'BIT':
                case 'YES/NO': //access
                    return 'false';
                case 'FLOAT':
                    return '0f';
                case 'DOUBLE': //access
                    return '0d';
                case 'SINGLE': //access
                    return '0f ';
                case 'LONG INTEGER': //access
                    return '0';
                case 'AUTONUMBER': //access
                case 'INTEGER': //access
                case 'INT': //access
                case 'BYTE': //access
                    return '0';
            }
            return $this->getType() . ' ';
        }

        /**
         * Determine CSS class name for INPUT
         * @return string   
         */
        public function getCssClassName()
        {
            // only if input text
            if ($this->getHtmlElement() == 'input' && $this->getHtmlTypeAttribute() == 'text')
            {
                switch ($this->getType())
                {
                    case 'STRING':
                    case 'CHAR':
                    case 'TEKST':
                    case 'TEXT': //access
                    case 'MEMO': //access
                    case 'KARAKTER':
                        return 'text';
                    case 'DATUM': 
                    case 'DATE':
                        return 'date ';
                    case 'DATETIME':
                        return 'datetime ';
                    case 'TIMESTAMP':
                        return 'datetime ';
                    case 'BOOL':
                    case 'BOOLEAN':
                    case 'BIT':
                    case 'YES/NO': //access
                        return 'integer';
                    case 'FLOAT':
                    case 'DOUBLE': //access 
                    case 'SINGLE': //access
                    case 'DECIMAL':
                    return 'decimal';
                    case 'LONG INTEGER': //access
                    case 'AUTONUMBER': //access
                    case 'INTEGER': //access
                    case 'INT': //access
                    case 'BYTE': //access
                        return 'integer';
                    default :
                        return false;
                }
            }
            else
            {
                return FALSE;
            }
        }

        /**
         * Determine CSS class name for INPUT
         * @return string   
         */
        public function getFilterInputSanitizeConstant()
        {
            switch ($this->getType())
            {
                case 'STRING':
                case 'CHAR':
                case 'TEKST':
                case 'TEXT': //access
                case 'MEMO': //access
                case 'KARAKTER':
                    return 'FILTER_SANITIZE_STRING';
                case 'DATUM': 
                case 'DATE':
                    return 'FILTER_SANITIZE_STRING ';
                case 'DATETIME':
                    return 'FILTER_SANITIZE_STRING ';
                case 'TIMESTAMP':
                    return 'FILTER_SANITIZE_STRING ';
                case 'BOOL':
                case 'BOOLEAN':
                case 'BIT':
                case 'YES/NO': //access
                    return 'FILTER_SANITIZE_NUMBER_INT';
                case 'FLOAT':
                case 'DOUBLE': //access 
                case 'SINGLE': //access
                case 'DECIMAL':
                    // return 'FILTER_SANITIZE_NUMBER_FLOAT';
                    return 'FILTER_SANITIZE_STRING';
                case 'LONG INTEGER': //access
                case 'AUTONUMBER': //access
                case 'INTEGER': //access
                case 'INT': //access
                case 'BYTE': //access
                    return 'FILTER_SANITIZE_NUMBER_INT';
                default :
                    return false;
            }
        }   
        public function getLength()
        {
            return $this->length;
        }

        public function getCssInLineStyle()
        {
            if ($this->getType() == 'CHAR') {
                if ($this->length > 0) {
                    $length = $this->getLength() * 0.25;
                    // minimum length
                    $length += 20;
                    return "style=\"width: {$length}%;\" ";
                } else {
                    return "style=\"width: 6em;\" ";
                }

            } elseif ($this->getType() == 'INT') {
                return "style=\"width: 6em;\" ";
            } elseif ($this->getType() == 'FLOAT') {
                return "style=\"width: 8em;\" ";
            } else {
                return '';
            }
        }


        public function setLength($value)
        {
            if(is_numeric($value))
            {
                $this->length = $value;
            }
            elseif (strtoupper($value) == 'MANY' || strtoupper($value) == 'ONE') {
                $this->length = $value;
            }
            else
            {
                $this->length = $value;
            }
        }

        public function isOneToOne() {
            return ($this->length == 'ONE');
        }

        public function isOneToMany() {
            return ($this->length == 'MANY');
        }

        public function GetVariable()
        {
            return $this->variable;
        }
    
        public function setVariable($value)
        {
            if(isset($value)) 
            {
                $this->variable = (strtoupper($value) == 'YES' ? TRUE : FALSE);
            }
            else
            {
                throw new \AnOrmApart\AnOrmApartException('Variable column: ' . 
                    $value . ' is missing.', 1);
            }
        }
    
        public function getInternational()
        {
            return $this->international;
        }
    
        public function setInternational($value)
        {
            if(isset($value)) 
            {
                $this->international = (strtoupper($value) == 'YES' ? TRUE : FALSE);
            }
            else
            {
                throw new \AnOrmApart\AnOrmApartException('International column: ' . 
                    $value . ' is missing.', 1);
            }
        }
    
        public function isRequired()
        {
           return $this->required;
        }
    
        public function setRequired($value)
        {
            if(isset($value)) 
            {
                $this->required = (strtoupper($value) == 'YES' ? TRUE : FALSE);
            }
            else
            {
                throw new \AnOrmApart\AnOrmApartException('Required column: ' . 
                    $value . ' is missing.', 1);
            }
        }
    
        public function getReferenceTable()
        {
            return $this->referenceTable;
        }
    
        public function setReferenceTable($value)
        {
            if(isset($value)) 
            {
                $this->referenceTable = preg_replace("/[^\w ]+/", '', $value);
            }
            else
            {
                throw new \AnOrmApart\AnOrmApartException('Reference table column: ' . 
                    $value . ' is missing.', 1);
            }
        }
    
        public function getReferenceColumn()
        {
            return $this->referenceColumn;
        }
    
        public function setReferenceColumn($value)
        {
            if(isset($value)) 
            {
                $this->referenceColumn = preg_replace("/[^\w ]+/", '', $value);
            }
            else
            {
                throw new \AnOrmApart\AnOrmApartException('Reference column column: ' . 
                    $value . ' is missing.', 1);
            }
        }

        public function getReferenceDisplayColumn()
        {
            return $this->referenceDisplayColumn;
        }
    
        public function setReferenceDisplayColumn($value)
        {
            if(isset($value)) 
            {
                $this->referenceDisplayColumn = preg_replace("/[^\w ]+/", '', $value);
            }
            else
            {
                throw new \AnOrmApart\AnOrmApartException('Reference display column column: ' . 
                    $value . ' is missing.', 1);
            }
        }

        public function getReferenceTablePlural() {
            $lower = strtolower($this->referenceTable);
            $plural = $this->referenceTable . 's';
            switch ($lower) {
                case 'country' :
                    $plural = 'Countries';
                    break;
            }
            return $plural;
        }
    
        public function isList()
        {
            return $this->list;
        }
    
        public function setList($value)
        {
            if(strtoupper($value) == 'YES')
            {
                $this->list = true;
            }
            else
            {
                $this->list = false;
            }
        }
    
        public function getHtmlElement()
        {
            return $this->htmlElement;
        }
    
        public function setHtmlElement($value)
        {
            if(empty($value) || strtoupper($value) == 'NONE' || strtoupper($value) == 'NO'
                    || strtoupper($value) == 'NA')
            {
                $this->htmlElement = false;
            }
            else
            {
                $this->htmlElement = strtolower($value);
            }
        }
    
        public function getHtmlTypeAttribute()
        {
            return $this->htmlTypeAttribute;
        }
    
        public function SetHtmlTypeAttribute($value)
        {
            // if html element is textarea set Type attribute also on textarea
            if ($this->getHtmlElement() == 'textarea')
            {
                $this->htmlTypeAttribute = 'textarea';
            }
            elseif (empty($value) || strtoupper($value) == 'NA' || strtoupper($value) == 'NO')
            {
                $this->htmlTypeAttribute = false;
            }
            else
            {
                $this->htmlTypeAttribute = strtolower($value);
            }
        }
    
        public function getHtmlIdAttribute()
        {
            // id attribute values in html are in lowercase
            return strtolower($this->columnName);
        }
    
        public function getHtmlNameAttribute()
        {
            if ($this->htmlNameAttribute)
            {
                return $this->htmlNameAttribute;
            }
    
        }
    
        public function setHtmlNameAttribute($value)
        {
            if(empty($value) || strtoupper($value) == 'NA' || strtoupper($value) == 'NO')
            {
                $this->htmlNameAttribute = false;
            }
            else
            {
                $this->htmlNameAttribute = strtolower($value);
            }
        }
    
        public function getHtmlPatternAttribute()
        {
            return $this->htmlPatternAttribute;
        }
    
        public function SetHtmlPatternAttribute($value)
        {
            if(empty($value) || strtoupper($value) == 'NA' || strtoupper($value) == 'NO')
            {
                $this->htmlPatternAttribute = false;
            }
            else
            {
                $this->htmlPatternAttribute = strtolower($value);
            }
        }
    
        public function getHtmlTitleAttribute()
        {
            return $this->htmlTitleAttribute;
        }
    
        public function setHtmlTitleAttribute($value)
        {
            if(empty($value) || strtoupper($value) == 'NA' || strtoupper($value) == 'NO')
            {
                $this->htmlTitleAttribute = false;
            }
            else
            {
                $this->htmlTitleAttribute = strtolower($value);
            }
        }
    
        public function getHtmlPlaceholder()
        {
            return $this->htmlPlaceholder;
        }
    
        public function setHtmlPlaceholder($value)
        {
            if(empty($value) || strtoupper($value) == 'NA' || strtoupper($value) == 'NO')
            {
                $this->htmlPlaceholder = false;
            }
            else
            {
                $this->htmlPlaceholder = strtolower($value);
            }
        }
    
        public function getMessage()
        {
            return $this->message;
        }   
    
    
        public function toString()
        {
            return 'columnName: ' . $this->GetColumnName() . '<br />' .
            'displayText: ' . $this->GetDisplayText() . '<br />' .
            'searchable: ' . $this->GetSearchable() . '<br />' .
            'displayOrder: ' . $this->GetDisplayOrder() . '<br />' .
            'primaryKey: ' . $this->GetPrimaryKey() . '<br />' .
            'unique: ' . $this->GetUnique() . '<br />' .
            'type: ' . $this->getType() . '<br />' .
            'length: ' . $this->getLength() . '<br />' .
            'variable: ' . $this->GetVariable() . '<br />' .
            'international: ' . $this->GetInternational() . '<br />' .
            'required: ' . $this->isRequired() . '<br />' .
            'referenceTablev: ' . $this->getReferenceTable() . '<br />' .
            'referenceColumn: ' . $this->GetReferenceColumn() . '<br />' .
            'referencedisplayColumn: ' . $this->GetReferencedisplayColumn() . '<br />' .
            'HtmlPatternAttribute: ' . $this->GetHtmlPatternAttribute() . '<br />' .
            'message: ' . $this->GetMessage() . '<br />' .
            'FieldsetName: ' . $this->GetFieldsetName() . '<br />' .
            'placeholder: ' . $this->GetPlaceholder() . '<br />';
        }
    
        public function isForeignKey()
        {
            // nog te testen als de vreemde tabel bestaat
            // check if there is no referencetable defined for this row
            if (strtoupper($this->getReferenceTable()) == 'NA')
            {
                // there is no referencetable defined for this row
                return false;
            }
            // if there is a referencetable defined for this row, check if there is 
            // a referencecolumn defined for this row
            else if (strtoupper($this->GetReferenceColumn()) !== 'NA')
            {
                // there is a referencecolumn defined for this row
                return true;
            }
            // there is no referencecolumn defined for this row
            return false;
        }
    
        public function isPrimaryKey()
        {
            if ($this->GetDefaultValue() == 'PK')
            {
                return true;
            }
            return false;
        }
    
        public function isUnique()
        {
            if ($this->GetUnique() == 'YES')
            {
                return true;
            }
            return false;
        }
    
        /**
         * Check if column is password boolean  
         * @return  true/false 
         */
        public function isPassword()
        {
            if ($this->getHtmlTypeAttribute() == 'password')
            {
                return true;
            }
            return false;
        }

        /**
         * Check if row is boolean  
         * @return  true/false 
         */
        public function isBool()
        {
            return ($this->getType() == 'BOOL' ? TRUE : false);
        }
    
        /**
         * Check if row is parameter for
         * insert stored procedure  
         * @return  true/false 
         */
        public function isSetByStoredProcedure()
        {
           // get stored procedure prefix sp-
           if (preg_match('/^[a-zA-Z0-9_\-]+(?=\-)/', $this->GetDefaultValue(), $match))
           {
               if (strtoupper($match[0]) == 'SP')
               {
                  return true;
               }
           }
           return false;
        }
        
        public function getDefaultValueCrudMethod()
        {
            if ($this->isSetByStoredProcedure())
            {
                /*  RegEx are greedy. That means RegEx match as much as they can. This means
                    the expression /(?<=\-).+(?=\()/ stops with the last (. To make it stop with the
                    first ( make the expression ungreedey:  */              
                if (preg_match('/(?<=\-)(.+?)(?=\()/', $this->GetDefaultValue(), $match))
                {
                    return strtoupper($match[0]);
                }
            }
            return 'NONE';
        }

        public function getDefaultValueFunctionName()
        {
            // get the function name, omit prefix sp-
            if (preg_match('/^[a-zA-Z0-9_\-]+(?=\()/', $this->GetDefaultValue(), $match))
            {
                return strtoupper($match[0]);
            }
            return false;
        }

        public function getDefaultValueFunctionParameterName()
        {
            // get the parameter (can also be a function)
            if (preg_match('/(?<=\().+(?=\))/', $this->GetDefaultValue(), $match))
            {
                return strtoupper($match[0]);
            }
            return 'error: no parameter specified in Default';
        }

        public function isCountable()
        {
            return ($this->GetSearchable() == 'COUNT');
        }

        // selectby = select where =
        // selectlike select where like A%
        // selectlikex select where like %A%
        // selectx, alle drie hierboven
        public function isSelectBy()
        {
            return ($this->GetSearchable() == 'SELECTBY' || $this->GetSearchable() == 'SELECTX');
        }

        public function isSelectLike()
        {
            return ($this->GetSearchable() == 'SELECTLIKE' || $this->GetSearchable() == 'SELECTX');
        }

        public function isSelectLikeX()
        {
            return ($this->GetSearchable() == 'SELECTLIKEX' || $this->GetSearchable() == 'SELECTX');
        }
    }
?>
