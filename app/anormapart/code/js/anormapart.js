var currentCatalogFileName;
var currentAction;
var currentView;

function coordinatingController(e) {
    // event.preventDefault();
    // if click on input field inside tile, do nothing
    if (e.target.tagName == 'INPUT') {

        e.target.focus();
        e.target.select();
    }
    else {
        var parent = e.target.parentNode;

        // is er binnenin een anchor geklikt?
        if (parent.tagName == 'A') {
            var action = parent.id;
            switch (action) {
                case 'make-template':
                case 'new-catalog':
                case 'php-dal-base-class':
                case 'php-test-dal-base-class':
                case 'php-dal-anormapart-class':
                case 'php-dal-class':
                case 'php-test-dal-class':
                case 'php-connection-class':
                case 'php-test-connection-class':
                case 'php-provider-class':
                case 'php-bll-base-class':
                case 'php-test-bll-base-class':
                case 'php-bll-class':
                case 'php-bll-anormapart-class':
                case 'php-test-bll-class':
                case 'php-feedback-class':
                case 'php-test-feedback-class':
                case 'php-log-class':
                case 'php-test-log-class':
                case 'php-locale-log-class':
                case 'php-log-view':
                case 'php-session-class':
                case 'php-login-class':
                case 'php-login-test-class':
                case 'php-password-hash':
                case 'ajax-class':
                case 'php-appmodel-class':
                case 'php-model-base-class':
                case 'php-model-class':
                case 'php-model-anormapart-class':
                case 'php-controller-anormapart-class':
                case 'php-controller-class':
                case 'php-threepenny-controller-class':
                case 'php-threepenny-view-class':
                case 'php-view-anormapart-class':
                case 'php-template-fieldset':
                case 'php-template':

                case 'csharp-model-class':
                case 'csharp-dal-class':

                case 'my-sql-ddl':
                case 'my-sql-dml':
                case 'ms-sql-ddl':
                case 'ms-sql-dml':
                    depotDispatcher(action);
                    break;
                case 'close-editor':
                    closeEditor();
                    break;

            }
        }
    }

}

var depotDispatcher = function (message, clause) {
    // to send off or away with speed, as a messenger, telegram, body of troops, etc
    // method of effecting a speedy delivery of goods, money, etc. 
    // a conveyance or organization for the expeditious transmission of goods, money, etc.

    // prepare data
    // the message is per default also the action
    // set current action
    currentAction = message;
    var data = 'anormapart-action=';
    switch (message) {
        case 'make-template':
            currentAction = 'make-template';
            currentView = 'make-template';
            data = data + currentAction;
            // set the newly entered filename
            setCurrentFilename(document.getElementById("newtemplatename").value);
            data = data + '&f=';
            data = data + encodeURIComponent(currentCatalogFileName);
            data = data + '&data=';
            // the catalog data is in a textarea with id catalog
            data = data + encodeURIComponent(document.getElementById('catalog-editor').innerHTML);
            break;
        case 'new-catalog':
            currentAction = 'read-catalog';
            currentView = 'read-catalog';
            data = data + currentAction;
            // first read the template
            setCurrentFilename('template/template');
            data = data + '&f=';
            data = data + encodeURIComponent(currentCatalogFileName);
            // set the newly entered filename
            setCurrentFilename(document.getElementById("newfilename").value);
            break;
        case 'open-catalog':
            currentAction = 'read-catalog';
            currentView = 'read-catalog';
            data = data + currentAction;
            setCurrentFilename(clause);
            data = data + '&f=';
            data = data + encodeURIComponent(currentCatalogFileName);
            break;
        case 'write-catalog':
            currentAction = 'write-catalog';
            currentView = 'write-catalog';
            data = data + currentAction;
            data = data + '&f=';
            data = data + encodeURIComponent(currentCatalogFileName);
            data = data + '&data=';
            // the catalog data is in a textarea with id catalog
            data = data + encodeURIComponent(document.getElementById('catalog-editor').innerHTML);
            break;
        case 'login':
            break;
        default:
            currentAction = message;
            currentView = 'read-code';
            data = data + currentAction;
            data = data + '&f=';
            data = data + encodeURIComponent(currentCatalogFileName);
            data = data + '&data=';
            // the catalog data is in a textarea with id catalog
            data = data + encodeURIComponent(document.getElementById('catalog-editor').innerHTML);
            break;
    }
    // dispatch message to server
    var ajax = new Ajax();
    ajax.asynchronousFlag = true;
    ajax.postRequest('coordinating-controller.php', data, function (str) { viewDispatcher(str, currentView) });
}

var setCurrentFilename = function (fileName) {
    currentCatalogFileName = fileName;
}

var viewDispatcher = function (str, message) {
    switch (message) {
        case 'read-catalog':
            displayInElement(str, 'catalog-editor');
            break;
        case 'read-code':
            displayInElement(str, 'code-editor');
            break;
        case 'write-catalog':
            alert(str);
            break;
        default :
            displayInElement(str, 'code-editor');
            //window.location.href = '#home-floor';
            break;            
    }
}

var displayInElement = function (str, elementName) {
    if (document.getElementById(elementName)) {
        document.getElementById(elementName).innerHTML = str;
        window.location.href = '#' + elementName + '-floor';
    }
    else {
        alert(elementName + ' element not found...')
    }
};

var goBackTo = function () {
    switch (currentAction) {
        case 'php-dal-base-class':
        case 'php-test-dal-base-class':
        case 'php-dal-class':
        case 'php-dal-anormapart-class':
        case 'php-test-dal-class':
        case 'php-connection-class':
        case 'php-provider-class':
            window.location.href = '#php-dal-floor';
            break;
        case 'php-bll-base-class':
        case 'php-test-bll-base-class':
        case 'php-bll-class':
        case 'php-bll-anormapart-class':
        case 'php-test-bll-class':
            window.location.href = '#php-bll-floor';
            break;
        case 'php-appmodel-class':
        case 'php-model-class':
        case 'php-model-anormapart-class':
        case 'php-model-base-class':
        case 'php-controller-class':
        case 'php-controller-anormapart-class':
        case 'php-template-fieldset':
        case 'php-view-anormapart-class':
        case 'php-template':
            window.location.href = '#php-mvc-floor';
            break;

        case 'csharp-model-class':
        case 'csharp-dal-class':
            window.location.href = '#csharp-mvc-floor';
            break;

        case 'my-sql-ddl':
        case 'my-sql-dml':
        case 'ms-sql-ddl':
        case 'ms-sql-dml':
            window.location.href = '#sql-floor';
            break;
        case 'php-feedback-class':
        case 'php-test-feedback-class':
        case 'php-log-class':
        case 'php-locale-log-class':
        case 'php-test-log-class':
        case 'php-session-class':
        case 'php-login-class':
        case 'php-login-test-class':
        case 'php-log-view':
        case 'ajax-class':
            window.location.href = '#php-helpers-floor';
            break;
        case 'read-catalog':
        default:
            window.location.href = '#home-floor';
            break;
    }
};

