<?php
    //////include 'appcode/membership/lib/session.class.php';
    //////include 'appcode/membership/lib/membership.class.php';
    // Our custom secure way of starting a php session. 
    // happens in the constructor of the membership class
    /////$membership = new \AnOrmApart\Web\Membership();
    // print_r($_SESSION);
    // includes used in all cases
    include('app/anormapart/code/bll/exception.class.php');
    include('app/anormapart/code/bll/row.class.php');
    include('app/anormapart/code/bll/table.class.php');
    include('app/anormapart/code/bll/catalog.class.php');
    $htmlCatalog = FALSE;
    if (isset($_POST['data']))
    {
        // dat is alleen het geval wanneer een nieuw cataloog
        // wordt opgeslaan. Dan wordt de naam van het bestand
        // en de cataloog doorgegeven.
        $htmlCatalog = urldecode($_POST['data']);
    }
    // echo 'test' . $htmlCatalog;
    // return;
    $action = $_POST['anormapart-action'];
    // echo $action;
    switch($action)  
    {   
        case 'make-template' :
            if ($htmlCatalog) {
                $zip = new \ZipArchive();
                $fileName = 'data/template/' . strtolower(($_POST['f']) . '.zip');
                if (file_exists($fileName)) {
                    unlink($fileName);
                }
                if ($zip->open($fileName, ZipArchive::CREATE)!==TRUE) {
                    echo ("cannot open <$fileName>\n");
                }
                else {
                    include('app/anormapart/code/bll/php/dal.class.php');
                    include('app/anormapart/code/bll/php/model.class.php');
                    include('app/anormapart/code/bll/php/controller.class.php');
                    include('app/anormapart/code/bll/php/view-template.class.php');
                    $tableCatalog = new \AnOrmApart\Php\Dal($action, $htmlCatalog);
                    $vendor = $tableCatalog->GetVendorToLower();
                    $applicationName = $tableCatalog->GetDatabaseNameToLower();


                    // Dal classes
                    // don't use $action as parameter, the value is used voor extend base table
                    $tableCatalog = new \AnOrmApart\Php\Dal('php-dal-class', $htmlCatalog);
                    $tableCatalog->zipAddScriptDalClasses($zip);


                    // Model classes
                    $tableCatalog = new \AnOrmApart\Php\Model($action, $htmlCatalog);
                    $tableCatalog->zipAddScriptModelClasses($zip);

                    // Controller classes
                    $tableCatalog = new \AnOrmApart\Php\Controller($action, $htmlCatalog);
                    $tableCatalog->zipAddScriptControllerClasses($zip);

                    // Templates
                    $tableCatalog = new \AnOrmApart\Php\Template($action, $htmlCatalog);
                    $tableCatalog->zipAddScriptTemplateAll($zip);

                    $zip->close();
                    echo ("Zip file with name $fileName created.\n");
                }
            }
            else
            {
                echo '<pre>Select a catalog first.</pre>';
            }
            break; 
        case 'read-catalog' :
            $fileName = 'app/anormapart/data/catalog/' . strtolower(($_POST['f']) . '.html');
            $htmlCatalog = file_get_contents($fileName);
            echo $htmlCatalog;
            break;
        case 'write-catalog' :
            $fileName = 'app/anormapart/data/catalog/' . strtolower(($_POST['f']) . '.html');
            if (file_put_contents($fileName, $htmlCatalog)) {
                echo "Changes in $fileName are saved!";
            }
            else {
                echo 'Changes in $fileName catalog are not saved.';
            }
            break;
        /******************* C# ***********************************************************/
        case 'csharp-dal-class' :
            if ($htmlCatalog)
            {
                include('app/anormapart/code/bll/csharp/dal.class.php');
                $tableCatalog = new \AnOrmApart\CSharp\Dal($action, $htmlCatalog);
                echo '<pre>' . htmlentities($tableCatalog->makeScriptDalClasses()) . '</pre>';
            }
            else
            {
                echo '<pre>Select a catalog first.</pre>';
            }
            break;
        case 'csharp-model-class' :
            if ($htmlCatalog)
            {
                include('app/anormapart/code/bll/csharp/model.class.php');
                $tableCatalog = new \AnOrmApart\CSharp\Model($action, $htmlCatalog);
                echo '<pre>' . htmlentities($tableCatalog->makeScriptModelClasses()) . '</pre>';
            }
            else
            {
                echo '<pre>Select a catalog first.</pre>';
            }
            break;
        /******************* PHP **********************************************************/
        case 'php-connection-class' :
            $script = file_get_contents('app/anormapart/code/bll/php/helpers/src/dal/Connection.php', false) . "\n";
            echo '<pre>' . htmlentities($script) . '</pre>';
            break;                        
        case 'php-test-connection-class' :
            $script = file_get_contents('app/anormapart/code/bll/php/helpers/src/dal/connection-test.php', false) . "\n";
            echo '<pre>' . htmlentities($script) . '</pre>';
            break;
        case 'php-provider-class' :
            $script = file_get_contents('app/anormapart/code/bll/php/helpers/src/dal/Provider.php', false) . "\n";
            echo '<pre>' . htmlentities($script) . '</pre>';
            break;                        
        case 'php-dal-base-class' :
            $script = file_get_contents('app/anormapart/code/bll/php/helpers/src/dal/Base.class.php', false) . "\n";
            echo '<pre>' . htmlentities($script) . '</pre>';
            break;
        case 'php-test-dal-base-class' :
            echo '<pre>Not yet available.</pre>';
            break;
        case 'php-dal-anormapart-class' :
            if ($htmlCatalog)
            {
                include('app/anormapart/code/bll/php/dal.class.php');
                $tableCatalog = new \AnOrmApart\Php\Dal($action, $htmlCatalog);
                echo '<pre>' . htmlentities($tableCatalog->makeScriptDalAnOrmApartClasses()) . '</pre>';
            }
            else
            {
                echo '<pre>Select a catalog first.</pre>';
            }
            break;
        case 'php-dal-class' :
            if ($htmlCatalog)
            {
                include('app/anormapart/code/bll/php/dal.class.php');
                $tableCatalog = new \AnOrmApart\Php\Dal($action, $htmlCatalog);
                echo '<pre>' . htmlentities($tableCatalog->makeScriptDalClasses()) . '</pre>';
            }
            else
            {
                echo '<pre>Select a catalog first.</pre>';
            }
            break;
        case 'php-test-dal-class' :
            if ($htmlCatalog)
            {
                include('app/anormapart/code/bll/php/daltest.class.php');
                $tableCatalog = new \AnOrmApart\Php\DalTest($action, $htmlCatalog);
                echo '<pre>' . htmlentities($tableCatalog->makeScriptDalTestAll()) . '</pre>';            
            }
            else
            {
                echo '<pre>Select a catalog first.</pre>';
            }
            break;    
        case 'php-bll-base-class' :
            $script = file_get_contents('app/anormapart/code/bll/php/helpers/src/bll/Base.php', false) . "\n";
            echo '<pre>' . htmlentities($script) . '</pre>';
            break;
        case 'php-test-bll-base-class' :
            $script = file_get_contents('app/anormapart/code/bll/php/helpers/src/base-bll-test.php', false) . "\n";
            echo '<pre>' . htmlentities($script) . '</pre>';
            break;
        case 'php-bll-AnOrmApart-class' :
            if ($htmlCatalog)
            {
                include('app/anormapart/code/bll/php/bll.class.php');
                $tableCatalog = new \AnOrmApart\Php\Bll($action, $htmlCatalog);
                echo '<pre>' . htmlentities($tableCatalog->makeScriptBllEntityAnOrmApartClasses()) . '</pre>';
            }
            else
            {
                echo '<pre>Select a catalog first.</pre>';
            }
            break;
        case 'php-bll-class' :
            if ($htmlCatalog)
            {
                include('app/anormapart/code/bll/php/bll.class.php');
                $tableCatalog = new \AnOrmApart\Php\Bll($action, $htmlCatalog);
                echo '<pre>' . htmlentities($tableCatalog->makeScriptBllEntityClasses()) . '</pre>';
            }
            else
            {
                echo '<pre>Select a catalog first.</pre>';
            }
            break;
        case 'php-test-bll-class' :
            if ($htmlCatalog)
            {
                include('app/anormapart/code/bll/php/daltest.class.php');
                $tableCatalog = new \AnOrmApart\Php\DalTest($action, $htmlCatalog);
                echo '<pre>' . htmlentities($tableCatalog->makeScriptBllTestAll()) . '</pre>';
            }
            else
            {
                echo '<pre>Select a catalog first.</pre>';
            }
            break;           
        case 'php-appmodel-class':
            if ($htmlCatalog)
            {
                include('app/anormapart/code/bll/php/model.class.php');
                $tableCatalog = new \AnOrmApart\Php\Model($action, $htmlCatalog);
                echo '<pre>' . htmlentities($tableCatalog->makeScriptAppModelClass()) . '</pre>';
            }
            else
            {
                echo '<pre>Select a catalog first.</pre>';
            }
            break;
        case 'php-model-base-class' :
            $script = file_get_contents('app/anormapart/code/bll/php/helpers/src/model/Base.class.php', false) . "\n";
            echo '<pre>' . htmlentities($script) . '</pre>';
            break;
        case 'php-model-anormapart-class' :
            if ($htmlCatalog)
            {
                include('app/anormapart/code/bll/php/model.class.php');
                $tableCatalog = new \AnOrmApart\Php\Model($action, $htmlCatalog);
                echo '<pre>' . htmlentities($tableCatalog->makeScriptModelAnOrmApartClasses()) . '</pre>';
            }
            else
            {
                echo '<pre>Select a catalog first.</pre>';
            }
            break;
        case 'php-model-class' :
            if ($htmlCatalog)
            {
                include('app/anormapart/code/bll/php/model.class.php');
                $tableCatalog = new \AnOrmApart\Php\Model($action, $htmlCatalog);
                echo '<pre>' . htmlentities($tableCatalog->makeScriptModelClasses()) . '</pre>';
            }
            else
            {
                echo '<pre>Select a catalog first.</pre>';
            }
            break;            
        case 'php-controller-anormapart-class' :
            if ($htmlCatalog)
            {
                include('app/anormapart/code/bll/php/controller.class.php');
                $tableCatalog = new \AnOrmApart\Php\Controller($action, $htmlCatalog);
                echo '<pre>' . htmlentities($tableCatalog->makeScriptControllerAnOrmApartClasses()) . '</pre>';
            }
            else
            {
                echo '<pre>Select a catalog first.</pre>';
            }
            break;            
        case 'php-controller-class' :
            if ($htmlCatalog)
            {
                include('app/anormapart/code/bll/php/controller.class.php');
                $tableCatalog = new \AnOrmApart\Php\Controller($action, $htmlCatalog);
                echo '<pre>' . htmlentities($tableCatalog->makeScriptControllerClasses()) . '</pre>';
            }
            else
            {
                echo '<pre>Select a catalog first.</pre>';
            }
            break;
        case 'php-view-class' :
            if ($htmlCatalog)
            {
                include('app/anormapart/code/bll/php/view.class.php');
                $tableCatalog = new \AnOrmApart\Php\View($action, $htmlCatalog);
                echo '<pre>' . htmlentities($tableCatalog->makeScriptViewClasses()) . '</pre>';
            }
            else
            {
                echo '<pre>Select a catalog first.</pre>';
            }
            break;
        case 'php-template-fieldset' :
            if ($htmlCatalog)
            {
                include('app/anormapart/code/bll/php/view-template.class.php');
                $tableCatalog = new \AnOrmApart\Php\Template($action, $htmlCatalog);
                echo '<pre>' . htmlentities($tableCatalog->makeScriptTemplateComponentsAll()) . '</pre>';
            }
            else
            {
                echo '<pre>Select a catalog first.</pre>';
            }
            break;
        case 'php-template' :
            if ($htmlCatalog)
            {
                include('app/anormapart/code/bll/php/view-template.class.php');
                $tableCatalog = new \AnOrmApart\Php\Template($action, $htmlCatalog);
                // var_dump($tableCatalog);
                echo '<pre>' . htmlentities($tableCatalog->makeScriptTemplateAll()) . '</pre>';
            }
            else
            {
                echo '<pre>Select a catalog first.</pre>';
            }
            break;
        /*********************************** Threepenny MVC **********************************/
        case 'php-threepenny-controller-class' :
            if ($htmlCatalog)
            {
                include('app/anormapart/code/bll/php/threepenny.controller.class.php');
                $tableCatalog = new \AnOrmApart\Php\Threepenny\Controller($action, $htmlCatalog);
                echo '<pre>' . htmlentities($tableCatalog->makeScriptControllerClasses()) . '</pre>';
            }
            else
            {
                echo '<pre>Select a catalog first.</pre>';
            }
            break;
        case 'php-threepenny-view-class' :
            if ($htmlCatalog)
            {
                include('app/anormapart/code/bll/php/threepenny.view.class.php');
                $tableCatalog = new \AnOrmApart\Php\Threepenny\View($action, $htmlCatalog);
                // var_dump($tableCatalog);
                echo '<pre>' . htmlentities($tableCatalog->makeScriptTemplateAll()) . '</pre>';
            }
            else
            {
                echo '<pre>Select a catalog first.</pre>';
            }
            break;        /*********************************** SQL *************************************************/
        case 'my-sql-ddl' :
            if ($htmlCatalog)
            {
                include('app/anormapart/code/bll/sql/sql.class.php');
                $tableCatalog = new \AnOrmApart\MySql\Catalog($action, $htmlCatalog);
                echo '<pre>' . $tableCatalog->GetCreateTableSqlScriptAll() . '</pre>';
            }
            else
            {
                echo '<pre>Select a catalog first.</pre>';
            }
            break;
        case 'my-sql-dml' :
            if ($htmlCatalog)
            {
                include('app/anormapart/code/bll/sql/sql.class.php');
                $tableCatalog = new \AnOrmApart\MySql\Catalog($action, $htmlCatalog);
                echo '<pre>' . htmlentities($tableCatalog->GetDMLAll()) . '</pre>';
            }
            else
            {
                echo '<pre>Select a catalog first.</pre>';
            }
            break;          
        case 'ms-sql-ddl' :
            if ($htmlCatalog)
            {
                include('app/anormapart/code/bll/sql/sql.class.php');
                $tableCatalog = new \AnOrmApart\MySql\Catalog($action, $htmlCatalog);
                echo '<pre>' . $tableCatalog->GetCreateTableSqlScriptAll() . '</pre>';
            }
            else
            {
                echo '<pre>Select a catalog first.</pre>';
            }
            break;
        case 'ms-sql-dml' :
            if ($htmlCatalog)
            {
                include('app/anormapart/code/bll/sql/sql.class.php');
                $tableCatalog = new \AnOrmApart\MySql\Catalog($action, $htmlCatalog);
                echo '<pre>' . htmlentities($tableCatalog->GetDMLAll()) . '</pre>';
            }
            else
            {
                echo '<pre>Select a catalog first.</pre>';
            }
            break;
        /**************************************** helpers *************************************/
        case 'ajax-class' :
            $script = file_get_contents('app/anormapart/code/bll/php/helpers/src/ajax.js', false) . "\n";
            echo '<pre>' . htmlentities($script) . '</pre>';
            break;
        case 'php-login-view' :
            $script = file_get_contents('app/anormapart/code/bll/php/helpers/src/login-view.php', false) . "\n";
            echo '<pre>' . htmlentities($script) . '</pre>';
            break;
        case 'php-register-view' :
            $script = file_get_contents('app/anormapart/code/bll/php/helpers/src/register-view.php', false) . "\n";
            echo '<pre>' . htmlentities($script) . '</pre>';
            break;
        case 'php-membership-html' :
            $script = file_get_contents('app/anormapart/code/bll/php/helpers/src/membership-test.php', false) . "\n";
            echo '<pre>' . htmlentities($script) . '</pre>';
            break;
        case 'php-membership-class' :
            $script = file_get_contents('app/anormapart/code/bll/php/helpers/src/membership.class.php', false) . "\n";
            echo '<pre>' . htmlentities($script) . '</pre>';
            break;
        case 'php-feedback-class' :
            $script = file_get_contents('app/anormapart/code/bll/php/helpers/src/Feedback.php', false) . "\n";
            echo '<pre>' . htmlentities($script) . '</pre>';
            break;
        case 'php-password-hash' :
            $script = file_get_contents('app/anormapart/code/bll/php/helpers/src/password.php', false) . "\n";
            echo '<pre>' . htmlentities($script) . '</pre>';
            break;
        case 'php-test-feedback-class' :
            $script = file_get_contents('app/anormapart/code/bll/php/helpers/src/feedback-test.php', false) . "\n";
            echo '<pre>' . htmlentities($script) . '</pre>';
            break;
        case 'php-log-class' :
            $script = file_get_contents('app/anormapart/code/bll/php/helpers/src/Log.php', false) . "\n";
            echo '<pre>' . htmlentities($script) . '</pre>';
            break;
        case 'php-locale-log-class':
            $script = file_get_contents('app/anormapart/code/bll/php/helpers/src/LogApp.php', false) . "\n";
            echo '<pre>' . htmlentities($script) . '</pre>';
            break;
        case 'php-session-class':
            $script = file_get_contents('app/anormapart/code/bll/php/helpers/src/Session.php', false) . "\n";
            echo '<pre>' . htmlentities($script) . '</pre>';
            break;
        case 'php-login-class':
            $script = file_get_contents('app/anormapart/code/bll/php/helpers/src/Login.php', false) . "\n";
            echo '<pre>' . htmlentities($script) . '</pre>';
            break;
        case 'php-login-test-class':
            $script = file_get_contents('app/anormapart/code/bll/php/helpers/src/LoginTest.php', false) . "\n";
            echo '<pre>' . htmlentities($script) . '</pre>';
            break;
        case 'php-test-log-class' :
            $script = file_get_contents('app/anormapart/code/bll/php/helpers/src/log-test.php', false) . "\n";
            echo '<pre>' . htmlentities($script) . '</pre>';
            break;
        case 'php-log-view' :
            $script = file_get_contents('app/anormapart/code/bll/php/helpers/src/log-view.php', false) . "\n";
            echo '<pre>' . htmlentities($script) . '</pre>';
            return;
        default : 
            break;
    }
?>