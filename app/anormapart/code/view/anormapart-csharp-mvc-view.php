<!-- anormapart-php-view   1/02/2015 JI -->
<div class="floor csharp" id="csharp-mvc-floor">
    <div class="control-panel">
        <a href="#home-floor" class="tile hover _14x1">
            <span class="icon-arrow-left"></span>
            <span class="screen-reader-text">Back</span>
        </a>
        <a href="#home-floor" class="tile _14x1">
            <span class="icon-menu2"></span>
            <span class="screen-reader-text">Home</span>
        </a>
        <h1>an<span>orm</span>apart</h1>
    </div>
    <div class="room">
        <div class="tile hover">
            <a href="#catalog-floor" class="mask fade-in-left">
                <h2>Catalogs</h2>
                <p>Select another catalog or create a new one based on a catalog template.</p>
                <span class="action">Select</span>
            </a>
            <h1>Catalogs</h1>
            <p>A list of catalogs</p>
        </div>

        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="csharp-model-class">
                <h2>C# Model class</h2>
                <p>Generates a Model domain class.</p>
                <span class="action">GO</span>
            </a>
            <h1>C#</h1>
            <p>model class</p>
        </div>


        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="csharp-controller-class">
                <h2>C# controller class</h2>
                <p>Generates controller classes for PHP.</p>
                <span class="action">GO</span>
            </a>
            <h1>C#</h1>
            <p>controller</p>
        </div>

        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="csharp-view-class">
                <h2>C# view class</h2>
                <p>Generates view classes for PHP.</p>
                <span class="action">GO</span>
            </a>
            <h1>C#</h1>
            <p>View</p>
        </div>
        <div class="tile hover">
            <div class="mask fade-in-left">
                <p>Model View Controller (MVC) is one of the most quoted - and most misquoted - patterns around.
                    It started as a framework developed by <em>Trygve Reenskaug</em> for the Smalltalk platform in the late 1970s.
                    Since then it has played an influential role in most UI frameworks and in the thinking about UI design.</p>
            </div>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="csharp-template">
                <h2>C# template</h2>
                <p>Generates different templates for views.</p>
                <span class="action">GO</span>
            </a>
            <h1>C#</h1>
            <p>Template</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="csharp-dal-class">
                <h2>DAL class(es)</h2>
                <p>Generates the C# DAL class(es) for the selected catalog.</p>
                <span class="action">DO</span>
            </a>
            <h1>DAL</h1>
            <p>class(es)</p>
        </div>
        <div class="tile">
        </div>
        <div class="tile">
        </div>

        <div class="tile">
        </div>
        <div class="tile">
        </div>

    </div>
</div>

