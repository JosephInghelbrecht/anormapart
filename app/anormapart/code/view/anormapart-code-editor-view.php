<!-- anormapart.code-editor-view   1/02/2015 JI -->
<div class="floor editor" id="code-editor-floor">
    <div class="control-panel">
        <a href="#home-floor" class="tile hover _14x1" onclick="closeEditor(); return false;">
            <span class="icon-arrow-left"></span>
            <span class="screen-reader-text">Back</span>
        </a>
        <a href="#second-floor" class="tile hover _14x1" onclick="closeEditor(true); return false;">
            <span class="icon-arrow-right"></span>
            <span class="screen-reader-text">Next</span>
        </a>
        <a href="#home-floor" class="tile _14x1" onclick="closeEditor(true); return false;">
            <span class="icon-menu2"></span>
            <span class="screen-reader-text">Home</span>
        </a>
        <h1>an<span>orm</span>apart</h1>
    </div>
    <div class="room">
        <div id="code-editor" class="editable"></div>
    </div>
</div>
