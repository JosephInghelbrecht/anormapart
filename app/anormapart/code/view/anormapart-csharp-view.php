<!-- anormapart-php-view   1/02/2015 JI -->
<div class="floor csharp" id="csharp-floor">
    <div class="control-panel">
        <a href="#home-floor" class="tile hover _14x1">
            <span class="icon-arrow-left"></span>
            <span class="screen-reader-text">Back</span>
        </a>
        <a href="#csharp-dal-floor" class="tile hover _14x1">
            <span class="icon-arrow-right"></span>
            <span class="screen-reader-text">Next</span>
        </a>
        <a href="#home-floor" class="tile _14x1">
            <span class="icon-menu2"></span>
            <span class="screen-reader-text">Home</span>
        </a>
        <h1>an<span>orm</span>apart</h1>
    </div>
    <div class="room">
        <div class="tile hover">
            <a href="#catalog-floor" class="mask fade-in-left">
                <h2>Catalogs</h2>
                <p>Select another catalog or create a new one based on a catalog template.</p>
                <span class="action">Select</span>
            </a>
            <h1>Catalogs</h1>
            <p>A list of catalogs</p>
        </div>
        <div class="tile hover">
            <a href="#scharp-bll-floor" class="mask fade-in-left">
                <h2>C# BLL</h2>
                <p>Generates only C# code, no need to learn a different syntax. Just build further on the C# knowledge you already have.</p>
                <span class="action">Go</span>
            </a>
            <h1>PHP</h1>
            <p>bll</p>
        </div>
        <div class="tile hover">
            <a href="#csharp-dal-floor" class="mask fade-in-left">
                <h2>C# DAL</h2>
                <p>Generates only C# code, no need to learn a different syntax. Just build further on the PHP knowledge you already have.</p>
                <span class="action">Go</span>
            </a>
            <h1>C#</h1>
            <p>dal</p>
        </div>
        <div class="tile hover">
            <a href="#csharp-mvc-floor" class="mask fade-in-left">
                <h2>C# MODEL, VIEW en CONTROLLER</h2>
                <p>Generates only C# code, no need to learn a different syntax. Just build further on the PHP knowledge you already have.</p>
                <span class="action">Go</span>
            </a>
            <h1>C#</h1>
            <p>MVC</p>
        </div>
        <div class="tile">
        </div>
        <div class="tile hover">
            <a href="#csharp-helpers-floor" class="mask fade-in-left">
                <h2>C# helpers</h2>
                <p>Helper classes, classes for Feedback, Log, Connection, ...</p>
                <span class="action">Go</span>
            </a>
            <h1>C#</h1>
            <p>helpers</p>
        </div>
        <div class="tile">
        </div>
        <div class="tile">
        </div>
        <div class="tile">
        </div>
        <div class="tile">
        </div>
        <div class="tile">
        </div>
    </div>
</div>