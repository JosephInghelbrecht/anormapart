<!-- anormapart-php-dal-view   1/02/2015 JI -->
<div class="floor php" id="php-helpers-floor">
    <div class="control-panel">
        <a href="#php-mvc-floor" class="tile hover _14x1">
            <span class="icon-arrow-left"></span>
            <span class="screen-reader-text">Back</span>
        </a>
        <a href="#php-dal-floor" class="tile hover _14x1">
            <span class="icon-arrow-right"></span>
            <span class="screen-reader-text">Next</span>
        </a>
        <a href="#php-floor" class="tile _14x1">
            <span class="icon-menu2"></span>
            <span class="screen-reader-text">Home</span>
        </a>
        <h1>an<span>orm</span>apart</h1>
    </div>
    <div class="room">
        <div class="tile hover">
            <a href="#catalog-floor" class="mask fade-in-left">
                <h2>Catalogs</h2>
                <p>Select another catalog or create a new one based on a catalog template.</p>
                <span class="action">Open</span>
            </a>
            <h1>Catalogs</h1>
            <p>A list of catalogs</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-feedback-class">
                <h2>Feedback class</h2>
                <p>Generates the feedback class. This class is used to store the feedback of one event, action, ...</p>
                <span class="action">GO</span>
            </a>
            <h1>FEEDBACK</h1>
            <p>class</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-test-feedback-class">
                <h2>Feedback test</h2>
                <p>Generates code to test and learn how to use the Feedback class.</p>
                <span class="action">Go</span>
            </a>
            <h1>Feedback</h1>
            <p>test</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-log-class">
                <h2>PHP Log class</h2>
                <p>Generates code for a logbook class to store individual feedback objects during a process.</p>
                <span class="action">Go</span>
            </a>
            <h1>PHP</h1>
            <p>logbook</p>
        </div>
        <div class="tile">
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-test-log-class">
                <h2>PHP test log class</h2>
                <p>Generates code to test and learn how to use the logbook class to store individual feedback objects.</p>
                <span class="action">Go</span>
            </a>
            <h1>Logbook</h1>
            <p>test</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-locale-log-class">
                <h2>PHP Locale log class</h2>
                <p>Generates code to test and learn how to use the locale logbook class to store individual feedback objects.</p>
                <span class="action">Go</span>
            </a>
            <h1>Locale Logbook</h1>
            <p>Gelocaliseerde en geïnternationaliseerde feedback</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-log-view">
                <h2>PHP Log view code</h2>
                <p>Generates HTML and PHP code to view the entries of a logbook.</p>
                <span class="action">Go</span>
            </a>
            <h1>View</h1>
            <p>logbook</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-session-class">
                <h2>PHP Session class</h2>
                <p>Session class.</p>
                <span class="action">Go</span>
            </a>
            <h1>Session</h1>
            <p>class</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-login-class">
                <h2>PHP Login class</h2>
                <p>Login class.</p>
                <span class="action">Go</span>
            </a>
            <h1>Login class</h1>
            <p>code</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-login-test-class">
                <h2>PHP Login class</h2>
                <p>Code to test the Login class.</p>
                <span class="action">Go</span>
            </a>
            <h1>Login</h1>
            <p>Test</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-password-hash">
                <h2>PHP Password Hash</h2>
                <p>A Compatibility library with PHP 5.5's simplified password hashing API<br/>MIT license.</p>
                <span class="action">Go</span>
            </a>
            <h1>Password</h1>
            <p>Hash Library</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="ajax-class">
                <h2>Ajax class</h2>
                <p>JavaScript Ajax class</p>
                <span class="action">Go</span>
            </a>
            <h1>AJAX</h1>
            <p>JavaScript class</p>
        </div>
        <div class="tile">
        </div>
        <div class="tile">
        </div>
        <div class="tile">
        </div>
    </div>
</div>