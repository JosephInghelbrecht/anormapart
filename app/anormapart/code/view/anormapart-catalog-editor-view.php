<!-- anormapart.code-editor-view   1/02/2015 JI -->
<div class="floor editor" id="catalog-editor-floor">
    <div class="control-panel">
        <a href="#home-floor" class="tile hover _14x1" id="close-editor">
            <span class="icon-arrow-left"></span>
            <span class="screen-reader-text">Back</span>
        </a>
        <a href="#home-floor" class="tile _14x1" onclick="closeEditor(true);">
            <span class="icon-menu2"></span>
            <span class="screen-reader-text">Home</span>
        </a>
        <h1>an<span>orm</span>apart</h1>
    </div>
    <div class="room">
        <div id="catalog-editor" class="editable"></div>
    </div>
</div>
