<!-- anormapart-php-view   1/02/2015 JI -->
<div class="floor php" id="php-mvc-floor">
    <div class="control-panel">
        <a href="#php-bll-floor" class="tile hover _14x1">
            <span class="icon-arrow-left"></span>
            <span class="screen-reader-text">Back</span>
        </a>
        <a href="#php-helpers-floor" class="tile hover _14x1">
            <span class="icon-arrow-right"></span>
            <span class="screen-reader-text">Next</span>
        </a>
        <a href="#php-floor" class="tile _14x1">
            <span class="icon-menu2"></span>
            <span class="screen-reader-text">Home</span>
        </a>
        <h1>an<span>orm</span>apart</h1>
    </div>
    <div class="room">
        <div class="tile hover">
            <a href="#catalog-floor" class="mask fade-in-left">
                <h2>Catalogs</h2>
                <p>Select another catalog or create a new one based on a catalog template.</p>
                <span class="action">Select</span>
            </a>
            <h1>Catalogs</h1>
            <p>A list of catalogs</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-model-base-class">
                <h2>PHP base Model class</h2>
                <p>Generates a base class for Model domain classes.</p>
                <span class="action">GO</span>
            </a>
            <h1>PHP</h1>
            <p>base model class</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-appmodel-class">
                <h2>PHP Application Model</h2>
                <p>Generates a Application Model domain class that contains references to all entity models</p>
                <span class="action">GO</span>
            </a>
            <h1>PHP</h1>
            <p>application model class</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-model-class">
                <h2>PHP Model class</h2>
                <p>Generates a Model domain class.</p>
                <span class="action">GO</span>
            </a>
            <h1>PHP</h1>
            <p>model class</p>
        </div>
        <div class="tile hover">
            <div class="mask fade-in-left">
                <p>Model View Controller (MVC) is one of the most quoted - and most misquoted - patterns around. 
                    It started as a framework developed by <em>Trygve Reenskaug</em> for the Smalltalk platform in the late 1970s. 
                    Since then it has played an influential role in most UI frameworks and in the thinking about UI design.</p>
            </div>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-model-anormapart-class">
                <h2>PHP Model codebehind class</h2>
                <p>Generates a Model domain codebehind class.</p>
                <span class="action">GO</span>
            </a>
            <h1>PHP</h1>
            <p>codebehind model class</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-controller-class">
                <h2>PHP controller class</h2>
                <p>Generates controller classes for PHP.</p>
                <span class="action">GO</span>
            </a>
            <h1>PHP</h1>
            <p>controller</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-controller-anormapart-class">
                <h2>PHP controller codebehind class</h2>
                <p>Generates controller codebehind classes for PHP.</p>
                <span class="action">DO</span>
            </a>
            <h1>PHP</h1>
            <p>controller codebehind</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-view-class">
                <h2>PHP view class</h2>
                <p>Generates view classes for PHP.</p>
                <span class="action">GO</span>
            </a>
            <h1>PHP</h1>
            <p>View</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-template-fieldset">
                <h2>PHP template Fieldset</h2>
                <p>Generates Fieldset templates façon Denis</p>
                <span class="action">DO</span>
            </a>
            <h1>PHP</h1>
            <p>Fieldset templates</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-template">
                <h2>PHP template</h2>
                <p>Generates different templates for views.</p>
                <span class="action">GO</span>
            </a>
            <h1>PHP</h1>
            <p>Template</p>
        </div>
    </div>
</div>

