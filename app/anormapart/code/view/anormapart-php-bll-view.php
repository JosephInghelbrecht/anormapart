<!-- anormapart-php-bll-view   1/02/2015 JI -->
<div class="floor php" id="php-bll-floor">
    <div class="control-panel">
        <a href="#php-dal-floor" class="tile hover _14x1">
            <span class="icon-arrow-left"></span>
            <span class="screen-reader-text">Back</span>
        </a>
        <a href="#php-mvc-floor" class="tile hover _14x1">
            <span class="icon-arrow-right"></span>
            <span class="screen-reader-text">Next</span>
        </a>
        <a href="#php-floor" class="tile _14x1">
            <span class="icon-menu2"></span>
            <span class="screen-reader-text">Home</span>
        </a>
        <h1>an<span>orm</span>apart</h1>
    </div>
    <div class="room">
        <div class="tile hover">
            <a href="#catalog-floor" class="mask fade-in-left">
                <h2>Catalogs</h2>
                <p>Select another catalog or create a new one based on a catalog template.</p>
                <span class="action">Open</span>
            </a>
            <h1>Catalogs</h1>
            <p>A list of catalogs</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-bll-base-class">
                <h2>BLL base class</h2>
                <p>Generates the base class for table BLL classes using PDO.</p>
                <span class="action">DO</span>
            </a>
            <h1>BLL</h1>
            <p>base class</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-test-bll-base-class">
                <h2>BLL test base class</h2>
                <p>Generates code to test the BLL base class.</p>
                <span class="action">Kies</span>
            </a>
            <h1>BLL</h1>
            <p>test base class</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-bll-class">
                <h2>BLL class</h2>
                <p>Generates the php BLL class(es) for the selected catalog using PDO.</p>
                <span class="action">DO</span>
            </a>
            <h1>BLL</h1>
            <p>class(es)</p>
        </div>
        <div class="tile">
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-bll-codebehind-class">
                <h2>BLL codebehind class</h2>
                <p>Generates the php BLL class(es) for the selected catalog using PDO.</p>
                <span class="action">DO</span>
            </a>
            <h1>BLL</h1>
            <p>codebehind class(es)</p>
        </div>
        <div class="tile hover">
            <a href="#seventh-floor" class="mask fade-in-left" id="php-test-bll-class">
                <h2>BLL test class</h2>
                <p>Generates code to test the BLL classes.</p>
                <span class="action">Kies</span>
            </a>
            <h1>BLL</h1>
            <p>test</p>
        </div>
        <div class="tile">
        </div>
        <div class="tile">
        </div>
        <div class="tile">
        </div>
        <div class="tile">
        </div>
    </div>
</div>