<!-- anormapart.catalog-view   1/02/2015 JI -->
<div class="floor catalog" id="catalog-floor">
    <div class="control-panel">
        <a href="#home-floor" class="tile hover _14x1">
            <span class="icon-arrow-left"></span>
            <span class="screen-reader-text">Back</span>
        </a>
        <a href="#php-floor" class="tile hover _14x1">
            <span class="icon-arrow-right"></span>
            <span class="screen-reader-text">Next</span>
        </a>
        <a href="#home-floor" class="tile _14x1">
            <span class="icon-menu2"></span>
            <span class="screen-reader-text">Home</span>
        </a>
        <h1>an<span>orm</span>apart</h1>
    </div>
    <div class="room">
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="new-catalog">
                <h2>Enter name:</h2>
                <input type="text" name="newfilename" id="newfilename" style="width: 90%; margin-left: 5%; line-height: 1.4em; height: 2em;">
                <span class="action" style="cursor: pointer">New</span>
            </a>
            <h1>Catalog</h1>
            <p>new</p>
        </div>

        <?php
            /*  JI
                Read the filenames from folder appdata/catalog
                must be called from a page in the root
            */
            $countCatalogs = 0;
            $imageTile = 6;
            foreach (glob("app/anormapart/data/catalog/*") as $filepath)
            {
                $countCatalogs++;
                if ($countCatalogs == $imageTile)
                {
                    $countCatalogs++;
         ?>  
                    <div class="tile">
                    </div>
                 
         <?php  }
                if (!is_dir($filepath))
                {
                    $fileinfo = pathinfo($filepath);
                    $filename = $fileinfo['filename'];
                    $data = file_get_contents($filepath);
                    $pattern = '/<!--(.*?)-->/si';
                    $maskText = '';
                    if (preg_match($pattern, $data, $matches)) {
                        foreach($matches as $key=>$match)
                        {
                            $maskText .= $match;
                        }
                    }
                    else
                        $maskText = 'Use the catalog to generate Back- and front-end and SQL.';
        ?>
        <div class="tile hover">
            <a href="#catalog-editor-floor" class="mask fade-in-left" 
                onclick="depotDispatcher('open-catalog', '<?php echo $filename?>'); return true;">
                <h2><?php echo $filename; ?></h2>
                <p><?php echo $maskText; ?></p>
                <span class="action">Open</span>
            </a>
            <h1><?php echo basename($filename, '.cat'); ?></h1>
            <p>catalog</p>
        </div>
        <?php
                    }
                }
            // fill with empty tiles upto 16
            // is afbeelding al gepasseerd?
            if ($countCatalogs < $imageTile)
            {
                $countCatalogs++;
            }
            while (++$countCatalogs < 16)
            {
        ?>
        <div class="tile">
        </div>
        <?php
            }
        ?>
    </div>
</div>