<!-- anormapart-php-view   1/02/2015 JI -->
<div class="floor php" id="php-floor">
    <div class="control-panel">
        <a href="#home-floor" class="tile hover _14x1">
            <span class="icon-arrow-left"></span>
            <span class="screen-reader-text">Back</span>
        </a>
        <a href="#php-dal-floor" class="tile hover _14x1">
            <span class="icon-arrow-right"></span>
            <span class="screen-reader-text">Next</span>
        </a>
        <a href="#home-floor" class="tile _14x1">
            <span class="icon-menu2"></span>
            <span class="screen-reader-text">Home</span>
        </a>
        <h1>an<span>orm</span>apart</h1>
    </div>
    <div class="room">
        <div class="tile hover">
            <a href="#catalog-floor" class="mask fade-in-left">
                <h2>Catalogs</h2>
                <p>Select another catalog or create a new one based on a catalog template.</p>
                <span class="action">Select</span>
            </a>
            <h1>Catalogs</h1>
            <p>A list of catalogs</p>
        </div>
        <div class="tile hover">
            <a href="#php-bll-floor" class="mask fade-in-left">
                <h2>PHP BLL</h2>
                <p>Generates only PHP code, no need to learn a different syntax. Just build further on the PHP knowledge you already have.</p>
                <span class="action">Go</span>
            </a>
            <h1>PHP</h1>
            <p>bll</p>
        </div>
        <div class="tile hover">
            <a href="#php-dal-floor" class="mask fade-in-left">
                <h2>PHP DAL</h2>
                <p>Generates only PHP code, no need to learn a different syntax. Just build further on the PHP knowledge you already have.</p>
                <span class="action">Go</span>
            </a>
            <h1>PHP</h1>
            <p>dal</p>
        </div>
        <div class="tile hover">
            <a href="#php-mvc-floor" class="mask fade-in-left">
                <h2>PHP MODEL, VIEW en CONTROLLER</h2>
                <p>Generates only PHP code, no need to learn a different syntax. Just build further on the PHP knowledge you already have.</p>
                <span class="action">Go</span>
            </a>
            <h1>PHP</h1>
            <p>MVC</p>
        </div>
        <div class="tile">
        </div>
        <div class="tile hover">
            <a href="#php-helpers-floor" class="mask fade-in-left">
                <h2>PHP helpers</h2>
                <p>Helper classes, classes for Feedback, Log, Connection, ...</p>
                <span class="action">Go</span>
            </a>
            <h1>PHP</h1>
            <p>helpers</p>
        </div>
        <div class="tile hover">
            <a href="#php-threepenny-floor" class="mask fade-in-left">
                <h2>PHP Threepenny</h2>
                <p>PHP MODEL, VIEW en CONTROLLER</p>
                <span class="action">Go</span>
            </a>
            <h1>PHP</h1>
            <p>Threepenny</p>

        </div>
        <div class="tile">
        </div>
        <div class="tile">
        </div>
        <div class="tile">
        </div>
        <div class="tile">
        </div>
    </div>
</div>