<!-- anormapart-sql-view   1/02/2015 JI -->
<div class="floor sql" id="sql-floor">
    <div class="control-panel">
        <a href="#home-floor" class="tile hover _14x1">
            <span class="icon-arrow-left"></span>
            <span class="screen-reader-text">Back</span>
        </a>
        <a href="#php-dal-floor" class="tile hover _14x1">
            <span class="icon-arrow-right"></span>
            <span class="screen-reader-text">Next</span>
        </a>
        <a href="#home-floor" class="tile _14x1">
            <span class="icon-menu2"></span>
            <span class="screen-reader-text">Home</span>
        </a>
        <h1>an<span>orm</span>apart</h1>
    </div>
    <div class="room">
        <div class="tile hover">
            <a href="#catalog-floor" class="mask fade-in-left">
                <h2>Catalogs</h2>
                <p>Select another catalog or create a new one based on a catalog template.</p>
                <span class="action">Kies</span>
            </a>
            <h1>Catalogs</h1>
            <p>A list of catalogs</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="my-sql-ddl">
                <h2>MySQL DDL</h2>
                <p>Generates SQL scripts for creating tables and constraints.</p>
                <span class="action">GO</span>
            </a>
            <h1>DDL</h1>
            <p>MySQL</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="my-sql-dml">
                <h2>MySQL DML</h2>
                <p>Generates SQL scripts for CRUD stored procedures.</p>
                <span class="action">GO</span>
            </a>
            <h1>DML</h1>
            <p>MySQL</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="ms-sql-ddl">
                <h2>MsSQL DDL</h2>
                <p>Generates SQL scripts for creating tables and constraints.</p>
                <span class="action">Lees meer...</span>
            </a>
            <h1>DDL</h1>
            <p>MsSQL</p>
        </div>
        <div class="tile hover">
            <div class="mask fade-in-left">
                <p><em>Edgar F. Codd</em> transformed the entire database field — which before him consisted of a collection of ad hoc products,
                    proposals, and techniques — into a scientific discipline. More specifically, he provided a theoretical
                framework within which a variety of important database problems could be attacked in a scientific manner.</p>
            </div>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="ms-sql-dml">
                <h2>MSSQL DML</h2>
                <p>Generates SQL scripts for CRUD stored procedures.</p>
                <span class="action">Kies</span>
            </a>
            <h1>DML</h1>
            <p>MsSQL</p>
        </div>
        <div class="tile">
        </div>
        <div class="tile">
        </div>
        <div class="tile">
        </div>
    </div>
</div>