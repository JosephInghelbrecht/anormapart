<!-- anormapart.template-view   1/05/2015 JI -->
<div class="floor catalog" id="template-floor">
    <div class="control-panel">
        <a href="#home-floor" class="tile hover _14x1">
            <span class="icon-arrow-left"></span>
            <span class="screen-reader-text">Back</span>
        </a>
        <a href="#php-floor" class="tile hover _14x1">
            <span class="icon-arrow-right"></span>
            <span class="screen-reader-text">Next</span>
        </a>
        <a href="#home-floor" class="tile _14x1">
            <span class="icon-menu2"></span>
            <span class="screen-reader-text">Home</span>
        </a>
        <h1>an<span>orm</span>apart</h1>
    </div>
    <div class="room">
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="make-template">
                <h2>Enter name:</h2>
                <input type="text" name="newtemplatename" id="newtemplatename" style="width: 90%; margin-left: 5%; line-height: 1.4em; height: 2em;">
                <span class="action" style="cursor: pointer">New</span>
            </a>
            <h1>Template</h1>
            <p>new</p>
        </div>

        <?php
            /*  JI
                Read the filenames from folder appdata/catalog
                must be called from a page in the root
            */
            $countFiles = 0;
            $imageTile = 6;
            foreach (glob("data/template/*") as $filepath) {
                $countFiles++;
                if ($countFiles == $imageTile) {
                    $countFiles++;
                ?>
                    <div class="tile">
                    </div>

                <?php
                }
                if (!is_dir($filepath)) {
                    $fileinfo = pathinfo($filepath);
                    $filename = $fileinfo['filename'];
                    $maskText = 'Zipfile with code.';
                ?>
                    <div class="tile hover">
                        <a href="<?php echo 'data/template/' . $filename. '.zip';?>" class="mask fade-in-left">
                            <h2><?php echo $filename; ?></h2>
                            <p><?php echo $maskText; ?></p>
                            <span class="action">Download</span>
                        </a>
                        <h1><?php echo basename($filename, '.zip'); ?></h1>
                        <p>AnOrmApart Template</p>
                    </div>
                <?php
                }
            }
            // fill with empty tiles upto 16
            // is afbeelding al gepasseerd?
            if ($countFiles < $imageTile) {
                $countFiles++;
            }
            while (++$countFiles < 16) {
            ?>
                <div class="tile">
                </div>
            <?php
            }
        ?>
    </div>
</div>