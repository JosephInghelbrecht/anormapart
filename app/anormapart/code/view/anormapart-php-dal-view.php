<!-- anormapart-php-dal-view   1/02/2015 JI -->
<div class="floor php" id="php-dal-floor">
    <div class="control-panel">
        <a href="#php-helpers-floor" class="tile hover _14x1">
            <span class="icon-arrow-left"></span>
            <span class="screen-reader-text">Back</span>
        </a>
        <a href="#php-bll-floor" class="tile hover _14x1">
            <span class="icon-arrow-right"></span>
            <span class="screen-reader-text">Next</span>
        </a>
        <a href="#php-floor" class="tile _14x1">
            <span class="icon-menu2"></span>
            <span class="screen-reader-text">Home</span>
        </a>
        <h1>an<span>orm</span>apart</h1>
    </div>
    <div class="room">
        <div class="tile hover">
            <a href="#catalog-floor" class="mask fade-in-left">
                <h2>Catalogs</h2>
                <p>Select another catalog or create a new one based on a catalog template.</p>
                <span class="action">Open</span>
            </a>
            <h1>Catalogs</h1>
            <p>A list of catalogs</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-dal-base-class">
                <h2>DAL base class</h2>
                <p>Generates the base class for table DAL classes using PDO.</p>
                <span class="action">DO</span>
            </a>
            <h1>DAL</h1>
            <p>base class</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-test-dal-base-class">
                <h2>DAL test base class</h2>
                <p>Generates code to test the DAL base class.</p>
                <span class="action">DO</span>
            </a>
            <h1>DAL</h1>
            <p>test base class</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-dal-class">
                <h2>DAL class(es)</h2>
                <p>Generates the php DAL class(es) for the selected catalog using PDO.</p>
                <span class="action">DO</span>
            </a>
            <h1>DAL</h1>
            <p>class(es)</p>
        </div>
        <div class="tile">
        </div>
         <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-dal-anormapart-class">
                <h2>DAL codebehind</h2>
                <p>Generates the php DAL codebehind class(es) for the selected catalog using PDO.</p>
                <span class="action">DO</span>
            </a>
            <h1>DAL</h1>
            <p>codebehind class(es)</p>
        </div>
       <div class="tile hover">
            <a href="#seventh-floor" class="mask fade-in-left" id="php-test-dal-class">
                <h2>DAL test class</h2>
                <p>Generates code to test the DAL classes.</p>
                <span class="action">Kies</span>
            </a>
            <h1>DAL</h1>
            <p>test</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-connection-class">
                <h2>DAL connection class</h2>
                <p>Generates the PHP connection class using PDO.</p>
                <span class="action">DO</span>
            </a>
            <h1>DAL</h1>
            <p>connection class</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-test-connection-class">
                <h2>DAL connection class</h2>
                <p>Generates PHP code test and learn how to use the connection class using PDO.</p>
                <span class="action">DO</span>
            </a>
            <h1>DAL</h1>
            <p>test connection class</p>
        </div>
        <div class="tile hover">
            <a href="#" class="mask fade-in-left" id="php-provider-class">
                <h2>DAL Provider class</h2>
                <p>Generates PHP a sample class that inherits from the connection class. Sets parameters connectionstring.</p>
                <span class="action">DO</span>
            </a>
            <h1>DAL</h1>
            <p>Provider class</p>
        </div>
        <div class="tile">
        </div>
    </div>
</div>