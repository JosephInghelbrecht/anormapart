<!-- anormapart.home-view   1/02/2015 JI -->
<div class="floor" id="home-floor">
    <div class="control-panel">
        <a href="http://www.inantwerpen.com" class="tile hover _14x1">
            <span class="icon-arrow-left"></span>
            <span class="screen-reader-text">Back</span>
        </a>
        <a href="#php-floor" class="tile hover _14x1">
            <span class="icon-arrow-right"></span>
            <span class="screen-reader-text">Next</span>
        </a>
        <h1>an<span>orm</span>apart</h1>
    </div>
    <div class="room">
        <div class="tile hover">
            <a href="#catalog-floor" class="mask fade-in-left">
                <h2>Catalogs</h2>
                <p>Select another catalog or create a new one based on a catalog template.</p>
                <span class="action">Kies</span>
            </a>
            <h1>Catalogs</h1>
            <p>A list of catalogs</p>
        </div>
        <div class="tile hover">
            <a href="#php-floor" class="mask fade-in-left">
                <h2>PHP</h2>
                <p>produceert code en alleen code, je moet geen framework leren, gewoon
                    verder gaan met wat je van programmeren al kent</p>
                <span class="action">Lees meer...</span>
            </a>
            <h1>PHP</h1>
            <p>Helpers, Back-end, MVC en SQL</p>
        </div>
        <div class="tile hover">
            <a href="#sql-floor" class="mask fade-in-left">
                <h2>SQL scripts</h2>
                <p>Generates DDL and DML scripts for MySQL and MsSQL.</p>
                <span class="action">Select</span>
            </a>
            <h1>SQL</h1>
            <p>scripts</p>
        </div>
        <div class="tile">
        </div>
        <div class="tile hover">
            <a href="#template-floor" class="mask fade-in-left">
                <h2>An Orm Apart Template</h2>
                <p>A zip file with complete directory structure and sql, dal, bll, mvc and helpers files.</p>
                <span class="action">Select</span>
            </a>
            <h1>Templates</h1>
            <p>A list of zip files</p>
        </div>
        <div class="tile hover">
            <a href="#csharp-mvc-floor" class="mask fade-in-left">
                <h2>C#</h2>
                <p>produceert code en alleen code, je moet geen framework leren, gewoon
                    verder gaan met wat je van programmeren al kent</p>
                <span class="action">Lees meer...</span>
            </a>
            <h1>C#</h1>
            <p>Helpers, Back-end, MVC en SQL</p>
        </div>
        <div class="tile">
        </div>
        <div class="tile">
        </div>
    </div>
</div>