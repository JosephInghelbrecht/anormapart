﻿-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE MemberRole
-- Created on Friday 13th of February 2015 10:38:19 AM
-- 
USE `Membership`;
DROP TABLE IF EXISTS `MemberRole`;
CREATE TABLE `MemberRole` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	`IdMember` INT NOT NULL,
	`IdRole` INT NOT NULL,
	`InsertedBy` NVARCHAR (255) NULL,
	`InsertedOn` TIMESTAMP NULL,
	`UpdatedBy` NVARCHAR (255) NULL,
	`UpdatedOn` TIMESTAMP NULL,
	CONSTRAINT fk_MemberRoleIdMember FOREIGN KEY (`IdMember`) REFERENCES `Member` (`Id`),
	CONSTRAINT fk_MemberRoleIdRole FOREIGN KEY (`IdRole`) REFERENCES `Role` (`Id`));