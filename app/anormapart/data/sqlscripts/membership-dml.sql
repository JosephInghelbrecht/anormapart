﻿-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Friday 13th of February 2015 10:43:35 AM
-- DML Insert Stored Procedure for Member 
-- 
USE Membership;
DROP PROCEDURE IF EXISTS MemberInsert;
DELIMITER //
CREATE PROCEDURE `MemberInsert`
(
	OUT pId INT ,
	IN pUserName NVARCHAR (50) ,
	IN pEmail VARCHAR (80) ,
	IN pPassword CHAR (128) ,
	IN pSalt CHAR (128) ,
	IN pAuthenticated BIT ,
	IN pInsertedBy NVARCHAR (255) 
)
BEGIN
INSERT INTO `Member`
	(
		`Member`.`UserName`,
		`Member`.`Email`,
		`Member`.`Password`,
		`Member`.`Salt`,
		`Member`.`FirstLogin`,
		`Member`.`Authenticated`,
		`Member`.`InsertedBy`,
		`Member`.`InsertedOn`
	)
	VALUES
	(
		pUserName,
		pEmail,
		pPassword,
		pSalt,
		NOW(),
		pAuthenticated,
		pInsertedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;
