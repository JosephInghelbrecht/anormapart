<?php
	include ('../../../../helpers/feedback.class.php');
	include ('../../../../helpers/log.class.php');
	// include connection class
	include ('../../../../helpers/connection.class.php');
	// Include the connection class for the anormapart app
	include ('../../helpers/anormapart-connection.class.php');
	include ('../../../../helpers/base-bll.class.php');
	include ('../../../../helpers/base-dal.class.php');
	// include bll class for table
	include ('../bll/anormapart-membership-bll-codebehind.class.php');
	include ('../bll/anormapart-membership-bll.class.php');
	// include dal class for table
	include ('anormapart-membership-dal-codebehind.class.php');
	include ('anormapart-membership-dal.class.php');
	// only required when there is a password property
	// include ('../../../../helpers/password.php');
	$log = new AnOrmApart\Helpers\Log();
	// connect using the AnOrmApart Connection class
	$connection = new AnOrmApart\Dal\AnOrmApart\Connection($log);
	$connection->open();
	// create an instance of the DAL class for this table
	$dal = new AnOrmApart\Membership\LoginAttempt\Dal($log);
	// create an instance of the Bll class for this table
	$bll = new AnOrmApart\Membership\LoginAttempt\Bll($log);
	// we start with making a business object
	$bll->setId(999);
	$bll->setIdMember(999);
	$bll->setTime('Time');
	// insert
	// pass the business dataobject to the DAL class
	$dal->setBdo($bll);
	// pass the connection object to the DAL class
	$dal->setConnection($connection);
	// and now it's time to insert
	$dal->insert();

	// de laatste insert zette de nieuw Id in
	// het id veld van het BLL object
	$dal->selectOne();

	$list = $dal->selectAll();

	// update section
	// first change bdo
	// now update
	// No need to pass bdo object again to dal
	// the bdo instance of the dal still
	// references the $bbl here.
	$dal->update();
	// delete row with Id set in bll object
	$dal->delete();
	// set Id to not existant number
	$bll->setId(-1);
	$dal->delete();
	// disconnect
	$connection->close();


?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Test BLL/DAL</title>
	</head>
	<body>
		<?php
		if (count($log->getBook()) > 0)
		{
			foreach ($log->getBook() as $key => $feedback)
			{?>
			<h3><?php echo $key;?></h3>
			<h4><?php echo $feedback->getName();?></h4>
			<div><label>Feedback</label><span><?php echo $feedback->getText();?></span></div>
			<div><label>Error code</label><span><?php echo $feedback->getErrorCode();?></span></div>
			<div><label>Error message</label><span><?php echo $feedback->getErrorMessage();?></span></div>
			<div><label>Error Code Driver</label><span><?php echo $feedback->getErrorCodeDriver();?></span></div>
			<div><label>Is error</label><span><?php echo $feedback->getIsError();?></span></div>
			<div><label>Start</label><span><?php echo $feedback->getStartTime();?></span></div>
			<div><label>End</label><span><?php echo $feedback->getEndTime();?></span></div>
			<?php
			}
		}
		else
		{?>
			<h1>No errors</h1>
		<?php
		}?>
		<fieldset>
			<legend>LoginAttempt</legend>
			<div>
				<label>NA</label>
				<span><?php echo $bll->getId();?></span>
			</div>
			<div>
				<label>NA</label>
				<span><?php echo $bll->getIdMember();?></span>
			</div>
			<div>
				<label>Time</label>
				<span><?php echo $bll->getTime();?></span>
			</div>
		</fieldset>
		<?php
		if (count($list) > 0)
		{
		?>
		<table>
			<caption>LoginAttempt</caption>
			<tr>
				<th>Time</th>
			</tr>
			<?php
			foreach ($list as $row)
			{
			?>
			<tr>
				<td><?php echo $row['Time'];?></td>
			</tr>
			<?php
			}
			?>
		</table>
		<?php
		}
		else
		{?>
			<h1>Lege lijst</h1>
		<?php
		}
		?>
	</body>
</html>

<?php
	include ('../../../../helpers/feedback.class.php');
	include ('../../../../helpers/log.class.php');
	// include connection class
	include ('../../../../helpers/connection.class.php');
	// Include the connection class for the anormapart app
	include ('../../helpers/anormapart-connection.class.php');
	include ('../../../../helpers/base-bll.class.php');
	include ('../../../../helpers/base-dal.class.php');
	// include bll class for table
	include ('../bll/anormapart-membership-bll-codebehind.class.php');
	include ('../bll/anormapart-membership-bll.class.php');
	// include dal class for table
	include ('anormapart-membership-dal-codebehind.class.php');
	include ('anormapart-membership-dal.class.php');
	// only required when there is a password property
	// include ('../../../../helpers/password.php');
	$log = new AnOrmApart\Helpers\Log();
	// connect using the AnOrmApart Connection class
	$connection = new AnOrmApart\Dal\AnOrmApart\Connection($log);
	$connection->open();
	// create an instance of the DAL class for this table
	$dal = new AnOrmApart\Membership\Member\Dal($log);
	// create an instance of the Bll class for this table
	$bll = new AnOrmApart\Membership\Member\Bll($log);
	// we start with making a business object
	$bll->setId(999);
	$bll->setUserName('UserName');
	$bll->setEmail('Email');
	$bll->hashPassword('password');
	$bll->setSalt('Salt');
	$bll->setLastActivity();
	$bll->setFirstLogin();
	$bll->setAuthenticated(TRUE);
	// insert
	// pass the business dataobject to the DAL class
	$dal->setBdo($bll);
	// pass the connection object to the DAL class
	$dal->setConnection($connection);
	// and now it's time to insert
	$dal->insert();

	// de laatste insert zette de nieuw Id in
	// het id veld van het BLL object
	$dal->selectOne();

	$list = $dal->selectAll();

	// update section
	// first change bdo
	// now update
	// No need to pass bdo object again to dal
	// the bdo instance of the dal still
	// references the $bbl here.
	$dal->update();
	// delete row with Id set in bll object
	$dal->delete();
	// set Id to not existant number
	$bll->setId(-1);
	$dal->delete();
	// disconnect
	$connection->close();


?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Test BLL/DAL</title>
	</head>
	<body>
		<?php
		if (count($log->getBook()) > 0)
		{
			foreach ($log->getBook() as $key => $feedback)
			{?>
			<h3><?php echo $key;?></h3>
			<h4><?php echo $feedback->getName();?></h4>
			<div><label>Feedback</label><span><?php echo $feedback->getText();?></span></div>
			<div><label>Error code</label><span><?php echo $feedback->getErrorCode();?></span></div>
			<div><label>Error message</label><span><?php echo $feedback->getErrorMessage();?></span></div>
			<div><label>Error Code Driver</label><span><?php echo $feedback->getErrorCodeDriver();?></span></div>
			<div><label>Is error</label><span><?php echo $feedback->getIsError();?></span></div>
			<div><label>Start</label><span><?php echo $feedback->getStartTime();?></span></div>
			<div><label>End</label><span><?php echo $feedback->getEndTime();?></span></div>
			<?php
			}
		}
		else
		{?>
			<h1>No errors</h1>
		<?php
		}?>
		<fieldset>
			<legend>Member</legend>
			<div>
				<label>NA</label>
				<span><?php echo $bll->getId();?></span>
			</div>
			<div>
				<label>Gebruikersnaam</label>
				<span><?php echo $bll->getUserName();?></span>
			</div>
			<div>
				<label>Email</label>
				<span><?php echo $bll->getEmail();?></span>
			</div>
			<div>
				<label>Wachtwoord</label>
				<span><?php echo $bll->getPassword();?></span>
			</div>
			<div>
				<label>Zout</label>
				<span><?php echo $bll->getSalt();?></span>
			</div>
			<div>
				<label>Laatst actief</label>
				<span><?php echo $bll->getLastActivity();?></span>
			</div>
			<div>
				<label>Eerste aanmelding</label>
				<span><?php echo $bll->getFirstLogin();?></span>
			</div>
			<div>
				<label>Geverifieerd</label>
				<span><?php echo $bll->getAuthenticated();?></span>
			</div>
		</fieldset>
		<?php
		if (count($list) > 0)
		{
		?>
		<table>
			<caption>Member</caption>
			<tr>
				<th>NA</th>
				<th>Gebruikersnaam</th>
				<th>Email</th>
			</tr>
			<?php
			foreach ($list as $row)
			{
			?>
			<tr>
				<td><?php echo $row['Id'];?></td>
				<td><?php echo $row['UserName'];?></td>
				<td><?php echo $row['Email'];?></td>
			</tr>
			<?php
			}
			?>
		</table>
		<?php
		}
		else
		{?>
			<h1>Lege lijst</h1>
		<?php
		}
		?>
	</body>
</html>

<?php
	include ('../../../../helpers/feedback.class.php');
	include ('../../../../helpers/log.class.php');
	// include connection class
	include ('../../../../helpers/connection.class.php');
	// Include the connection class for the anormapart app
	include ('../../helpers/anormapart-connection.class.php');
	include ('../../../../helpers/base-bll.class.php');
	include ('../../../../helpers/base-dal.class.php');
	// include bll class for table
	include ('../bll/anormapart-membership-bll-codebehind.class.php');
	include ('../bll/anormapart-membership-bll.class.php');
	// include dal class for table
	include ('anormapart-membership-dal-codebehind.class.php');
	include ('anormapart-membership-dal.class.php');
	// only required when there is a password property
	// include ('../../../../helpers/password.php');
	$log = new AnOrmApart\Helpers\Log();
	// connect using the AnOrmApart Connection class
	$connection = new AnOrmApart\Dal\AnOrmApart\Connection($log);
	$connection->open();
	// create an instance of the DAL class for this table
	$dal = new AnOrmApart\Membership\Role\Dal($log);
	// create an instance of the Bll class for this table
	$bll = new AnOrmApart\Membership\Role\Bll($log);
	// we start with making a business object
	$bll->setId(999);
	$bll->setName('Name');
	$bll->setDescription('Description');
	// insert
	// pass the business dataobject to the DAL class
	$dal->setBdo($bll);
	// pass the connection object to the DAL class
	$dal->setConnection($connection);
	// and now it's time to insert
	$dal->insert();

	// de laatste insert zette de nieuw Id in
	// het id veld van het BLL object
	$dal->selectOne();

	$list = $dal->selectAll();

	// update section
	// first change bdo
	// now update
	// No need to pass bdo object again to dal
	// the bdo instance of the dal still
	// references the $bbl here.
	$dal->update();
	// delete row with Id set in bll object
	$dal->delete();
	// set Id to not existant number
	$bll->setId(-1);
	$dal->delete();
	// disconnect
	$connection->close();


?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Test BLL/DAL</title>
	</head>
	<body>
		<?php
		if (count($log->getBook()) > 0)
		{
			foreach ($log->getBook() as $key => $feedback)
			{?>
			<h3><?php echo $key;?></h3>
			<h4><?php echo $feedback->getName();?></h4>
			<div><label>Feedback</label><span><?php echo $feedback->getText();?></span></div>
			<div><label>Error code</label><span><?php echo $feedback->getErrorCode();?></span></div>
			<div><label>Error message</label><span><?php echo $feedback->getErrorMessage();?></span></div>
			<div><label>Error Code Driver</label><span><?php echo $feedback->getErrorCodeDriver();?></span></div>
			<div><label>Is error</label><span><?php echo $feedback->getIsError();?></span></div>
			<div><label>Start</label><span><?php echo $feedback->getStartTime();?></span></div>
			<div><label>End</label><span><?php echo $feedback->getEndTime();?></span></div>
			<?php
			}
		}
		else
		{?>
			<h1>No errors</h1>
		<?php
		}?>
		<fieldset>
			<legend>Role</legend>
			<div>
				<label>NA</label>
				<span><?php echo $bll->getId();?></span>
			</div>
			<div>
				<label>Naam</label>
				<span><?php echo $bll->getName();?></span>
			</div>
			<div>
				<label>Beschrijving</label>
				<span><?php echo $bll->getDescription();?></span>
			</div>
		</fieldset>
		<?php
		if (count($list) > 0)
		{
		?>
		<table>
			<caption>Role</caption>
			<tr>
				<th>Naam</th>
			</tr>
			<?php
			foreach ($list as $row)
			{
			?>
			<tr>
				<td><?php echo $row['Name'];?></td>
			</tr>
			<?php
			}
			?>
		</table>
		<?php
		}
		else
		{?>
			<h1>Lege lijst</h1>
		<?php
		}
		?>
	</body>
</html>

<?php
	include ('../../../../helpers/feedback.class.php');
	include ('../../../../helpers/log.class.php');
	// include connection class
	include ('../../../../helpers/connection.class.php');
	// Include the connection class for the anormapart app
	include ('../../helpers/anormapart-connection.class.php');
	include ('../../../../helpers/base-bll.class.php');
	include ('../../../../helpers/base-dal.class.php');
	// include bll class for table
	include ('../bll/anormapart-membership-bll-codebehind.class.php');
	include ('../bll/anormapart-membership-bll.class.php');
	// include dal class for table
	include ('anormapart-membership-dal-codebehind.class.php');
	include ('anormapart-membership-dal.class.php');
	// only required when there is a password property
	// include ('../../../../helpers/password.php');
	$log = new AnOrmApart\Helpers\Log();
	// connect using the AnOrmApart Connection class
	$connection = new AnOrmApart\Dal\AnOrmApart\Connection($log);
	$connection->open();
	// create an instance of the DAL class for this table
	$dal = new AnOrmApart\Membership\MemberRole\Dal($log);
	// create an instance of the Bll class for this table
	$bll = new AnOrmApart\Membership\MemberRole\Bll($log);
	// we start with making a business object
	$bll->setId(999);
	$bll->setIdMember(999);
	$bll->setIdRole(999);
	// insert
	// pass the business dataobject to the DAL class
	$dal->setBdo($bll);
	// pass the connection object to the DAL class
	$dal->setConnection($connection);
	// and now it's time to insert
	$dal->insert();

	// de laatste insert zette de nieuw Id in
	// het id veld van het BLL object
	$dal->selectOne();

	$list = $dal->selectAll();

	// update section
	// first change bdo
	// now update
	// No need to pass bdo object again to dal
	// the bdo instance of the dal still
	// references the $bbl here.
	$dal->update();
	// delete row with Id set in bll object
	$dal->delete();
	// set Id to not existant number
	$bll->setId(-1);
	$dal->delete();
	// disconnect
	$connection->close();


?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Test BLL/DAL</title>
	</head>
	<body>
		<?php
		if (count($log->getBook()) > 0)
		{
			foreach ($log->getBook() as $key => $feedback)
			{?>
			<h3><?php echo $key;?></h3>
			<h4><?php echo $feedback->getName();?></h4>
			<div><label>Feedback</label><span><?php echo $feedback->getText();?></span></div>
			<div><label>Error code</label><span><?php echo $feedback->getErrorCode();?></span></div>
			<div><label>Error message</label><span><?php echo $feedback->getErrorMessage();?></span></div>
			<div><label>Error Code Driver</label><span><?php echo $feedback->getErrorCodeDriver();?></span></div>
			<div><label>Is error</label><span><?php echo $feedback->getIsError();?></span></div>
			<div><label>Start</label><span><?php echo $feedback->getStartTime();?></span></div>
			<div><label>End</label><span><?php echo $feedback->getEndTime();?></span></div>
			<?php
			}
		}
		else
		{?>
			<h1>No errors</h1>
		<?php
		}?>
		<fieldset>
			<legend>MemberRole</legend>
			<div>
				<label>NA</label>
				<span><?php echo $bll->getId();?></span>
			</div>
			<div>
				<label>Id Member</label>
				<span><?php echo $bll->getIdMember();?></span>
			</div>
			<div>
				<label>Id Role</label>
				<span><?php echo $bll->getIdRole();?></span>
			</div>
		</fieldset>
		<?php
		if (count($list) > 0)
		{
		?>
		<table>
			<caption>MemberRole</caption>
			<tr>
				<th>Id Member</th>
				<th>Id Role</th>
			</tr>
			<?php
			foreach ($list as $row)
			{
			?>
			<tr>
				<td><?php echo $row['IdMember'];?></td>
				<td><?php echo $row['IdRole'];?></td>
			</tr>
			<?php
			}
			?>
		</table>
		<?php
		}
		else
		{?>
			<h1>Lege lijst</h1>
		<?php
		}
		?>
	</body>
</html>


