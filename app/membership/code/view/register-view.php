<!-- anormapart.com register floor   19/02/2015 JI -->
<div class="floor" id="register">
    <div class="control-panel">
        <a href="#" class="tile hover _14x1" onclick="goBackTo(); return false;">
            <span class="icon-arrow-left"></span>
            <span class="screen-reader-text">Back</span>
        </a>
        <a href="#" class="tile hover _14x1" onclick="goBackTo(); return false;">
            <span class="icon-arrow-right"></span>
            <span class="screen-reader-text">Next</span>
        </a>
        <a href="#home-floor" class="tile _14x1">
            <span class="icon-menu2"></span>
            <span class="screen-reader-text">Home</span>
        </a>
        <h1>an<span>orm</span>apart</h1>
    </div>
    <div class="room">
        <div class="form">
            <h1>Registreren</h1>
            <div>
                <label for="usernameregister" class="icon-user">Gebruikersnaam</label>
                <input id="usernameregister" name="usernameregister" required="required" type="text" />
            </div>
            <div>
                <label for="emailregister" class="icon-mail">Email</label>
                <input id="emailregister" name="emailregister" required="required" type="email" placeholder="mysupermail@mail.com" />
            </div>
            <div>
                <label for="passwordregister" class="icon-key">Paswoord</label>
                <input id="passwordregister" name="passwordregister" required="required" type="password" placeholder="eg. X8df!90EO" />
            </div>
            <div>
                <label for="passwordregister-confirm" class="icon-key">Paswoord bevestigen</label>
                <input id="passwordregister-confirm" name="passwordregister-confirm" required="required" type="password" placeholder="eg. X8df!90EO" />
            </div>
            <div class="register button">
                <button id="registerSubmit" type="submit" value="register" name="action">Registreren</button>
            </div>
        </div>
        <div class="feedback">Al geregistreerd?
            <a href="#to-login">Ga naar aanmelden.</a>
        </div>
    </div>
</div>


