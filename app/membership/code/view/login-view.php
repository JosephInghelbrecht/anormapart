<!-- anormapart.com login floor   19/02/2015 JI -->
<div class="floor" id="login">
    <div class="control-panel">
        <a href="#" class="tile hover _14x1" onclick="goBackTo(); return false;">
            <span class="icon-arrow-left"></span>
            <span class="screen-reader-text">Back</span>
        </a>
        <a href="#" class="tile hover _14x1" onclick="goBackTo(); return false;">
            <span class="icon-arrow-right"></span>
            <span class="screen-reader-text">Next</span>
        </a>
        <a href="#home-floor" class="tile _14x1">
            <span class="icon-menu2"></span>
            <span class="screen-reader-text">Home</span>
        </a>
        <h1>an<span>orm</span>apart</h1>
    </div>
    <div class="room form-container">
        <div class="form">
            <h1>Aanmelden</h1>
            <div>
                <label for="username" class="icon-user">Gebruikernaam of e-mail</label>
                <input id="username" name="username" required="required" type="text" placeholder="gebruikersnaam of mymail@mail.be" />
            </div>
            <div>
                <label for="password" class="icon-key">Wachtwoord</label>
                <input id="password" name="password" required="required" type="password" placeholder="eg. X8df!90EO" />
            </div>
            <div>
                <input type="checkbox" name="loginkeeping" id="loginkeeping" value="loginkeeping" />
                <label class="checkbox" for="loginkeeping">aangemeld blijven</label>
                <button type="submit" value="login" name="action">Aanmelden</button>
            </div>
        </div>
        <div class="feedback">Nog niet geregistreerd?
            <a href="#register">Ga naar registreren.</a>
        </div>
    </div>
</div>
