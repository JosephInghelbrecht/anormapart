<?php
    // use here include_one, could already be done in app coordinating controller
    include_once ('../../../helpers/base-bll.class.php');
    include_once ('../../../helpers/base-dal.class.php');
    include_once ('../../../helpers/base-model.class.php');
    include_once ('../../../helpers/connection.class.php');
    include_once ('../../../helpers/feedback.class.php');
    include_once ('../../../helpers/log.class.php');
    include_once ('../../../helpers/session.class.php');

    // parameters for Membership connection
    include_once ('../helpers/anormapart-connection.class.php');
    include ('../bll/anormapart-membership-bll-codebehind.class.php');
    include ('../bll/anormapart-membership-bll.class.php');
    include ('../dal/anormapart-membership-dal-codebehind.class.php');
    include ('../dal/anormapart-membership-dal.class.php');
    include ('../model/anormapart-membership-model-codebehind.class.php');
    include ('../model/anormapart-membership-model.class.php');

    $action = $_POST['anormapart-action'];
    // echo $action;
    switch($action)  
    {    
        case 'login' :
            if(isset($_POST['username'], $_POST['password'])) 
            { 
                $userName = $_POST['username'];
                $password = $_POST['password'];
                // create a logbook object if there is not already one, we always need one
                if (!isset($log))
                {
                    $log = new \AnormApart\Helpers\Log();
                }
                // is there a connection object available
                if (!isset($connectionMembership))
                {
                    $connectionMembership = new \AnOrmApart\Dal\OVH\Connection($log);
                    $connectionMembership->open(); 
                }
                $membershipModel = new \AnOrmApart\Membership\Model($log, $connectionMembership);
                $membershipModel->login($userName, $password);
            }
            break;
        case 'register' :
            break;
    }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title></title>
    </head>
    <body>
        <a href="<?php echo (isset($membershipModel) ? ($membershipModel->isLoggedIn() ? '#' : '#to-login') : '#to-login');?>">
            <?php echo (isset($membershipModel) ? ($membershipModel->isLoggedIn() ? 'Afmelden' : 'Aanmelden') : 'Aanmelden');?>
        </a>
        <?php
            foreach ($log->getBook() as $key => $feedback)
            {
        ?>
        <h1><?php echo $key;?></h1>
        <p><b>Name</b> <?php echo $feedback->getName();?></p>
        <p><b>Feedback</b> <?php echo $feedback->getText();?></p>
        <p><b>Error code</b> <?php echo $feedback->getErrorCode();?></p>
        <p><b>Error message</b> <?php echo $feedback->getErrorMessage();?></p>
        <p><b>Error Code Driver</b> <?php echo $feedback->getErrorCodeDriver();?></p>
        <p><b>Is error</b> <?php echo $feedback->getIsError();?></p>
        <p><b>Start</b> <?php echo $feedback->getStartTime();?></p>
        <p><b>End</b> <?php echo $feedback->getEndTime();?></p>
        <?php
            }
        ?>
    </body>
</html>
