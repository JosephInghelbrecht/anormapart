<?php
/* inantwerpen.com
 * created by an orm apart
 * Entreprise de modes et de maniÃ¨res modernes
 * Dal for Membership app
 * Created on Monday 9th of February 2015 04:15:26 PM
*/ 
// Write here your own code to change the
// standard behaviour of the code generated
namespace AnOrmApart\Membership\LoginAttempt;
class Dal extends \AnOrmApart\Membership\LoginAttempt\CodeBehind\Dal
{
	public function count()
	{
		$this->log->startTimeInKey('count');
		$result = FALSE;
        $this->rowCount = 0;
		if (!$this->bdo->isValid())
		{
			$this->log->setText('Ongeldige gegevens. Kan niet tellen.');
			$this->log->setErrorMessage('zie BLL feedback voor details');
			$this->log->setErrorCodeDriver('none');
			$this->log->setErrorCode('DAL COUNT LoginAttempt');
			$this->log->setContext('LoginAttempt');
		}
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare('CALL LoginAttemptCount(:pIdMember, :pTime)');
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then binndValue
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pIdMember', $this->bdo->getIdMember(), \PDO::PARAM_INT);
				$preparedStatement->bindValue(':pTime', $this->bdo->getTime(), \PDO::PARAM_STR);
				// Returns TRUE on success or FALSE on failure
                $preparedStatement->execute();
                $result = $preparedStatement->fetch(\PDO::FETCH_NUM);
                $this->rowCount = $result[0];
   			    $this->log->setText("Aantal progingen sinds {$this->bdo->getTime()} voor {$this->bdo->getIdMember()}</b>.");

				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->log->setErrorCode($sQLErrorInfo[0]);
				$this->log->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->log->setErrorMessage($sQLErrorInfo[2]);
				$this->log->setContext('LoginAttempt');
			}
			catch (\PDOException $e)
			{
				$this->log->setText('Rij is niet geUpdated.');
				$this->log->setErrorMessage($e->getMessage());
				$this->log->setErrorCodeDriver($e->getCode());
				$this->log->setErrorCode('DAL LoginAttemptUpdate');
				$this->log->setContext('LoginAttempt');
			}
		}
		else
		{
			$this->log->setText('Niet geteld.');
			$this->log->setErrorMessage('Not connected to LoginAttempt');
			$this->log->setErrorCodeDriver('none');
			$this->log->setErrorCode('DAL COUNT LoginAttempt');
			$this->log->setContext('LoginAttempt');
		}
		$this->log->log();
		return  $this->rowCount;
	}
}

namespace AnOrmApart\Membership\Member;
class Dal extends \AnOrmApart\Membership\Member\CodeBehind\Dal
{
	public function updateLockedOut()
	{
		$this->log->startTimeInKey('updateLockedOut');
		$result = FALSE;
		if (!$this->bdo->isValid())
		{
			$this->log->setText('Ongeldige gegevens. Kan niet updaten.');
			$this->log->setErrorMessage('zie BLL feedback voor details');
			$this->log->setErrorCodeDriver('none');
			$this->log->setErrorCode('DAL updateLockedOut Member');
			$this->log->setContext('Member');
		}
		if ($this->connection->isConnected())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->connection->getPdo()->
				prepare('CALL MemberUpdateLockedOut(:pUserName, :pLockedOut, :pUpdatedBy)');
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then binndValue
				// so we cannot use bindParam that requires a variable by value
				// if you want to use a variable, use then bindParam
				$preparedStatement->bindValue(':pUserName', $this->bdo->getUserName(), \PDO::PARAM_STR);
				$preparedStatement->bindValue(':pLockedOut', $this->bdo->getLockedOut(), \PDO::PARAM_BOOL);
				$preparedStatement->bindValue(':pUpdatedBy', $this->bdo->getUpdatedBy(), \PDO::PARAM_STR);
				// Returns TRUE on success or FALSE on failure
				$result = $preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result)
				{
					if ($this->rowCount > 0)
					{
						// De rij met de opgegeven Id is geüpdated.
						// Dat leiden we af uit het feit dat er geen
						// foutmelding werd geretourneerd maar dat
						// het aantal geaffecteerde rijen groter dan 0 is.
						$this->log->setText('Rij met gebruiker <b> ' . $this->bdo->getUserName() . '</b> is geüpdated.');
						$result = TRUE;
					}
					else
					{
						// De rij met de opgegeven Id bestaat niet.
						// Dat leiden we af uit het feit dat er geen
						// foutmelding werd geretourneerd maar dat
						// het aantal geaffecteerde rijen gelijk is aan 0.
						$this->log->setText('Rij met gebruiker <b> ' . $this->bdo->getUserName() . '</b> bestaat niet en dus niet geüpdated.');
						$result = FALSE;
					}
				}
				else
				{
					$this->log->setText('Fout in stored procedure MemberUpdateLockedOut(). Rij is niet geüpdated.');
				}
				$sQLErrorInfo = $preparedStatement->errorInfo();
				$this->log->setErrorCode($sQLErrorInfo[0]);
				$this->log->setErrorCodeDriver($sQLErrorInfo[1]);
				$this->log->setErrorMessage($sQLErrorInfo[2]);
				$this->log->setContext('Member');
			}
			catch (\PDOException $e)
			{
				$this->log->setText('Rij is niet geUpdated.');
				$this->log->setErrorMessage($e->getMessage());
				$this->log->setErrorCodeDriver($e->getCode());
				$this->log->setErrorCode('DAL MemberUpdateLockedOut');
				$this->log->setContext('Member');
			}
		}
		else
		{
			$this->log->setText('Rij is niet geüpdated.');
			$this->log->setErrorMessage('Not connected to Member');
			$this->log->setErrorCodeDriver('none');
			$this->log->setErrorCode('DAL MemberUpdateLockedOut');
			$this->log->setContext('Member');
		}
		$this->log->log();
		return $result['Count'];
	}
}

namespace AnOrmApart\Membership\Role;
class Dal extends \AnOrmApart\Membership\Role\CodeBehind\Dal
{
}

namespace AnOrmApart\Membership\MemberRole;
class Dal extends \AnOrmApart\Membership\MemberRole\CodeBehind\Dal
{
}

?>

