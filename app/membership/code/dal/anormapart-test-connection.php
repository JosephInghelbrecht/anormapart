<?php
	include ('../../../helpers/feedback.class.php');
	include ('../../../helpers/log.class.php');
    include ('../../../helpers/connection.class.php');
    include ('anormapart-connection.class.php');
    // maak een logboek
    $log = new \AnormApart\Helpers\Log();
    $connection = new \AnOrmApart\Dal\OVH\Connection($log);
    $connection->open();
    $connection->close();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Connection class test</title>
    </head>
    <body>
        <?php
            foreach ($log->getBook() as $key => $feedback)
            {
        ?>
        <h1><?php echo $key;?></h1>
        <p><b>Name</b> <?php echo $feedback->getName();?></p>
        <p><b>Feedback</b> <?php echo $feedback->getText();?></p>
        <p><b>Error code</b> <?php echo $feedback->getErrorCode();?></p>
        <p><b>Error message</b> <?php echo $feedback->getErrorMessage();?></p>
        <p><b>Error Code Driver</b> <?php echo $feedback->getErrorCodeDriver();?></p>
        <p><b>Is error</b> <?php echo $feedback->getIsError();?></p>
        <p><b>Start</b> <?php echo $feedback->getStartTime();?></p>
        <p><b>End</b> <?php echo $feedback->getEndTime();?></p>
        <?php
            }
        ?>
    </body>
</html>

