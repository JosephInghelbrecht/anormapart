<?php
	/* inantwerpen.com
	 * created by an orm apart
	 * Entreprise de modes et de manières modernes
	 * Model for Membership app
	 * Created on Tuesday 17th of February 2015 02:08:54 PM
	*/ 
	// Write here your own code to change the
	// standard behaviour of the code generated

	namespace AnOrmApart\Membership;
	class Model
	{
        private $authenticated;
        private $lockedOut;
        private $member;
        private $passwordVerified;
        private $userName;
        private $email;
        private $log;
	    protected $connection;
        protected $session;

        public function __construct($log, $connection)
        {
            $this->authenticated = FALSE;
            $this->lockedOut = TRUE;
            $this->member = FALSE;
            $this->passwordVerified = FALSE;
            $this->log = $log;
            $this->connection = $connection;
            $this->session = new \AnOrmApart\Session();
        }

	    public function getLog()
	    {
		    return $this->log;
	    }

        public function isAuthenticated()
        {
            return $this->authenticated;
        }

        public function isLockedOut()
        {
            return $this->lockedOut;
        }

        public function isMember()
        {
            return $this->member;
        }

        public function isPasswordVerified()
        {
            return $this->passwordVerified;
        }

        public function isLoggedIn()
        {
            $userId = $this->session->get('x');
            // haal password op
            $dal = new \AnOrmApart\Membership\Member\Dal($this->log);
            $bll = new \AnOrmApart\Membership\Member\Bll($this->log);
            $modelMember = new \AnOrmApart\Membership\Member\Model($this->log, $this->connection, $dal, $bll);
            $modelMember->memberReadOne($userId);
            $result = $this->session->isValidToken($modelMember->getBdo()->getPassword());
            return $result;           
        }


        public function login($userName, $password)
        {
            $dal = new \AnOrmApart\Membership\Member\Dal($this->log);
            $bll = new \AnOrmApart\Membership\Member\Bll($this->log);
            $modelMember = new \AnOrmApart\Membership\Member\Model($this->log, $this->connection, $dal, $bll);
            $dal = new \AnOrmApart\Membership\LoginAttempt\Dal($this->log);
            $bll = new \AnOrmApart\Membership\LoginAttempt\Bll($this->log);
            $modelLoginAttempt = new \AnOrmApart\Membership\LoginAttempt\Model($this->log, $this->connection, $dal, $bll);
                
            // Does user exist?
            if ($modelMember->isMember($userName))
            {
                $this->member = TRUE;
                if ($modelMember->isAuthenticated())
                {
                    $this->authenticated = TRUE;
                    if (!$modelMember->isLockedOut())
                    {
                        $this->lockedOut = FALSE;
                        if ($modelMember->isPasswordVerified($password))
                        {
                            $this->passwordVerified = TRUE;
                            if ($modelMember->isAuthenticated())
                            {
                                $this->passwordVerified = TRUE;
                                // start sessie
                                $this->session->start();
                                $this->session->makeToken($modelMember->getBdo()->getPassword(), $modelMember->getBdo()->getId());
                            }
                        }
                        else
                        {
                            // hoeveel keer al geprobeerd?
                            $modelLoginAttempt->getBdo()->setIdMember($modelMember->getBdo()->getId());
                            $modelLoginAttempt->getBdo()->setTime(time() - (2 * 60 * 60));
                            // als meer dan vijf keer blokkeer account
                            if ($modelLoginAttempt->getDal()->count() > 15)
                            {
                                $modelMember->getBdo()->setUserName($modelMember->getBdo()->getUserName());
	                            $modelMember->getBdo()->setLockedOut(TRUE);
                                $modelMember->getBdo()->setUpdatedBy('SYSTEM');
	                            $modelMember->getDal()->updateLockedOut();
                            }
                            else
                            {
                                $modelLoginAttempt->getBdo()->setTime(time());
                                $modelLoginAttempt->loginAttemptCreateOne($modelMember->getBdo()->getId(), time());
                            }
                        }
                    }
                }
            }
        }
	}

	namespace AnOrmApart\Membership\LoginAttempt;
	class Model extends \AnOrmApart\Membership\LoginAttempt\CodeBehind\Model
	{
	}

	namespace AnOrmApart\Membership\Member;
	class Model extends \AnOrmApart\Membership\Member\CodeBehind\Model
	{
        public function isMember($userName)
        {
            // we suppose it is not a member
            $result = FALSE;
            // pass the business dataobject to the DAL class
	        $this->bll->setUserName($userName);
	        $this->list = $this->dal->selectByUserName();
            if ($this->dal->getRowCount() >= 1)
            {
                $result = TRUE;
            }
            return $result;
        }

        public function isPasswordVerified($password)
        {
            // we supposse member is not verified
            $result = FALSE;
            // pass the business dataobject to the DAL class
            if ($this->bll->verifyPassword($password))
            {
                $result = TRUE;
            }
	        return $result;
        }

        public function isAuthenticated()
        {
            return $this->bll->getAuthenticated();
        }

        public function isLockedOut()
        {
            return $this->bll->getLockedOut();
        }

	}

	namespace AnOrmApart\Membership\Role;
	class Model extends \AnOrmApart\Membership\Role\CodeBehind\Model
	{
	}

	namespace AnOrmApart\Membership\MemberRole;
	class Model extends \AnOrmApart\Membership\MemberRole\CodeBehind\Model
	{
	}

?>

