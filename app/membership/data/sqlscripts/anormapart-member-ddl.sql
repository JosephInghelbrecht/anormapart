﻿-- An Orm Apart -- Wednesday 18th of February 2015 10:53:19 AM
-- 
SET GLOBAL TRANSACTION ISOLATION LEVEL SERIALIZABLE;
SET GLOBAL sql_mode = 'ANSI';
-- If database does not exist, create the database
CREATE DATABASE IF NOT EXISTS Membership;
-- With the MySQL FOREIGN_KEY_CHECKS variable,
-- you don't have to worry about the order of your
-- DROP and CREATE TABLE statements at all, and you can
-- write them in any order you like, even the exact opposite.
SET FOREIGN_KEY_CHECKS = 0;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE LoginAttempt
-- Created on Wednesday 18th of February 2015 10:53:19 AM
-- 
USE `Membership`;
DROP TABLE IF EXISTS `LoginAttempt`;
CREATE TABLE `LoginAttempt` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	`IdMember` INT NOT NULL,
	`Time` VARCHAR (30) NOT NULL);

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE Member
-- Created on Wednesday 18th of February 2015 10:53:19 AM
-- 
USE `Membership`;
DROP TABLE IF EXISTS `Member`;
CREATE TABLE `Member` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	`UserName` NVARCHAR (50) NOT NULL,
	`Email` VARCHAR (80) NOT NULL,
	`Password` CHAR (128) NOT NULL,
	`Salt` CHAR (128) NOT NULL,
	`LastActivity` TIMESTAMP NULL,
	`FirstLogin` TIMESTAMP NULL,
	`Authenticated` BIT NULL,
	`LockedOut` BIT NULL,
	`InsertedBy` NVARCHAR (255) NULL,
	`InsertedOn` TIMESTAMP NULL,
	`UpdatedBy` NVARCHAR (255) NULL,
	`UpdatedOn` TIMESTAMP NULL,
	CONSTRAINT uc_Member_UserName UNIQUE (UserName),
	CONSTRAINT uc_Member_Email UNIQUE (Email));

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE Role
-- Created on Wednesday 18th of February 2015 10:53:19 AM
-- 
USE `Membership`;
DROP TABLE IF EXISTS `Role`;
CREATE TABLE `Role` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	`Name` NVARCHAR (255) NOT NULL,
	`Description` NVARCHAR (255) NULL,
	`InsertedBy` NVARCHAR (255) NULL,
	`InsertedOn` TIMESTAMP NULL,
	`UpdatedBy` NVARCHAR (255) NULL,
	`UpdatedOn` TIMESTAMP NULL,
	CONSTRAINT uc_Role_Name UNIQUE (Name));

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE MemberRole
-- Created on Wednesday 18th of February 2015 10:53:19 AM
-- 
USE `Membership`;
DROP TABLE IF EXISTS `MemberRole`;
CREATE TABLE `MemberRole` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	`IdMember` INT NOT NULL,
	`IdRole` INT NOT NULL,
	`InsertedBy` NVARCHAR (255) NULL,
	`InsertedOn` TIMESTAMP NULL,
	`UpdatedBy` NVARCHAR (255) NULL,
	`UpdatedOn` TIMESTAMP NULL,
	CONSTRAINT fk_MemberRoleIdMember FOREIGN KEY (`IdMember`) REFERENCES `Member` (`Id`),
	CONSTRAINT fk_MemberRoleIdRole FOREIGN KEY (`IdRole`) REFERENCES `Role` (`Id`));

-- With the MySQL FOREIGN_KEY_CHECKS variable,
-- you don't have to worry about the order of your
-- DROP and CREATE TABLE statements at all, and you can
-- write them in any order you like, even the exact opposite.
SET FOREIGN_KEY_CHECKS = 1;


