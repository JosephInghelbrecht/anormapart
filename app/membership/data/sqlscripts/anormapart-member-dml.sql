﻿-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML Insert Stored Procedure for LoginAttempt 
-- 
USE Membership;
DROP PROCEDURE IF EXISTS LoginAttemptInsert;
DELIMITER //
CREATE PROCEDURE `LoginAttemptInsert`
(
	OUT pId INT ,
	IN pIdMember INT ,
	IN pTime VARCHAR (30) 
)
BEGIN
INSERT INTO `LoginAttempt`
	(
		`LoginAttempt`.`IdMember`,
		`LoginAttempt`.`Time`
	)
	VALUES
	(
		pIdMember,
		pTime
	);
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML Update Stored Procedure for LoginAttempt
-- 
USE Membership;
DROP PROCEDURE IF EXISTS LoginAttemptUpdate;
DELIMITER //
CREATE PROCEDURE `LoginAttemptUpdate`
(
	pId INT ,
	pIdMember INT ,
	pTime VARCHAR (30) 
)
BEGIN
UPDATE `LoginAttempt`
	SET
		`IdMember` = pIdMember,
		`Time` = pTime
	WHERE `LoginAttempt`.`Id` = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML Delete Stored Procedure for LoginAttempt 
-- 
USE Membership;
DROP PROCEDURE IF EXISTS LoginAttemptDelete;
DELIMITER //
CREATE PROCEDURE `LoginAttemptDelete`
(
	 pId INT 
)
BEGIN
DELETE FROM `LoginAttempt`
	WHERE `LoginAttempt`.`Id` = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML SelectOne Stored Procedure for LoginAttempt 
-- 
USE Membership;
DROP PROCEDURE IF EXISTS LoginAttemptSelectOne;
DELIMITER //
CREATE PROCEDURE `LoginAttemptSelectOne`
(
	 pId INT 
)
BEGIN
SELECT * FROM `LoginAttempt`
	WHERE `LoginAttempt`.`Id` = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML SelectAll Stored Procedure for table LoginAttempt 
-- 
USE Membership;
DROP PROCEDURE IF EXISTS LoginAttemptSelectAll;
DELIMITER //
CREATE PROCEDURE `LoginAttemptSelectAll`
(
)
BEGIN
SELECT `LoginAttempt`.`Id`, `LoginAttempt`.`IdMember`, `LoginAttempt`.`Time`
	FROM `LoginAttempt`
;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML Count Stored Procedure for table LoginAttempt 
-- 
USE Membership;
DROP PROCEDURE IF EXISTS LoginAttemptCount;
DELIMITER //
CREATE PROCEDURE `LoginAttemptCount`
(
)
BEGIN
	SELECT count(*) FROM `LoginAttempt`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML SelectById Stored Procedure for table LoginAttempt
-- 
USE Membership;
DROP PROCEDURE IF EXISTS LoginAttemptSelectById;
DELIMITER //
CREATE PROCEDURE `LoginAttemptSelectById`
(
	 pId INT 
)
BEGIN
SELECT `LoginAttempt`.`Id`, `LoginAttempt`.`IdMember`, `LoginAttempt`.`Time`

	FROM `LoginAttempt`
	WHERE `LoginAttempt`.`Id` = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML SelectByIdMember Stored Procedure for table LoginAttempt
-- 
USE Membership;
DROP PROCEDURE IF EXISTS LoginAttemptSelectByIdMember;
DELIMITER //
CREATE PROCEDURE `LoginAttemptSelectByIdMember`
(
	 pIdMember INT 
)
BEGIN
SELECT `LoginAttempt`.`Id`, `LoginAttempt`.`IdMember`, `LoginAttempt`.`Time`

	FROM `LoginAttempt`
	WHERE `LoginAttempt`.`IdMember` = pIdMember
	ORDER BY `LoginAttempt`.`IdMember`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML Insert Stored Procedure for Member 
-- 
USE Membership;
DROP PROCEDURE IF EXISTS MemberInsert;
DELIMITER //
CREATE PROCEDURE `MemberInsert`
(
	OUT pId INT ,
	IN pUserName NVARCHAR (50) ,
	IN pEmail VARCHAR (80) ,
	IN pPassword CHAR (128) ,
	IN pSalt CHAR (128) ,
	IN pAuthenticated BIT ,
	IN pLockedOut BIT ,
	IN pInsertedBy NVARCHAR (255) 
)
BEGIN
INSERT INTO `Member`
	(
		`Member`.`UserName`,
		`Member`.`Email`,
		`Member`.`Password`,
		`Member`.`Salt`,
		`Member`.`FirstLogin`,
		`Member`.`Authenticated`,
		`Member`.`LockedOut`,
		`Member`.`InsertedBy`,
		`Member`.`InsertedOn`
	)
	VALUES
	(
		pUserName,
		pEmail,
		pPassword,
		pSalt,
		NOW(),
		pAuthenticated,
		pLockedOut,
		pInsertedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML Update Stored Procedure for Member
-- 
USE Membership;
DROP PROCEDURE IF EXISTS MemberUpdate;
DELIMITER //
CREATE PROCEDURE `MemberUpdate`
(
	pId INT ,
	pUserName NVARCHAR (50) ,
	pEmail VARCHAR (80) ,
	pPassword CHAR (128) ,
	pSalt CHAR (128) ,
	pAuthenticated BIT ,
	pLockedOut BIT ,
	pUpdatedBy NVARCHAR (255) 
)
BEGIN
UPDATE `Member`
	SET
		`UserName` = pUserName,
		`Email` = pEmail,
		`Password` = pPassword,
		`Salt` = pSalt,
		`LastActivity` = NOW(),
		`Authenticated` = pAuthenticated,
		`LockedOut` = pLockedOut,
		`UpdatedBy` = pUpdatedBy,
		`UpdatedOn` = NOW()
	WHERE `Member`.`Id` = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML Delete Stored Procedure for Member 
-- 
USE Membership;
DROP PROCEDURE IF EXISTS MemberDelete;
DELIMITER //
CREATE PROCEDURE `MemberDelete`
(
	 pId INT 
)
BEGIN
DELETE FROM `Member`
	WHERE `Member`.`Id` = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML SelectOne Stored Procedure for Member 
-- 
USE Membership;
DROP PROCEDURE IF EXISTS MemberSelectOne;
DELIMITER //
CREATE PROCEDURE `MemberSelectOne`
(
	 pId INT 
)
BEGIN
SELECT * FROM `Member`
	WHERE `Member`.`Id` = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML SelectAll Stored Procedure for table Member 
-- 
USE Membership;
DROP PROCEDURE IF EXISTS MemberSelectAll;
DELIMITER //
CREATE PROCEDURE `MemberSelectAll`
(
)
BEGIN
SELECT `Member`.`Id`, `Member`.`UserName`, `Member`.`Email`, `Member`.`Password`, `Member`.`Authenticated`, `Member`.`LockedOut`
	FROM `Member`
;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML Count Stored Procedure for table Member 
-- 
USE Membership;
DROP PROCEDURE IF EXISTS MemberCount;
DELIMITER //
CREATE PROCEDURE `MemberCount`
(
)
BEGIN
	SELECT count(*) FROM `Member`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML SelectById Stored Procedure for table Member
-- 
USE Membership;
DROP PROCEDURE IF EXISTS MemberSelectById;
DELIMITER //
CREATE PROCEDURE `MemberSelectById`
(
	 pId INT 
)
BEGIN
SELECT `Member`.`Id`, `Member`.`UserName`, `Member`.`Email`, `Member`.`Password`, `Member`.`Authenticated`, `Member`.`LockedOut`

	FROM `Member`
	WHERE `Member`.`Id` = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML SelectByUserName Stored Procedure for table Member
-- 
USE Membership;
DROP PROCEDURE IF EXISTS MemberSelectByUserName;
DELIMITER //
CREATE PROCEDURE `MemberSelectByUserName`
(
	 pUserName NVARCHAR (50) 
)
BEGIN
SELECT `Member`.`Id`, `Member`.`UserName`, `Member`.`Email`, `Member`.`Password`, `Member`.`Authenticated`, `Member`.`LockedOut`

	FROM `Member`
	WHERE `Member`.`UserName` = pUserName
	ORDER BY `Member`.`UserName`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML SelectByEmail Stored Procedure for table Member
-- 
USE Membership;
DROP PROCEDURE IF EXISTS MemberSelectByEmail;
DELIMITER //
CREATE PROCEDURE `MemberSelectByEmail`
(
	 pEmail VARCHAR (80) 
)
BEGIN
SELECT `Member`.`Id`, `Member`.`UserName`, `Member`.`Email`, `Member`.`Password`, `Member`.`Authenticated`, `Member`.`LockedOut`

	FROM `Member`
	WHERE `Member`.`Email` = pEmail
	ORDER BY `Member`.`Email`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML SelectLikeUserName Stored Procedure for table Member 
-- 
USE Membership;
DROP PROCEDURE IF EXISTS MemberSelectLikeUserName;
DELIMITER //
CREATE PROCEDURE `MemberSelectLikeUserName`
(
	pUserName NVARCHAR (50) 
)
BEGIN
SELECT `Member`.`Id`, `Member`.`UserName`, `Member`.`Email`, `Member`.`Password`, `Member`.`Authenticated`, `Member`.`LockedOut`
 
	FROM `Member`
	WHERE `Member`.`UserName` like CONCAT(pUserName, '%')
	ORDER BY `Member`.`UserName`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML SelectLikeEmail Stored Procedure for table Member 
-- 
USE Membership;
DROP PROCEDURE IF EXISTS MemberSelectLikeEmail;
DELIMITER //
CREATE PROCEDURE `MemberSelectLikeEmail`
(
	pEmail VARCHAR (80) 
)
BEGIN
SELECT `Member`.`Id`, `Member`.`UserName`, `Member`.`Email`, `Member`.`Password`, `Member`.`Authenticated`, `Member`.`LockedOut`
 
	FROM `Member`
	WHERE `Member`.`Email` like CONCAT(pEmail, '%')
	ORDER BY `Member`.`Email`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML SelectLikeXUserName Stored Procedure for table Member
-- 
USE Membership;
DROP PROCEDURE IF EXISTS MemberSelectLikeXUserName;
DELIMITER //
CREATE PROCEDURE `MemberSelectLikeXUserName`
(
	pUserName NVARCHAR (50) 
)
BEGIN
	SELECT `Member`.`Id`, `Member`.`UserName`, `Member`.`Email`, `Member`.`Password`, `Member`.`Authenticated`, `Member`.`LockedOut`

	FROM `Member`
	WHERE `Member`.`UserName` like CONCAT('%', pUserName, '%')

	ORDER BY `Member`.`UserName` ;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML SelectLikeXEmail Stored Procedure for table Member
-- 
USE Membership;
DROP PROCEDURE IF EXISTS MemberSelectLikeXEmail;
DELIMITER //
CREATE PROCEDURE `MemberSelectLikeXEmail`
(
	pEmail VARCHAR (80) 
)
BEGIN
	SELECT `Member`.`Id`, `Member`.`UserName`, `Member`.`Email`, `Member`.`Password`, `Member`.`Authenticated`, `Member`.`LockedOut`

	FROM `Member`
	WHERE `Member`.`Email` like CONCAT('%', pEmail, '%')

	ORDER BY `Member`.`Email` ;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML Insert Stored Procedure for Role 
-- 
USE Membership;
DROP PROCEDURE IF EXISTS RoleInsert;
DELIMITER //
CREATE PROCEDURE `RoleInsert`
(
	OUT pId INT ,
	IN pName NVARCHAR (255) ,
	IN pDescription NVARCHAR (255) ,
	IN pInsertedBy NVARCHAR (255) 
)
BEGIN
INSERT INTO `Role`
	(
		`Role`.`Name`,
		`Role`.`Description`,
		`Role`.`InsertedBy`,
		`Role`.`InsertedOn`
	)
	VALUES
	(
		pName,
		pDescription,
		pInsertedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML Update Stored Procedure for Role
-- 
USE Membership;
DROP PROCEDURE IF EXISTS RoleUpdate;
DELIMITER //
CREATE PROCEDURE `RoleUpdate`
(
	pId INT ,
	pName NVARCHAR (255) ,
	pDescription NVARCHAR (255) ,
	pUpdatedBy NVARCHAR (255) 
)
BEGIN
UPDATE `Role`
	SET
		`Name` = pName,
		`Description` = pDescription,
		`UpdatedBy` = pUpdatedBy,
		`UpdatedOn` = NOW()
	WHERE `Role`.`Id` = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML Delete Stored Procedure for Role 
-- 
USE Membership;
DROP PROCEDURE IF EXISTS RoleDelete;
DELIMITER //
CREATE PROCEDURE `RoleDelete`
(
	 pId INT 
)
BEGIN
DELETE FROM `Role`
	WHERE `Role`.`Id` = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML SelectOne Stored Procedure for Role 
-- 
USE Membership;
DROP PROCEDURE IF EXISTS RoleSelectOne;
DELIMITER //
CREATE PROCEDURE `RoleSelectOne`
(
	 pId INT 
)
BEGIN
SELECT * FROM `Role`
	WHERE `Role`.`Id` = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML SelectAll Stored Procedure for table Role 
-- 
USE Membership;
DROP PROCEDURE IF EXISTS RoleSelectAll;
DELIMITER //
CREATE PROCEDURE `RoleSelectAll`
(
)
BEGIN
SELECT `Role`.`Id`, `Role`.`Name`
	FROM `Role`
;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML Count Stored Procedure for table Role 
-- 
USE Membership;
DROP PROCEDURE IF EXISTS RoleCount;
DELIMITER //
CREATE PROCEDURE `RoleCount`
(
)
BEGIN
	SELECT count(*) FROM `Role`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML SelectById Stored Procedure for table Role
-- 
USE Membership;
DROP PROCEDURE IF EXISTS RoleSelectById;
DELIMITER //
CREATE PROCEDURE `RoleSelectById`
(
	 pId INT 
)
BEGIN
SELECT `Role`.`Id`, `Role`.`Name`

	FROM `Role`
	WHERE `Role`.`Id` = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML SelectByName Stored Procedure for table Role
-- 
USE Membership;
DROP PROCEDURE IF EXISTS RoleSelectByName;
DELIMITER //
CREATE PROCEDURE `RoleSelectByName`
(
	 pName NVARCHAR (255) 
)
BEGIN
SELECT `Role`.`Id`, `Role`.`Name`

	FROM `Role`
	WHERE `Role`.`Name` = pName
	ORDER BY `Role`.`Name`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML SelectLikeName Stored Procedure for table Role 
-- 
USE Membership;
DROP PROCEDURE IF EXISTS RoleSelectLikeName;
DELIMITER //
CREATE PROCEDURE `RoleSelectLikeName`
(
	pName NVARCHAR (255) 
)
BEGIN
SELECT `Role`.`Id`, `Role`.`Name`
 
	FROM `Role`
	WHERE `Role`.`Name` like CONCAT(pName, '%')
	ORDER BY `Role`.`Name`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML SelectLikeXName Stored Procedure for table Role
-- 
USE Membership;
DROP PROCEDURE IF EXISTS RoleSelectLikeXName;
DELIMITER //
CREATE PROCEDURE `RoleSelectLikeXName`
(
	pName NVARCHAR (255) 
)
BEGIN
	SELECT `Role`.`Id`, `Role`.`Name`

	FROM `Role`
	WHERE `Role`.`Name` like CONCAT('%', pName, '%')

	ORDER BY `Role`.`Name` ;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML Insert Stored Procedure for MemberRole 
-- 
USE Membership;
DROP PROCEDURE IF EXISTS MemberRoleInsert;
DELIMITER //
CREATE PROCEDURE `MemberRoleInsert`
(
	OUT pId INT ,
	IN pIdMember INT ,
	IN pIdRole INT ,
	IN pInsertedBy NVARCHAR (255) 
)
BEGIN
INSERT INTO `MemberRole`
	(
		`MemberRole`.`IdMember`,
		`MemberRole`.`IdRole`,
		`MemberRole`.`InsertedBy`,
		`MemberRole`.`InsertedOn`
	)
	VALUES
	(
		pIdMember,
		pIdRole,
		pInsertedBy,
		NOW()
	);
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML Update Stored Procedure for MemberRole
-- 
USE Membership;
DROP PROCEDURE IF EXISTS MemberRoleUpdate;
DELIMITER //
CREATE PROCEDURE `MemberRoleUpdate`
(
	pId INT ,
	pIdMember INT ,
	pIdRole INT ,
	pUpdatedBy NVARCHAR (255) 
)
BEGIN
UPDATE `MemberRole`
	SET
		`IdMember` = pIdMember,
		`IdRole` = pIdRole,
		`UpdatedBy` = pUpdatedBy,
		`UpdatedOn` = NOW()
	WHERE `MemberRole`.`Id` = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML Delete Stored Procedure for MemberRole 
-- 
USE Membership;
DROP PROCEDURE IF EXISTS MemberRoleDelete;
DELIMITER //
CREATE PROCEDURE `MemberRoleDelete`
(
	 pId INT 
)
BEGIN
DELETE FROM `MemberRole`
	WHERE `MemberRole`.`Id` = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML SelectOne Stored Procedure for MemberRole 
-- 
USE Membership;
DROP PROCEDURE IF EXISTS MemberRoleSelectOne;
DELIMITER //
CREATE PROCEDURE `MemberRoleSelectOne`
(
	 pId INT 
)
BEGIN
SELECT * FROM `MemberRole`
	WHERE `MemberRole`.`Id` = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML SelectAll Stored Procedure for table MemberRole 
-- 
USE Membership;
DROP PROCEDURE IF EXISTS MemberRoleSelectAll;
DELIMITER //
CREATE PROCEDURE `MemberRoleSelectAll`
(
)
BEGIN
SELECT `MemberRole`.`Id`, `MemberRole`.`IdMember`, `IdMember-Member`.`UserName` as IdMemberUserName, `MemberRole`.`IdRole`, `IdRole-Role`.`Name` as IdRoleName
	FROM `MemberRole`
	INNER JOIN `Member` as `IdMember-Member`
		ON `MemberRole`.`IdMember` = `IdMember-Member`.`Id`
	INNER JOIN `Role` as `IdRole-Role`
		ON `MemberRole`.`IdRole` = `IdRole-Role`.`Id`
;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML Count Stored Procedure for table MemberRole 
-- 
USE Membership;
DROP PROCEDURE IF EXISTS MemberRoleCount;
DELIMITER //
CREATE PROCEDURE `MemberRoleCount`
(
)
BEGIN
	SELECT count(*) FROM `MemberRole`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML SelectById Stored Procedure for table MemberRole
-- 
USE Membership;
DROP PROCEDURE IF EXISTS MemberRoleSelectById;
DELIMITER //
CREATE PROCEDURE `MemberRoleSelectById`
(
	 pId INT 
)
BEGIN
SELECT `MemberRole`.`Id`, `MemberRole`.`IdMember`, `IdMember-Member`.`UserName` as IdMemberUserName, `MemberRole`.`IdRole`, `IdRole-Role`.`Name` as IdRoleName

	FROM `MemberRole`
	INNER JOIN `Member` as `IdMember-Member`
		ON `MemberRole`.`IdMember` = `IdMember-Member`.`Id`
	INNER JOIN `Role` as `IdRole-Role`
		ON `MemberRole`.`IdRole` = `IdRole-Role`.`Id`
	WHERE `MemberRole`.`Id` = pId;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML SelectByIdMember Stored Procedure for table MemberRole
-- 
USE Membership;
DROP PROCEDURE IF EXISTS MemberRoleSelectByIdMember;
DELIMITER //
CREATE PROCEDURE `MemberRoleSelectByIdMember`
(
	 pIdMember INT 
)
BEGIN
SELECT `MemberRole`.`Id`, `MemberRole`.`IdMember`, `IdMember-Member`.`UserName` as IdMemberUserName, `MemberRole`.`IdRole`, `IdRole-Role`.`Name` as IdRoleName

	FROM `MemberRole`
	INNER JOIN `Member` as `IdMember-Member`
		ON `MemberRole`.`IdMember` = `IdMember-Member`.`Id`
	INNER JOIN `Role` as `IdRole-Role`
		ON `MemberRole`.`IdRole` = `IdRole-Role`.`Id`
	WHERE `MemberRole`.`IdMember` = pIdMember
	ORDER BY `MemberRole`.`IdMember`;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML SelectByIdRole Stored Procedure for table MemberRole
-- 
USE Membership;
DROP PROCEDURE IF EXISTS MemberRoleSelectByIdRole;
DELIMITER //
CREATE PROCEDURE `MemberRoleSelectByIdRole`
(
	 pIdRole INT 
)
BEGIN
SELECT `MemberRole`.`Id`, `MemberRole`.`IdMember`, `IdMember-Member`.`UserName` as IdMemberUserName, `MemberRole`.`IdRole`, `IdRole-Role`.`Name` as IdRoleName

	FROM `MemberRole`
	INNER JOIN `Member` as `IdMember-Member`
		ON `MemberRole`.`IdMember` = `IdMember-Member`.`Id`
	INNER JOIN `Role` as `IdRole-Role`
		ON `MemberRole`.`IdRole` = `IdRole-Role`.`Id`
	WHERE `MemberRole`.`IdRole` = pIdRole
	ORDER BY `MemberRole`.`IdRole`;
END //
DELIMITER ;


