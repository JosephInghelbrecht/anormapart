﻿-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML Update Stored Procedure for Member
-- 
USE Membership;
DROP PROCEDURE IF EXISTS MemberUpdateLockedOut;
DELIMITER //
CREATE PROCEDURE `MemberUpdateLockedOut`
(
	pUserName NVARCHAR (50),
	pLockedOut BIT ,
	pUpdatedBy NVARCHAR (255) 
)
BEGIN
UPDATE `Member`
	SET
		`LockedOut` = pLockedOut,
		`UpdatedBy` = pUpdatedBy,
		`UpdatedOn` = NOW()
	WHERE `Member`.`UserName` = pUserName;
END //
DELIMITER ;

-- inantwerpen.com
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql DML
-- Created : Wednesday 18th of February 2015 10:56:58 AM
-- DML Update Stored Procedure for Member
-- 
USE Membership;
DROP PROCEDURE IF EXISTS LoginAttemptCount;
DELIMITER //
CREATE PROCEDURE `LoginAttemptCount`
(
	pIdMember INT ,
	pTime VARCHAR (30) 
)
BEGIN
	SELECT count(`IdMember`) FROM `LoginAttempt`
    WHERE `IdMember` = pIdMember AND `Time` > pTime;
END //
DELIMITER ;