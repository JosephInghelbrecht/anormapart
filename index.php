﻿<?php

?>

<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="utf-8"/>
    <title>An Orm Apart</title>
    <link type="text/css" rel="stylesheet" href="css/tower.css">
    <link type="text/css" rel="stylesheet" href="css/floor.css">
    <link type="text/css" rel="stylesheet" href="css/room.css">
    <link type="text/css" rel="stylesheet" href="css/control-panel.css">
    <link type="text/css" rel="stylesheet" href="css/iconfont.css">
    <link type="text/css" rel="stylesheet" href="css/tile.css">
    <link type="text/css" rel="stylesheet" href="css/animate.css">
    <link type="text/css" rel="stylesheet" href="css/footer.css">
    <link type="text/css" rel="stylesheet" href="css/anormapart.css">
    <link type="text/css" rel="stylesheet" href="css/anormapart-logical-model.css">
    <link type="text/css" rel="stylesheet" href="css/form.css">
    <script src="ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="app/js/ajax.js"></script>
    <script type="text/javascript" src="app/anormapart/code/js/anormapart.js"></script>
    <script>
        window.onload = function () {
            location.href = "#home-floor";
            // Listen to the double click event.
            if (window.addEventListener)
                document.body.addEventListener('dblclick', onDoubleClick, false);
            else if (window.attachEvent)
                document.body.attachEvent('ondblclick', onDoubleClick);

            document.body.addEventListener('click', coordinatingController, true);
        };

        function onDoubleClick(ev) {
            // Get the element which fired the event. This is not necessarily the
            // element to which the event has been attached.
            var element = ev.target || ev.srcElement;

            // Find out the div that holds this element.
            var name;

            do {
                element = element.parentNode;
            }
            while (element && (name = element.nodeName.toLowerCase()) &&
            (name != 'div' || element.className.indexOf('editable') == -1) && name != 'body');

            if (name == 'div' && element.className.indexOf('editable') != -1)
                replaceDiv(element);
        }

        var editor;

        function replaceDiv(div) {
            if (editor)
                editor.destroy();

            editor = CKEDITOR.replace(div);
            editor.config.allowedContent = true;

            // editor.config.extraAllowedContent = 'div h4 h5 h6';
            editor.config.height = '36em';
            editor.config.entities = false;
            editor.config.entities_latin = false;
        }

        function closeEditor() {
            if (editor) {
                var changed = editor.checkDirty();
                // alert(changed);
                editor.destroy();
                editor = null;

                if (changed) {
                    // alert("Content changed");
                    // save catalog to server
                    depotDispatcher('write-catalog');
                }
            }
            goBackTo();
        }
    </script>
</head>
<body>
<div id="watermark"></div>
<div id="tower">
    <?php
    include('app/anormapart/code/view/anormapart-catalog-editor-view.php');
    include('app/anormapart/code/view/anormapart-code-editor-view.php');
    include('app/anormapart/code/view/anormapart-home-view.php');
    include('app/anormapart/code/view/anormapart-catalog-view.php');
    include('app/anormapart/code/view/anormapart-template-view.php');
    include('app/anormapart/code/view/anormapart-php-view.php');
    include('app/anormapart/code/view/anormapart-php-dal-view.php');
    include('app/anormapart/code/view/anormapart-php-bll-view.php');
    include('app/anormapart/code/view/anormapart-php-mvc-view.php');
    include('app/anormapart/code/view/anormapart-php-threepenny-view.php');
    include('app/anormapart/code/view/anormapart-php-helpers-view.php');
    require('app/anormapart/code/view/anormapart-sql-view.php');
    include('app/membership/code/view/login-view.php');
    include('app/membership/code/view/register-view.php');
    include('app/anormapart/code/view/anormapart-csharp-mvc-view.php');
    ?>
</div>
<footer>
    <p class="copy">concept & design - Entreprise de Modes et de Manières Modernes 2012-2015<br>
    </p>

    <p>
        <a href="<?php echo(isset($membershipModel) ? ($membershipModel->isLoggedIn() ? '#' : '#login') : '#login'); ?>">
            <?php echo(isset($membershipModel) ? ($membershipModel->isLoggedIn() ? 'Afmelden' : 'Aanmelden') : 'Aanmelden'); ?>
        </a>
    </p>

    <div class="vcard">
        <h3>Contact</h3>

        <p class="fn org">a n<span>orm</span> apart</p>

        <div class="adr">
            <div class="street-address">Braziliëstraat 38</div>
            <div class="postal-code">2000</div>
            <div class="locality">Antwerpen</div>
            <div class="country-name">België</div>
            <div class="email"><a href="mailto:jef.inghelbrecht@inantwerpen.com">jef.inghelbrecht@inantwerpen.com</a>
            </div>
        </div>
    </div>
</footer>
</body>
</html>