/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referencing this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'tower-pattern\'">' + entity + '</span>' + html;
	}
	var icons = {
		'icon-enter': '&#xe600;',
		'icon-exit': '&#xe601;',
		'icon-pencil': '&#xe602;',
		'icon-image': '&#xe603;',
		'icon-images': '&#xe604;',
		'icon-book': '&#xe605;',
		'icon-books': '&#xe606;',
		'icon-file': '&#xe607;',
		'icon-copy': '&#xe608;',
		'icon-stack': '&#xe609;',
		'icon-folder': '&#xe60a;',
		'icon-folder-open': '&#xe60b;',
		'icon-notebook': '&#xe60c;',
		'icon-disk': '&#xe60d;',
		'icon-user': '&#xe60e;',
		'icon-search': '&#xe60f;',
		'icon-key': '&#xe610;',
		'icon-minus': '&#xe611;',
		'icon-plus': '&#xe612;',
		'icon-arrow-up': '&#xe613;',
		'icon-arrow-right': '&#xe614;',
		'icon-arrow-down': '&#xe615;',
		'icon-arrow-left': '&#xe616;',
		'icon-bold': '&#xe617;',
		'icon-underline': '&#xe618;',
		'icon-italic': '&#xe619;',
		'icon-paragraph-left': '&#xe61a;',
		'icon-paragraph-center': '&#xe61b;',
		'icon-paragraph-right': '&#xe61c;',
		'icon-list': '&#xe61d;',
		'icon-numbered-list': '&#xe61e;',
		'icon-menu': '&#xe61f;',
		'icon-menu2': '&#xe620;',
		'icon-print': '&#xe621;',
		'icon-remove': '&#xe622;',
		'icon-link': '&#xe623;',
		'icon-scissors': '&#xe624;',
		'icon-close': '&#xe625;',
		'icon-pencil2': '&#xe626;',
		'icon-undo': '&#xe627;',
		'icon-redo': '&#xe628;',
		'icon-file-xml': '&#xe629;',
		'icon-file-css': '&#xe62a;',
		'icon-html5': '&#xe62b;',
		'icon-css3': '&#xe62c;',
		'icon-mail': '&#xe62d;',
		'icon-indent-increase': '&#xe62e;',
		'icon-indent-decrease': '&#xe62f;',
		'icon-table': '&#xe630;',
		'icon-paste': '&#xe631;',
		'icon-spoon-knife': '&#xe632;',
		'icon-opt': '&#xe633;',
		'icon-users': '&#xe634;',
		'icon-send': '&#xe635;',
		'icon-paperplane': '&#xe636;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
